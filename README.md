# Svilen Marchev's CodeForces sources #

## Java with IntelliJ ##

You can use IntelliJ's community edition.

If not using the CHelper plugin: When creating a new project for a given contest, make sure you create a new dir for the contest and inside the dir create another dir - "java" or "kotlin", depending on the language you are going to use.

### Egor's CHelper IntelliJ plugin ###

You can use the Egor's CHelper plugin:

* https://plugins.jetbrains.com/idea/plugin/7091-chelper
* https://code.google.com/archive/p/idea-chelper/wikis/MainPage.wiki
* http://codeforces.com/blog/entry/18974
* https://www.youtube.com/watch?v=mKLbCFJHd2U (how Egor uses it in practice)
* https://github.com/EgorKulikov/idea-chelper (its code)

NOTE: When you initially set up the project with the required CHelper structure, make sure you specify a package name within the default dir. E.g. give "main/bg.svilen.codeforces" instead of just "main". For the inputClass you can give "java.io.InputStreamReader" (but any class with a ctor that takes a java.io.InputStream would work).

Make sure you create a "main" dir with a "bg.svilen.codeforces" subdirectory in it (not bg/svilen/codeforces). Parsing a contest might also help with creating the structure. (Restoring a task from the archive didn't work for me immediately.)

In order to create your own generated tests, click the "Edit Task" action, then "Advanced" and enable the checkbox for test case and finally click the "Create" button. The newly created class (the name should be something like TaskDTestCase) has a method createTests. Just create your own tests using the "new Test(inputString, outputString)" ctor. The generated tests will be run after all other enabled tests.