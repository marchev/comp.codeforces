/**
 * CodeForces
 * Codeforces Round #402 (Div. 1)
 * http://codeforces.com/contest/778
 * http://codeforces.com/contest/778/problem/bg.svilen.A
 * A. String Game
 *
 * Status: Accepted
 * Submission: http://codeforces.com/contest/778/submission/25057058
 * Date: 2017-02-26
 * Author: Svilen Marchev
 * Tags: binary search, greedy, strings
 */

import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.*

object A {
  object sc {
    val reader = BufferedReader(InputStreamReader(System.`in`))
    var tokenizer = StringTokenizer("")

    fun next(): String {
      while (!tokenizer.hasMoreTokens()) {
        val nextLine = reader.readLine() ?: throw IllegalStateException()
        tokenizer = StringTokenizer(nextLine)
      }
      return tokenizer.nextToken()
    }

    fun nextInt(): Int {
      return next().toInt()
    }
  }

  val MAX = 200010
  val ix2deletionStep = IntArray(MAX)

  fun isContainedAfterDeletions(a: String, b: String, numDeletions: Int): Boolean {
    var aIx = 0
    for (bc in b) {
      while (aIx < a.length && (ix2deletionStep[aIx] <= numDeletions || a[aIx] != bc)) {
        aIx++
      }
      if (aIx == a.length) {
        return false
      }
      aIx++
    }
    return true
  }

  fun solve() {
    val a = sc.next()
    val b = sc.next()
    for (i in 1..a.length) {
      ix2deletionStep[sc.nextInt() - 1] = i
    }

    var l = 0
    var r = a.length - b.length + 1
    while (r - l > 1) {
      var m = (l + r) / 2
      if (isContainedAfterDeletions(a, b, m)) {
        l = m
      } else {
        r = m
      }
    }
    println(l)
  }
}

fun main(args: Array<String>) {
  A.solve()
}
