/**
 * CodeForces
 * Codeforces Round #390 (Div. 2)
 * http://codeforces.com/contest/754
 * http://codeforces.com/contest/754/problem/A
 * A - Lesha and array splitting
 *
 * Status: Accepted
 * Date: 2017-02-25
 * Author: Svilen Marchev
 * Tags: DP
 */

import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.*

object A {
  object sc {
    val reader = BufferedReader(InputStreamReader(System.`in`))
    var tokenizer = StringTokenizer("")

    fun next(): String {
      while (!tokenizer.hasMoreTokens()) {
        val nextLine = reader.readLine() ?: throw IllegalStateException()
        tokenizer = StringTokenizer(nextLine)
      }
      return tokenizer.nextToken()
    }

    fun nextInt(): Int {
      return next().toInt()
    }
  }

  fun printSolution(g: IntArray, n: Int, num: Int) {
    if (n == 0) {
      println(num)
      return
    }
    printSolution(g, g[n], num + 1)
    println("${g[n] + 1} $n")
  }

  fun solve() {
    val n = sc.nextInt()
    val a = IntArray(n + 1)
    for (i in 1..n) {
      a[i] = sc.nextInt()
    }

    val f = BooleanArray(n + 1)
    val g = IntArray(n + 1)
    f[0] = true

    for (i in 1..n) {
      var sum = 0
      for (k in i downTo 1) {
        sum += a[k]
        if (sum != 0 && f[k - 1]) {
          f[i] = true
          g[i] = k - 1
        }
      }
    }

    if (f[n]) {
      println("YES")
      printSolution(g, n, 0)
    } else {
      println("NO")
    }
  }
}

fun main(args: Array<String>) {
  A.solve()
}
