/**
 * CodeForces
 * Codeforces Round #390 (Div. 2)
 * http://codeforces.com/contest/754
 * http://codeforces.com/contest/754/problem/B
 * B - Ilya and tic-tac-toe game
 *
 * Status: Accepted
 * Submission: http://codeforces.com/contest/754/submission/25025068
 * Date: 2017-02-25
 * Author: Svilen Marchev
 * Tags: implementation
 */

import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.*

object B {
  object sc {
    val reader = BufferedReader(InputStreamReader(System.`in`))
    var tokenizer = StringTokenizer("")

    fun next(): String {
      while (!tokenizer.hasMoreTokens()) {
        val nextLine = reader.readLine() ?: throw IllegalStateException()
        tokenizer = StringTokenizer(nextLine)
      }
      return tokenizer.nextToken()
    }

    fun nextInt(): Int {
      return next().toInt()
    }
  }

  val a = Array(4, { CharArray(4) })

  fun w(i: Int, j: Int): Boolean {
    return a[i][j] == 'x'
  }

  fun isWinning(): Boolean {
    var ans = false
    // rows
    for (i in 0..3) {
      for (j in 0..1) {
        ans = ans || w(i, j) && w(i, j + 1) && w(i, j + 2)
      }
    }
    // cols
    for (i in 0..1) {
      for (j in 0..3) {
        ans = ans || w(i, j) && w(i + 1, j) && w(i + 2, j)
      }
    }
    // diag
    ans = ans || w(0, 0) && w(1, 1) && w(2, 2)
    ans = ans || w(1, 1) && w(2, 2) && w(3, 3)
    ans = ans || w(0, 1) && w(1, 2) && w(2, 3)
    ans = ans || w(1, 0) && w(2, 1) && w(3, 2)
    // rev diag
    ans = ans || w(0, 2) && w(1, 1) && w(2, 0)
    ans = ans || w(0, 3) && w(1, 2) && w(2, 1)
    ans = ans || w(1, 2) && w(2, 1) && w(3, 0)
    ans = ans || w(1, 3) && w(2, 2) && w(3, 1)
    return ans
  }

  fun canWin(): Boolean {
    for (i in 0..3) {
      for (j in 0..3) {
        if (a[i][j] == '.') {
          val saved = a[i][j]
          a[i][j] = 'x'
          if (isWinning()) {
            return true
          }
          a[i][j] = saved
        }
      }
    }
    return false
  }

  fun solve() {
    for (i in 0..3) {
      a[i] = sc.next().toCharArray()
    }
    println(if (canWin()) "YES" else "NO")
  }
}

fun main(args: Array<String>) {
  B.solve()
}
