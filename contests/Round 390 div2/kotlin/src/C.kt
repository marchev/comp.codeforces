/**
 * CodeForces
 * Codeforces Round #390 (Div. 2)
 * http://codeforces.com/contest/754
 * http://codeforces.com/contest/754/problem/C
 * C - Vladik and chat
 *
 * Status: Accepted
 * Submission: http://codeforces.com/contest/754/submission/25025980
 * Date: 2017-02-25
 * Author: Svilen Marchev
 * Tags: DP, implementation
 */

import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.*

object C {
  object sc {
    val reader = BufferedReader(InputStreamReader(System.`in`))

    fun nextLine(): String {
      return reader.readLine()
    }
  }

  val userName2userId = HashMap<String, Int>()
  val userId2userName = HashMap<Int, String>()

  fun doesMsgMentionUser(msg: String, user: String): Boolean {
    var startIx = 0
    while (true) {
      val p = msg.indexOf(user, startIx)
      if (p == -1) {
        return false
      }
      if ((p == 0 || !msg[p - 1].isLetterOrDigit()) &&
          (p + user.length == msg.length || !msg[p + user.length].isLetterOrDigit())) {
        return true
      }
      startIx = p + 1
    }
  }

  fun printSolution(msgXuser2prevUserId: Array<IntArray>, userId: Int, msgIx: Int, msgs: Array<String>) {
    if (msgIx == 0) {
      return
    }
    printSolution(msgXuser2prevUserId, msgXuser2prevUserId[msgIx][userId], msgIx - 1, msgs)
    println("${userId2userName[userId]}:${msgs[msgIx]}")
  }

  fun solveTest() {
    userName2userId.clear()
    userId2userName.clear()

    val n = sc.nextLine().toInt()
    val allUsers = sc.nextLine()
    val tokenizer = StringTokenizer(allUsers)
    while (tokenizer.hasMoreTokens()) {
      val userName = tokenizer.nextToken()
      val userId = userName2userId.size
      userName2userId.put(userName, userId)
      userId2userName.put(userId, userName)
    }

    val m = sc.nextLine().toInt()
    val msgs = Array(m + 1, { "" })

    // f[i][u] = true, iff it's possible to assign the first i messages somehow, with the i-th
    // message being sent by user u
    val f = Array(m + 1, { BooleanArray(n) })
    f[0].fill(true)
    val msgXuser2prevUserId = Array(m + 1, { IntArray(n) })

    for (i in 1..m) {
      val line = sc.nextLine()
      val userName = line.substringBefore(":")
      val msg = line.substringAfter(":")
      msgs[i] = msg

      if (userName[0] == '?') {
        // unknown user
        for ((userName, userId) in userName2userId) {
          if (!doesMsgMentionUser(msg, userName)) { // if the user is a possible sender of the msg
            for (prevUserId in 0..n - 1) {
              if ((i == 1 || prevUserId != userId) && f[i - 1][prevUserId]) { // no two consecutive msgs can have the same user
                f[i][userId] = true
                msgXuser2prevUserId[i][userId] = prevUserId
              }
            }
          }
        }
      } else {
        // known user
        val userId = userName2userId[userName]!!
        for (prevUserId in 0..n - 1) {
          if ((i == 1 || prevUserId != userId) && f[i - 1][prevUserId]) { // no two consecutive msgs can have the same user
            f[i][userId] = true
            msgXuser2prevUserId[i][userId] = prevUserId
          }
        }
      }
    }

    for (u in 0..n - 1) {
      if (f[m][u]) {
        printSolution(msgXuser2prevUserId, u, m, msgs)
        return
      }
    }
    println("Impossible")
  }

  fun solve() {
    val tests = sc.nextLine().toInt()
    for (test in 1..tests) {
      solveTest()
    }
  }
}

fun main(args: Array<String>) {
  C.solve()
}
