/**
 * CodeForces
 * Codeforces Round #390 (Div. 2)
 * http://codeforces.com/contest/754
 * http://codeforces.com/contest/754/problem/D
 * D - Fedor and coupons
 *
 * Status: Accepted
 * Submission: http://codeforces.com/contest/754/submission/25060718
 * Date: 2017-02-26
 * Author: Svilen Marchev
 * Tags: sweeping, greedy, sorting, events, data structures, heap, priority queue
 *
 * Complexity: O(NlogN + NlogK)
 * Niceness: 9/10
 * Analysis: Create start and end events for each segment and sweep from left to right.
 * Maintain a heap, where you keep the largest K *end* coordinates of the currently overlapping
 * segments. See comments for details.
 */

import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.*

object D {
  object sc {
    val reader = BufferedReader(InputStreamReader(System.`in`))
    var tokenizer = StringTokenizer("")

    fun next(): String {
      while (!tokenizer.hasMoreTokens()) {
        val nextLine = reader.readLine() ?: throw IllegalStateException()
        tokenizer = StringTokenizer(nextLine)
      }
      return tokenizer.nextToken()
    }

    fun nextInt(): Int {
      return next().toInt()
    }
  }

  data class Segment(val start: Int, val end: Int)
  data class Event(val pos: Int, val balance: Int, val end: Int)

  fun solve() {
    val n = sc.nextInt()
    val k = sc.nextInt()
    val segments = ArrayList<Segment>(n)
    val events = ArrayList<Event>(2 * n)
    for (i in 0 until n) {
      val start = sc.nextInt()
      val end = sc.nextInt()
      events.add(Event(start, +1, end))
      events.add(Event(end, -1, end))
      segments.add(Segment(start, end))
    }

    // When two events happen at the same position, "start" events go first
    events.sort { e1, e2 -> if (e1.pos != e2.pos) e1.pos - e2.pos else e2.balance - e1.balance }

    // At each "start" event, we use a heap to get the minimum end position of the longest
    // currently overlapping segments. This is based on the observation that for the
    // optimal selection of K overlapping segments, the longest overlap will always begin
    // at the start position of some of the K segments and will always end at the end position
    // of some of the K segments.
    //
    // We cap the heap's size to K. At each "start" event we (assuming there are at least k
    // overlapping segments in the moment):
    // 1) add the end coordinate of the corresponding segment to the heap
    // 2) if the heap has more than k elements, remove the min element from it
    // 3) we get the min element from the heap (which is the min from the ends of the K
    // currently overlapping segments with max ends)
    val heap = PriorityQueue<Int>()
    // We maintain the balance at each moment, so that we know when we have at least k
    // overlapping segments. We cannot use the heap size for that, since there might
    // be some old events in there, which we don't want to delete in order to keep it simple.
    var balance = 0
    var maxOverlapSize = 0
    var maxOverlapStart = 0
    for (event in events) {
      balance += event.balance
      if (event.balance == +1) {
        heap.offer(event.end)
        if (heap.size > k) heap.poll()

        if (balance >= k) {
          val minEndOfMaxKCurrentSegments = heap.peek()
          if (maxOverlapSize < minEndOfMaxKCurrentSegments - event.pos + 1) {
            maxOverlapSize = minEndOfMaxKCurrentSegments - event.pos + 1
            maxOverlapStart = event.pos
          }
        }
      }
    }

    println(maxOverlapSize)
    val seq = StringJoiner(" ")
    if (maxOverlapSize == 0) {
      for (i in 1..k) {
        seq.add(i.toString())
      }
    } else {
      val maxCoverageEnd = maxOverlapStart + maxOverlapSize - 1
      var numAddedEvents = 0
      for (i in 0 until n) {
        if (numAddedEvents < k && segments[i].start <= maxOverlapStart && maxCoverageEnd <= segments[i].end) {
          seq.add((i + 1).toString())
          numAddedEvents++
        }
      }
    }
    println(seq)
  }
}

fun main(args: Array<String>) {
  D.solve()
}
