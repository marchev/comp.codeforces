import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.*

/**
 * CodeForces
 * Codeforces Round #390 (Div. 2)
 * http://codeforces.com/contest/754
 * http://codeforces.com/contest/754/problem/E
 * E - Dasha and cyclic table
 *
 * Status: Correct, but the Kotlin version is a bit too slow. See the Java version
 * Submission: http://codeforces.com/contest/754/submission/25065464
 * Date: 2017-02-26
 * Author: Svilen Marchev
 * Tags: brute force, cyclic tables, matrices, bit sets, optimization
 *
 * Complexity: O(N^4 / 64)
 * Niceness: 5/10
 * Analysis: http://codeforces.com/blog/entry/49637 (similar solution)
 * See the Java version for solution.
 */
object E {
  object sc {
    val reader = BufferedReader(InputStreamReader(System.`in`))
    var tokenizer = StringTokenizer("")

    fun next(): String {
      while (!tokenizer.hasMoreTokens()) {
        val nextLine = reader.readLine() ?: throw IllegalStateException()
        tokenizer = StringTokenizer(nextLine)
      }
      return tokenizer.nextToken()
    }

    fun nextInt(): Int {
      return next().toInt()
    }
  }

  class BitSet(var size: Int) {
    var words = LongArray((size + 63) / 64)

    fun setBit(i: Int) {
      words[i ushr 6] = words[i ushr 6] or (1L shl (i and 63))
    }

    fun unsetBit(i: Int) {
      words[i ushr 6] = words[i ushr 6] and (1L shl (i and 63)).inv()
    }

    fun getBit(i: Int): Boolean {
      return (words[i ushr 6] and ((1L shl (i and 63)))) != 0L
    }

    fun shiftRight() {
      val bit0 = words[0] and 1L
      words[0] = words[0] ushr 1
      for (i in 1 until words.size) {
        var wordFirstBit = words[i] and 1L
        words[i] = words[i] ushr 1
        words[i - 1] = words[i - 1] or (wordFirstBit shl 63)
      }
      if (bit0 != 0L) setBit(size - 1)
    }

    fun and(other: BitSet) {
      for (i in 0 until words.size) {
        words[i] = words[i].and(other.words[i])
      }
    }

    fun or(other: BitSet) {
      for (i in 0 until words.size) {
        words[i] = words[i].or(other.words[i])
      }
    }

    override fun toString(): String {
      val sb = StringBuilder("")
      for (i in 0 until size) {
        sb.append(if (getBit(i)) '1' else '0')
      }
      return sb.toString()
    }

    fun clone(): BitSet {
      val newBS = BitSet(0)
      newBS.words = words.clone()
      newBS.size = size
      return newBS
    }
  }

  fun solve() {
    val an = sc.nextInt()
    val am = sc.nextInt()
    val a = Array(an, { charArrayOf() })
    for (i in 0 until an) {
      a[i] = sc.next().toCharArray()
    }
    val tn = sc.nextInt()
    val tm = sc.nextInt()
    val t = Array(tn, { charArrayOf() })
    for (i in 0 until tn) {
      t[i] = sc.next().toCharArray()
    }

    val res = Array(an, { BitSet(am) })
    for (i in 0 until an) {
      for (j in 0 until am) {
        res[i].setBit(j)
      }
    }

    val H = Array(26, { Array(an, { Array(am, { BitSet(0) }) }) })
    for (c in 0 until 26) {
      for (i in 0 until an) {
        val bs = BitSet(am)
        for (j in 0 until am) {
          if (a[i][j] - 'a' == c) {
            bs.setBit(j)
          }
        }

        H[c][i][0] = bs
        for (shift in 1 until am) {
          H[c][i][shift] = H[c][i][shift - 1].clone()
          H[c][i][shift].shiftRight()
        }
      }
    }

    for (r in 0 until tn) {
      for (c in 0 until tm) {
        val tc = t[r][c]
        if (tc != '?') {
          val hh = H[tc - 'a']
          for (i in 0 until an) {
            res[i].and(hh[(i + r) % an][c % am])
          }
        }
      }
    }

    for (i in 0 until an) {
      println(res[i])
    }
  }
}

fun main(args: Array<String>) {
  E.solve()
}
