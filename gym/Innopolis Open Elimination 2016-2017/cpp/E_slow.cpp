#include <cstdio>
#include <map>
#include <vector>

using namespace std;

typedef long long LL;

#define MAXP   ((LL)1000000)

int main() {
  int n, time;
  scanf("%d%d", &n, &time);

  map<LL,int> timeXperiod2freq = map<LL,int>();

  for (int i = 0; i < n; ++i) {
    int start, period;
    scanf("%d%d", &start, &period);
    if (start <= time) {
      timeXperiod2freq[start * MAXP + period]++;
    }
  }

  for (int t = 1; t <= time; ++t) {
    // printf("t = %d\n", t);
    int sumFreq = 0;
    while (!timeXperiod2freq.empty()) {
      auto e = timeXperiod2freq.begin();  // smallest element, based on time
      int e_period = e->first % MAXP;
      int e_time = e->first / MAXP;
      int e_freq = e->second;
      if (e_time > t) break;

      // Delete immediately, before adding new elements
      timeXperiod2freq.erase(e);
      // printf(" t=%d per=%d fr=%d\n", e_time, e_period, e_freq);

      sumFreq += e_freq;
      if (t + e_period <= time) {
        timeXperiod2freq[(t + e_period) * MAXP + e_period] += e_freq;
      }
    }

    if (t > 1) printf(" ");
    printf("%d", sumFreq);
  }
  printf("\n");

  return 0;
}
