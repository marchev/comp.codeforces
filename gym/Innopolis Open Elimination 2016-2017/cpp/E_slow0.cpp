#include <cstdio>
#include <map>
#include <vector>

using namespace std;

void updatePeriod2FreqMap(map<int,int>& period2freq, int period, int balance) {
  period2freq[period] += balance;
}

int main() {
  int n, time;
  scanf("%d%d", &n, &time);

  vector<map<int,int> > time2periods = vector<map<int,int> >(time+1);

  for (int i = 0; i < n; ++i) {
    int start, period;
    scanf("%d%d", &start, &period);
    if (start <= time) {
      updatePeriod2FreqMap(time2periods[start], period, 1);
    }
  }

  for (int t = 1; t <= time; ++t) {
    const map<int,int>& periods2freq = time2periods[t];
    printf("t = %d\n", t);

    long long sumFreq = 0;
    for (const auto& entry : periods2freq) {
      printf(" (%d %d)\n", entry.first, entry.second);
      sumFreq += entry.second;
      if (t + entry.first <= time) {
        updatePeriod2FreqMap(time2periods[t + entry.first], entry.first, entry.second);
      }
    }

    if (t > 1) printf(" ");
    printf("%lld", sumFreq);
  }
  printf("\n");

  return 0;
}
