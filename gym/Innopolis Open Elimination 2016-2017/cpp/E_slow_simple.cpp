#include <cstdio>
#include <map>
#include <vector>

using namespace std;

typedef long long LL;

#define MAXP   ((LL)1000000)

int turns[MAXP];

int main() {
  int n, time;
  scanf("%d%d", &n, &time);

  for (int i = 0; i < n; ++i) {
    int start, period;
    scanf("%d%d", &start, &period);

    for (int t = start; t <= time; t += period) {
      turns[t]++;
    }
  }

  for (int t = 1; t <= time; ++t) {
    if (t > 1) printf(" ");
    printf("%d", turns[t]);
  }
  printf("\n");

  return 0;
}
