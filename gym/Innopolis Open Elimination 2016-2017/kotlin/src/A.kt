/**
 * CodeForces
 * Innopolis Open, Elimination round, 2016-2017
 * http://codeforces.com/gym/101182
 * A - Rock-Paper-Scissors for three
 *
 * Status: Accepted
 * Date: 2017-02-20
 * Author: Svilen Marchev
 * Tags: implementation
 */

import java.util.*

fun main(args : Array<String>) {
  val sc = Scanner(System.`in`)
  val tests = sc.nextInt()
  val a = sc.next()
  val b = sc.next()

  for (i in 0..tests-1) {
    var ai = Math.min(a[i].toInt(), b[i].toInt()).toChar()
    var bi = Math.max(a[i].toInt(), b[i].toInt()).toChar()
    val ab = "" + ai + bi
    print (when (ab) {
      "SS" -> "R"
      "RR" -> "P"
      "PP" -> "S"
      "RS" -> "R"
      "PR" -> "P"
      "PS" -> "S"
      else -> "."
    })
  }
  println()
}
