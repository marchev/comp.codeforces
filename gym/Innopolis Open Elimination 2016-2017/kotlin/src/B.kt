/**
 * CodeForces
 * Innopolis Open, Elimination round, 2016-2017
 * http://codeforces.com/gym/101182
 * B - Life in Innopolis
 *
 * Status: Accepted
 * Date: 2017-02-20
 * Author: Svilen Marchev
 * Tags: implementation, induction, sequences
 */

import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.*

object B {
  object sc {
    val reader = BufferedReader(InputStreamReader(System.`in`))
    var tokenizer = StringTokenizer("")

    fun next(): String {
      while (!tokenizer.hasMoreTokens()) {
        val nextLine = reader.readLine() ?: throw IllegalStateException()
        tokenizer = StringTokenizer(nextLine)
      }
      return tokenizer.nextToken()
    }
  }

  fun solve() {
    val s = sc.next()

    var ans = 0
    var wasLess = false
    for (i in s.length-2 downTo 0) {
      val a = s[i].toInt()
      val b = s[i+1].toInt()
      if (a < b || (a == b && wasLess)) {
        ans += 1
        wasLess = true
      } else {
        wasLess = false
      }
    }
    println(ans)
  }
}

fun main(args : Array<String>) {
  B.solve()
}
