/**
 * CodeForces
 * Innopolis Open, Elimination round, 2016-2017
 * http://codeforces.com/gym/101182
 * C - Barrels
 *
 * Status: Accepted
 * Date: 2017-02-20
 * Author: Svilen Marchev
 */

import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.*

object C {
  object sc {
    val reader = BufferedReader(InputStreamReader(System.`in`))
    var tokenizer = StringTokenizer("")

    fun next(): String {
      while (!tokenizer.hasMoreTokens()) {
        val nextLine = reader.readLine() ?: throw IllegalStateException()
        tokenizer = StringTokenizer(nextLine)
      }
      return tokenizer.nextToken()
    }

    fun nextInt(): Int {
      return next().toInt()
    }
  }

  fun solve() {
    val n = sc.nextInt()
    val l = IntArray(n + 2)
    val r = IntArray(n + 2)
    val c = IntArray(n + 2)
    for (i in 1..n) {
      l[i] = sc.nextInt()
      r[i] = sc.nextInt()
      c[i] = sc.nextInt()
    }

    val Fl = LongArray(n + 2)
    val Fr = LongArray(n + 2)
    var max = 0L
    for (i in n downTo 1) {
      Fr[i] = c[i] + Math.min(r[i].toLong(), Fr[i+1])
    }
    for (i in 1..n) {
      val F = c[i] + Math.min(l[i].toLong(), Fl[i-1]) + Math.min(r[i].toLong(), Fr[i+1])
      max = Math.max(F, max)

      Fl[i] = c[i] + Math.min(l[i].toLong(), Fl[i-1])
    }
    println(max)
  }
}

fun main(args : Array<String>) {
  C.solve()
}
