/**
 * CodeForces
 * Innopolis Open, Elimination round, 2016-2017
 * http://codeforces.com/gym/101182
 * D - Wedding cake
 *
 * Status: Accepted
 * Date: 2017-02-20
 * Author: Svilen Marchev
 * Tags: greedy, big integers
 */

import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.*

object D {
  object sc {
    val reader = BufferedReader(InputStreamReader(System.`in`))
    var tokenizer = StringTokenizer("")

    fun next(): String {
      while (!tokenizer.hasMoreTokens()) {
        val nextLine = reader.readLine() ?: throw IllegalStateException()
        tokenizer = StringTokenizer(nextLine)
      }
      return tokenizer.nextToken()
    }

    fun nextInt(): Int {
      return next().toInt()
    }
  }

  fun subtract(v: IntArray, p: Int): Boolean {
    v[p] -= 1
    if (v[p] < 0) {
      var i = p - 1
      while (i >= 0 && v[i] == 0) i -= 1
      if (i < 0) return false
      v[i] -= 1
      i += 1
      while (i < p) {
        v[i] += 9
        i += 1
      }
      v[p] += 10
    }
    return true
  }

  fun solve() {
    val n = sc.nextInt()
    val a = IntArray(n)
    for (i in 0..n - 1) {
      a[i] = sc.nextInt()
    }

    var amax = a.max() ?: 0
    if (a.count( { x -> x == amax }) == 1) {
      println("NO")
      return
    }

    // The "remaining" vector - represents the number 10^amax
    val rem = IntArray(amax + 1)
    rem[0] = 1

    // Subtract from 10^amax all small fractions i: 10^(amax-ai)
    for (i in 0..n - 1) {
      if (!subtract(rem, a[i])) {
        println("NO")
        return
      }
    }

    println("YES")

    var numPrintedAMax = 0
    for (i in 0..n - 1) {
      print("0.")
      if (a[i] == amax && numPrintedAMax < 2) {
        // The first time we print the smallest fraction, we print also all digits but the last from
        // the "remaining" vector. It is guaranteed this is possible, since all those digits are in
        // [0,9] and the smallest fraction has zeros there. The only non-0 digit of the smallest
        // fraction is its last position. Since its value is 1, we can add at that position only
        // up to 8. This means that if the "remaining" vector has value of 9 at its last position,
        // this needs to be added the next time we print the smallest fraction. Note that we have
        // guaranteed there are at least two smallest fractions (otherwise solution doesn't exist).
        if (numPrintedAMax == 0) {
          for (i in 1..amax-1) print(rem[i])
          println(1 + Math.min(8, rem[amax]))
        } else {
          for (i in 1..amax-1) print("0")
          println(1 + Math.max(0, rem[amax] - 8))
        }
        numPrintedAMax += 1
      } else {
        for (i in 1..a[i]-1) print("0")
        println("1")
      }
    }
  }
}

fun main(args : Array<String>) {
  D.solve()
}
