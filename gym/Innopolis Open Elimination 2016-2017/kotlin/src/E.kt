/**
 * CodeForces
 * Innopolis Open, Elimination round, 2016-2017
 * http://codeforces.com/gym/101182
 * E - Magical hourglass store
 *
 * Status: Accepted
 * Date: 2017-02-25
 * Author: Svilen Marchev
 * Tags: SQRT decomposition
 */

import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.*

object E {
  object sc {
    val reader = BufferedReader(InputStreamReader(System.`in`))
    var tokenizer = StringTokenizer("")

    fun next(): String {
      while (!tokenizer.hasMoreTokens()) {
        val nextLine = reader.readLine() ?: throw IllegalStateException()
        tokenizer = StringTokenizer(nextLine)
      }
      return tokenizer.nextToken()
    }

    fun nextInt(): Int {
      return next().toInt()
    }
  }

  data class Event(val start: Int, val period: Int)

  fun solve() {
    val n = sc.nextInt()
    val time = sc.nextInt()
    val sqrtTime = Math.ceil(Math.sqrt(time.toDouble())).toInt()

    var freq1 = IntArray(time + 1) // for subtask 1
    var events = ArrayList<Event>() // for subtask 2
    for (i in 0..n - 1) {
      val start = sc.nextInt()
      val period = sc.nextInt()

      if (period >= sqrtTime) { // subtask 1
        for (t in start..time step period) {
          freq1[t]++
        }
      } else { // save for subtask 2
        events.add(Event(start, period))
      }
    }

    // subtask 2
    events.sort { e1, e2 -> e1.start - e2.start }
    var eventsIx = 0

    var freq2 = IntArray(time + 1) // for subtask 2
    val timeXperiod2num = Array(sqrtTime, { IntArray(sqrtTime) })
    for (t in 1..time) {
      val tIx = t % sqrtTime
      while (eventsIx < events.size && events[eventsIx].start == t) {
        timeXperiod2num[tIx][events[eventsIx].period]++
        eventsIx++
      }

      for (period in 1..sqrtTime - 1) {  // periods of events in subtask 2 are in [1,sqrtTime-1]
        freq2[t] += timeXperiod2num[tIx][period]
        timeXperiod2num[(t + period) % sqrtTime][period] += timeXperiod2num[tIx][period]
        timeXperiod2num[tIx][period] = 0
      }
    }

    val freqSeq = StringJoiner(" ")
    for (t in 1..time) {
      val freq = freq1[t] + freq2[t]
      freqSeq.add(freq.toString())
    }
    println(freqSeq)
  }
}

fun main(args: Array<String>) {
  E.solve()
}
