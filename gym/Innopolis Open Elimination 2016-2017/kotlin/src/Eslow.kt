import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.*

object Eslow {
  object sc {
    val reader = BufferedReader(InputStreamReader(System.`in`))
    var tokenizer = StringTokenizer("")

    fun next(): String {
      while (!tokenizer.hasMoreTokens()) {
        val nextLine = reader.readLine() ?: throw IllegalStateException()
        tokenizer = StringTokenizer(nextLine)
      }
      return tokenizer.nextToken()
    }

    fun nextInt(): Int {
      return next().toInt()
    }
  }

  data class MutInt(var n: Int)

  fun updatePeriod2FreqMap(period2freq: LinkedHashMap<Int, MutInt>, period: Int, balance: Int) {
    val mutInt = period2freq.getOrPut(period, { MutInt(0) })
    mutInt.n += balance
  }

  fun solve() {
    val n = sc.nextInt()
    val time = sc.nextInt()
    val time2periods = Array(time + 1, { LinkedHashMap<Int, MutInt>() })
    for (i in 0..n - 1) {
      val start = sc.nextInt()
      val period = sc.nextInt()
      if (start <= time) {
        updatePeriod2FreqMap(time2periods[start], period, 1)
      }
    }

    for (t in 1..time) {
      val periods2freq = time2periods[t]
      time2periods[t] = LinkedHashMap<Int, MutInt>() // mark the big array for GC

      var sumFreq = 0L
      for ((period, freq) in periods2freq) {
        sumFreq += freq.n
        if (t + period <= time) {
          updatePeriod2FreqMap(time2periods[t + period], period, freq.n)
        }
      }

      if (t > 1) print(' ')
      print(sumFreq)
    }
    println()
  }
}

fun main(args: Array<String>) {
  Eslow.solve()
}
