/**
 * CodeForces
 * Codeforces Round #402 (Div. 1)
 * http://codeforces.com/contest/750/problems
 * A. New Year and Hurry
 *
 * Status: Accepted
 * Date: 2017-02-28
 * Author: Svilen Marchev
 * Tags: implementation
 */

package bg.svilen.codeforces;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Scanner;

public class TaskA {

  static int f(int n, int k) {
    int task = 1;
    int time = 0;
    while (task <= n) {
      time += 5 * task;
      if (time > 240 - k) {
        break;
      }
      task++;
    }
    return task - 1;
  }

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    Scanner sc = new Scanner(new BufferedReader(in));
    int n = sc.nextInt();
    int k = sc.nextInt();
    out.println(f(n, k));
  }
}
