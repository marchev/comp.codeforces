/**
 * CodeForces
 * Codeforces Round #402 (Div. 1)
 * http://codeforces.com/contest/750/problems
 * B. New Year and North Pole
 *
 * Status: Accepted
 * Niceness: 7/10
 * Date: 2017-02-28
 * Author: Svilen Marchev
 * Tags: implementation
 */

package bg.svilen.codeforces;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Scanner;

public class TaskB {

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    Scanner sc = new Scanner(new BufferedReader(in));
    int n = sc.nextInt();
    int pos = 0; // north
    for (int i = 0; i < n; ++i) {
      int len = sc.nextInt();
      String dir = sc.next();

      if (dir.charAt(0) == 'E' || dir.charAt(0) == 'W') {
        if (pos == 0 || pos == 20000) {
          out.println("NO");
          return;
        }
      } else if (dir.charAt(0) == 'S') {
        if (pos == 20000 || pos + len > 20000) {
          out.println("NO");
          return;
        }
        pos += len;
      } else {
        if (pos == 0 || pos - len < 0) {
          out.println("NO");
          return;
        }
        pos -= len;
      }
    }
    out.println(pos == 0 ? "YES" : "NO");
  }
}
