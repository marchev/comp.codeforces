/**
 * CodeForces
 * Codeforces Round #402 (Div. 1)
 * http://codeforces.com/contest/750/problems
 * B. New Year and North Pole
 *
 * Status: Accepted
 * Niceness: 9/10
 * Date: 2017-02-28
 * Author: Svilen Marchev
 * Tags: greedy, constraints, tightening
 */

package bg.svilen.codeforces;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Scanner;

public class TaskC {

  static final int MAXN = 200000;
  static final int MAXDIFF = 100;

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    Scanner sc = new Scanner(new BufferedReader(in));
    int n = sc.nextInt();

    int l = -1_000_000_000;
    int r = +1_000_000_000;
    for (int i = 0; i < n; ++i) {
      int diff = sc.nextInt();
      int div = sc.nextInt();
      if (div == 1) {
        l = Integer.max(l, 1900);
      } else {
        r = Integer.min(r, 1899);
      }
      if (l > r) {
        out.println("Impossible");
        return;
      }
      l += diff;
      r += diff;
    }

    if (r > MAXN * MAXDIFF + 1900) {
      out.println("Infinity");
    } else {
      out.println(r);
    }
  }
}
