/**
 * CodeForces
 * Codeforces Round #402 (Div. 1)
 * http://codeforces.com/contest/750/problems
 * D. New Year and Fireworks
 *
 * Status: Accepted
 * Niceness: 8/10
 * Date: 2017-02-28
 * Author: Svilen Marchev
 * Tags: DFS, graphs theory, grid
 */

package bg.svilen.codeforces;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Scanner;

public class TaskD {

  static final int MR = 310;
  static final int MN = 30;

  static final int[] shiftR = {-1, -1, 0, 1, 1, 1, 0, -1};
  static final int[] shiftC = {0, -1, -1, -1, 0, 1, 1, 1};

  int[] t = new int[MN];
  int n;

  boolean[][][][] u = new boolean[MR][MR][MN][8];
  boolean[][] w = new boolean[MR][MR];

  void dfs(int i, int j, int step, int dir) {
    if (step >= n) {
      return;
    }
    if (u[i][j][step][dir]) {
      return;
    }
    u[i][j][step][dir] = true;
    for (int dist = 1; dist <= t[step]; ++dist) {
      int ni = i + dist * shiftR[dir];
      int nj = j + dist * shiftC[dir];
      w[ni][nj] = true;
    }
    dfs(i + t[step] * shiftR[dir], j + t[step] * shiftC[dir], step + 1, (dir + 7) % 8);
    dfs(i + t[step] * shiftR[dir], j + t[step] * shiftC[dir], step + 1, (dir + 1) % 8);
  }

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    Scanner sc = new Scanner(new BufferedReader(in));
    n = sc.nextInt();
    for (int i = 0; i < n; ++i) {
      t[i] = sc.nextInt();
    }

    dfs(MR / 2, MR / 2, 0, 0);

    int ans = 0;
    for (int i = 0; i < MR; ++i) {
      for (int j = 0; j < MR; ++j) {
        ans += w[i][j] ? 1 : 0;
//        out.print(w[i][j] ? ". " : "  ");
      }
//      out.println();
    }
    out.println(ans);
  }
}
