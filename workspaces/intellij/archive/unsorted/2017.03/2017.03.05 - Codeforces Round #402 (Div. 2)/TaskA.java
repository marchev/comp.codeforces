/**
 * CodeForces
 * Codeforces Round #402 (Div. 2)
 * http://codeforces.com/contest/779/problems
 * A - Pupils Redistribution
 *
 * Status: Accepted
 * Submission: http://codeforces.com/contest/779/submission/25270437
 * Niceness: 7/10
 * Date: 2017-03-05
 * Author: Svilen Marchev
 * Tags: implementation, greedy
 */

package bg.svilen.codeforces;

import bg.svilen.comp.lib.io.SvScanner;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class TaskA {

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    SvScanner sc = new SvScanner(in);
    int n = sc.nextInt();
    int[] numsA = new int[6];
    int[] numsB = new int[6];
    for (int i = 0; i < n; ++i) {
      numsA[sc.nextInt()]++;
    }
    for (int i = 0; i < n; ++i) {
      numsB[sc.nextInt()]++;
    }

    int aToGive = 0;
    int bToGive = 0;
    for (int k = 1; k <= 5; ++k) {
      int total = numsA[k] + numsB[k];
      if (total % 2 == 1) {
        out.println("-1");
        return;
      }

      if (numsA[k] > numsB[k]) {
        aToGive += (numsA[k] - numsB[k]) / 2;
      } else if (numsB[k] > numsA[k]) {
        bToGive += (numsB[k] - numsA[k]) / 2;
      }
    }

    if (aToGive != bToGive) {
      out.println("-1");
    } else {
      out.println(aToGive);
    }
  }
}
