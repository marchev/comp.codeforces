/**
 * CodeForces
 * Codeforces Round #403 (Div. 2, based on Technocup 2017 Finals)
 * http://codeforces.com/contest/782/problems
 * A. Andryusha and Socks
 *
 * Status: Accepted
 * Submission: http://codeforces.com/contest/782/submission/25248680
 * Niceness: 5/10
 * Date: 2017-03-05
 * Author: Svilen Marchev
 * Tags: implementation, hash table
 */

package bg.svilen.codeforces;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;


public class TaskA {

  static class MyScanner {

    BufferedReader reader;
    StringTokenizer tokenizer = new StringTokenizer("");

    MyScanner(InputStreamReader in) {
      reader = new BufferedReader(in);
    }

    String next() {
      try {
        while (!tokenizer.hasMoreTokens()) {
          String nextLine = reader.readLine();
          if (nextLine == null) {
            throw new IllegalStateException("next line is null");
          }
          tokenizer = new StringTokenizer(nextLine);
        }
        return tokenizer.nextToken();
      } catch (IOException e) {
        throw new IllegalStateException(e);
      }
    }

    int nextInt() {
      return Integer.parseInt(next());
    }
  }

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    MyScanner sc = new MyScanner(in);
    int n = sc.nextInt();
    int max = 0;
    Set<Integer> table = new HashSet<>();
    for (int i = 0; i < 2*n; ++i) {
      int cur = sc.nextInt();
      if (table.contains(cur)) {
        table.remove(cur);
      } else {
        table.add(cur);
      }
      max = Integer.max(max, table.size());
    }
    out.println(max);
  }
}
