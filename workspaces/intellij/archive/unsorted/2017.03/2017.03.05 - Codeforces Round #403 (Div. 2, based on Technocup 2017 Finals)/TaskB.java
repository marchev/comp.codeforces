/**
 * CodeForces
 * Codeforces Round #403 (Div. 2, based on Technocup 2017 Finals)
 * http://codeforces.com/contest/782/problems
 * B. The Meeting Place Cannot Be Changed
 *
 * Status: Accepted
 * Submission: http://codeforces.com/contest/782/submission/25255756
 * Niceness: 7/10
 * Date: 2017-03-05
 * Author: Svilen Marchev
 * Tags: binary search, greedy, sweeping, constraints
 */

package bg.svilen.codeforces;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class TaskB {

  static final int MAXC = 1_000_000_100;
  static final double MAXCC = 1_200_000_000;

  static class MyScanner {

    BufferedReader reader;
    StringTokenizer tokenizer = new StringTokenizer("");

    MyScanner(InputStreamReader in) {
      reader = new BufferedReader(in);
    }

    String next() {
      try {
        while (!tokenizer.hasMoreTokens()) {
          String nextLine = reader.readLine();
          if (nextLine == null) {
            throw new IllegalStateException("next line is null");
          }
          tokenizer = new StringTokenizer(nextLine);
        }
        return tokenizer.nextToken();
      } catch (IOException e) {
        throw new IllegalStateException(e);
      }
    }

    int nextInt() {
      return Integer.parseInt(next());
    }
  }

  int[] s;
  int[] v;
  int n;

  boolean theyMeetAfter(double time) {
    double globalMin = 0;
    double globalMax = MAXC;

    for (int i = 0; i < n; ++i) {
      if (time > MAXCC / v[i]) {
        continue;
      }

      double reach = time * v[i];
      globalMin = Double.max(globalMin, s[i] - reach);
      globalMax = Double.min(globalMax, s[i] + reach);
      if (globalMin > globalMax) {
        return false;
      }
    }
    return true;
  }

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    MyScanner sc = new MyScanner(in);
    n = sc.nextInt();
    s = new int[n];
    v = new int[n];
    for (int i = 0; i < n; ++i) {
      s[i] = sc.nextInt();
    }
    for (int i = 0; i < n; ++i) {
      v[i] = sc.nextInt();
    }

    double l = 0;
    double r = MAXC;
    while (r - l > 1e-7) {
      double m = (l + r) / 2.0;
      if (theyMeetAfter(m)) {
        r = m;
      } else {
        l = m;
      }
    }
    out.print(r);
  }
}
