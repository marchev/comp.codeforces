/**
 * CodeForces
 * Codeforces Round #403 (Div. 2, based on Technocup 2017 Finals)
 * http://codeforces.com/contest/782/problems
 * C - Andryusha and Colored Balloons
 *
 * Status: Accepted
 * Submission: http://codeforces.com/contest/782/submission/25260049
 * Niceness: 7/10
 * Date: 2017-03-05
 * Author: Svilen Marchev
 * Tags: graphs, trees, bfs, graph coloring
 */

package bg.svilen.codeforces;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class TaskC {

  static class MyScanner {

    BufferedReader reader;
    StringTokenizer tokenizer = new StringTokenizer("");

    MyScanner(InputStreamReader in) {
      reader = new BufferedReader(in);
    }

    String next() {
      try {
        while (!tokenizer.hasMoreTokens()) {
          String nextLine = reader.readLine();
          if (nextLine == null) {
            throw new IllegalStateException("next line is null");
          }
          tokenizer = new StringTokenizer(nextLine);
        }
        return tokenizer.nextToken();
      } catch (IOException e) {
        throw new IllegalStateException(e);
      }
    }

    int nextInt() {
      return Integer.parseInt(next());
    }
  }

  final int MAXN = 200_100;

  ArrayList[] kids = new ArrayList[MAXN + 1];
  int[] col = new int[MAXN + 1];
  int[] par = new int[MAXN + 1];
  int[] queue = new int[MAXN + 1];
  int queueN = 0;
  int n;

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    MyScanner sc = new MyScanner(in);
    n = sc.nextInt();
    for (int i = 1; i <= n; ++i) {
      kids[i] = new ArrayList<Integer>();
    }
    for (int i = 1; i < n; ++i) {
      int a = sc.nextInt();
      int b = sc.nextInt();
      kids[a].add(b);
      kids[b].add(a);
    }

    col[1] = 1;
    queue[queueN++] = 1;

    for (int queueStart = 0; queueStart < queueN; queueStart++) {
      int v = queue[queueStart];
      int c = 1;
      for (int kid : (ArrayList<Integer>) kids[v]) {
        if (kid != par[v]) {
          par[kid] = v;
          queue[queueN++] = kid;

          while (c == col[v] || c == col[par[v]]) {
            c++;
          }
          col[kid] = c;
          c++;
        }
      }
    }

    int max = 1;
    for (int v = 1; v <= n; v++) {
      max = Integer.max(max, col[v]);
    }
    out.println(max);
    for (int v = 1; v <= n; v++) {
      if (v == n) {
        out.println(col[v]);
      } else {
        out.print(col[v] + " ");
      }
    }
  }
}
