/**
 * CodeForces
 * Codeforces Round #403 (Div. 2, based on Technocup 2017 Finals)
 * http://codeforces.com/contest/782/problems
 * D - Innokenty and a Football League
 *
 * Status: Accepted
 * Submission: http://codeforces.com/contest/782/submission/25268707
 * Niceness: 6/10
 * Date: 2017-03-05
 * Author: Svilen Marchev
 * Tags: greedy, constraints, graphs, bipartite matching, max flow
 */

/*
I solve it with a greedy algorithm, but can also be solved with bipartite matching:
"Can be modelled as a maximum bipartite matching, with all teams sharing the first option
having all their first options removed. This is a standard algorithm -- see Hopcroft-Karp
for an efficient algorithm."
http://codeforces.com/blog/entry/50804?#comment-347445
*/

package bg.svilen.codeforces;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.StringTokenizer;

public class TaskD {

  static class MyScanner {

    BufferedReader reader;
    StringTokenizer tokenizer = new StringTokenizer("");

    MyScanner(InputStreamReader in) {
      reader = new BufferedReader(in);
    }

    String next() {
      try {
        while (!tokenizer.hasMoreTokens()) {
          String nextLine = reader.readLine();
          if (nextLine == null) {
            throw new IllegalStateException("next line is null");
          }
          tokenizer = new StringTokenizer(nextLine);
        }
        return tokenizer.nextToken();
      } catch (IOException e) {
        throw new IllegalStateException(e);
      }
    }

    int nextInt() {
      return Integer.parseInt(next());
    }
  }

  static class Club {

    String short1, short2;
    int ix;
  }

  HashMap<String, ArrayList<Club>> map = new HashMap<>(); // group by short-name-1
  Club[] clubs;
  int n;

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    MyScanner sc = new MyScanner(in);
    n = sc.nextInt();
    clubs = new Club[n + 1];
    for (int i = 1; i <= n; ++i) {
      String a = sc.next();
      String b = sc.next();
      String short1 = new StringBuilder().append(a.charAt(0)).append(a.charAt(1))
          .append(a.charAt(2)).toString();
      String short2 = new StringBuilder().append(a.charAt(0)).append(a.charAt(1))
          .append(b.charAt(0)).toString();

      clubs[i] = new Club();
      clubs[i].short1 = short1;
      clubs[i].short2 = short2;
      clubs[i].ix = i;

      ArrayList<Club> existing = map.getOrDefault(short1, new ArrayList<>());
      existing.add(clubs[i]);
      map.putIfAbsent(short1, existing);
    }

    String[] chosenNames = new String[n + 1];
    HashSet<String> used = new HashSet<>();

    // STEP 1
    // If there are more than 1 clubs with the same short-name-1, we are forced to pick the
    // short-name-2 for each of them.
    for (Map.Entry<String, ArrayList<Club>> entry : map.entrySet()) {
      if (entry.getValue().size() <= 1) {
        continue;
      }

      for (Club club : entry.getValue()) {
        if (used.contains(club.short2)) {
          out.println("NO");
          return;
        }
        chosenNames[club.ix] = club.short2;
        used.add(club.short2);
      }
    }

    // STEP 2
    // It's important to observe that the remaining clubs have different short-name-1 and
    // therefore do not conflict between each other. But they can conflict with already chosen
    // short-name-2's in step 1. So we just iterate, looking for already set constraints,
    // and satisfy them one by one.
    boolean added;
    do {
      added = false;
      for (int i = 1; i <= n; ++i) {
        // If there is already a constraint to not use the short-name-1...
        if (chosenNames[i] == null && used.contains(clubs[i].short1)) {
          if (used.contains(clubs[i].short2)) {
            out.println("NO");
            return;
          }
          chosenNames[i] = clubs[i].short2;
          used.add(clubs[i].short2);
          added = true;
        }
      }
    } while (added);

    // STEP 3
    // Here have remained only clubs, for which we haven't been forced to choose their short-name-2.
    // Since their short-name-1's do not conflict between each other, we can just pick them.
    for (int i = 1; i <= n; ++i) {
      if (chosenNames[i] == null) {
        chosenNames[i] = clubs[i].short1;
      }
    }

    out.println("YES");
    for (int i = 1; i <= n; ++i) {
      out.println(chosenNames[i]);
    }
  }
}
