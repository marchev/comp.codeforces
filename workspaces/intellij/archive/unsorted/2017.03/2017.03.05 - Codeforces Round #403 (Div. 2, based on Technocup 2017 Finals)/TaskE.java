/**
 * CodeForces
 * Codeforces Round #403 (Div. 2, based on Technocup 2017 Finals)
 * http://codeforces.com/contest/782/problems
 * C. Underground Lab
 *
 * Status: Accepted
 * Submission: http://codeforces.com/contest/781/submission/25460363
 * Niceness: 6/10
 * Date: 2017-03-13
 * Author: Svilen Marchev
 * Tags: graphs, trees, dfs, spanning tree
 */

/*
From http://codeforces.com/blog/entry/50804?#comment-347500:
------
Put in an array the nodes following the visiting order of a dfs on the graph.
Then, for each contiguous block of ceil(2n / k) nodes in the array, assign a robot.
If a robot gets no node, just assign any node to it. This works because the dfs spanning
tree has exactly n - 1 edges and you visit each of them 2 times (going down and up);
hence, 2(n - 1) + 1 nodes will be visited.
-------
ceil(2n / k)*k >= 2n  > 2n-1, so there will be enough steps to cover all nodes using this approach.
*/

package bg.svilen.codeforces;

import bg.svilen.comp.lib.io.SvScanner;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

public class TaskE {

  final int MAXN = 200010;

  ArrayList<Integer>[] g = new ArrayList[MAXN + 1];
  int n, m, k;

  int maxRouteLen;
  ArrayList<Integer> curRoute;
  int numRoutesPrinted;
  boolean[] visited = new boolean[MAXN + 1];

  PrintWriter out;

  void printAndClearCurRoute() {
    out.print(curRoute.size());
    for (int v : curRoute) {
      out.print(' ');
      out.print(v);
    }
    out.println();

    curRoute.clear();
    numRoutesPrinted += 1;
  }

  void addToCurRoute(int v) {
    curRoute.add(v);
    if (curRoute.size() == maxRouteLen) {
      printAndClearCurRoute();
    }
  }

  void dfs(int v) {
    addToCurRoute(v);
    visited[v] = true;
    for (int kid : g[v]) {
      if (!visited[kid]) {
        dfs(kid);
        addToCurRoute(v);
      }
    }
  }

  public void solve(int testNumber, InputStreamReader in, PrintWriter out_) {
    out = out_;
    SvScanner sc = new SvScanner(in);
    n = sc.nextInt();
    m = sc.nextInt();
    k = sc.nextInt();
    for (int i = 1; i <= n; ++i) {
      g[i] = new ArrayList<>();
    }
    for (int i = 0; i < m; ++i) {
      int a = sc.nextInt();
      int b = sc.nextInt();
      g[a].add(b);
      g[b].add(a);
    }

    maxRouteLen = (2 * n + k - 1) / k;
    curRoute = new ArrayList<>(maxRouteLen);
    numRoutesPrinted = 0;

    dfs(1);

    if (!curRoute.isEmpty()) {
      printAndClearCurRoute();
    }
    while (numRoutesPrinted < k) {
      out.println("1 1");
      numRoutesPrinted += 1;
    }
  }
}
