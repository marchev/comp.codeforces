/**
 * CodeForces
 * Codeforces Round #403 (Div. 2, based on Technocup 2017 Finals)
 * http://codeforces.com/contest/782/problems
 * F. Axel and Marston in Bitland
 *
 * Status: Accepted
 * Submission: http://codeforces.com/contest/782/submission/25463998
 * Tutorial: http://codeforces.com/blog/entry/50854
 * Niceness: 5/10
 * Date: 2017-03-13
 * Author: Svilen Marchev
 * Tags: matrices, adjacency matrix, matrix powers, binary search, bitmasks
 */

package bg.svilen.codeforces;

import bg.svilen.comp.lib.ds.SvBitSet;
import bg.svilen.comp.lib.io.SvScanner;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class TaskF {

  final int MAXN = 512;
  final int MAX_POWER = 60; // min p, s.t. 2^p >= 10^18

  PrintWriter out;
  // A[p][i][j]=true iff there is a route of length 2^p between nodes i applyAnd j in the "straight" graph.
  // E.g. A[0][i][j]=true iff there is a direct pedestrian road between nodes i applyAnd j
  SvBitSet[][] A = new SvBitSet[64][];
  // Same as A, but for the "reversed" graph
  // E.g. B[0][i][j]=true iff there is a direct biking road between nodes i applyAnd j
  SvBitSet[][] B = new SvBitSet[64][];
  int n, m;

  public void solve(int testNumber, InputStreamReader in, PrintWriter out_) {
    out = out_;
    SvScanner sc = new SvScanner(in);
    n = sc.nextInt();
    m = sc.nextInt();
    for (int power = 0; power <= MAX_POWER; ++power) {
      A[power] = createEmpty();
      B[power] = createEmpty();
    }
    for (int i = 0; i < m; ++i) {
      int a = sc.nextInt() - 1;
      int b = sc.nextInt() - 1;
      int t = sc.nextInt();
      if (t == 0) {
        A[0][a].set(b);
      } else {
        B[0][a].set(b);
      }
    }

    tmpMultiplyBRev = createEmpty();
    for (int power = 1; power <= MAX_POWER; ++power) {
      multiply(A[power - 1], B[power - 1], A[power]);
      multiply(B[power - 1], A[power - 1], B[power]);
    }

    // Binary search
    SvBitSet[] W = createIdentity();
    SvBitSet[] Wnew = createEmpty();
    long ans = 0L;
    boolean useStraight = true;
    for (int power = MAX_POWER; power >= 0; --power) {
      if (useStraight) {
        multiply(W, A[power], Wnew);
      } else {
        multiply(W, B[power], Wnew);
      }
      if (existsPath(Wnew)) {
        SvBitSet[] swap = Wnew;
        Wnew = W;
        W = swap;

        ans += 1L << power;
        // After adding one more piece to the answer, the next part would start with
        // the graph in the opposite direction (i.e. the next piece would use the opposite direction
        // of the string).
        useStraight = !useStraight;
      }
    }

    if (ans > 1_000_000_000_000_000_000L) {
      out.println("-1");
    } else {
      out.println(ans);
    }
  }

  SvBitSet[] tmpMultiplyBRev;

  void multiply(SvBitSet[] A, SvBitSet[] B, SvBitSet[] R) {
    // Compute reverse of B
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < n; ++j) {
        tmpMultiplyBRev[j].set(i, B[i].get(j));
      }
    }
    // Multiply
    SvBitSet tmp = new SvBitSet(n);
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < n; ++j) {
        tmp.copyFrom(A[i]).applyAnd(tmpMultiplyBRev[j]);
        R[i].set(j, tmp.isAnySet());
      }
    }
  }

//  void multiplySlow(boolean[][] A, boolean[][] B, boolean[][] R) {
//    for (int i = 0; i < n; ++i) {
//      for (int j = 0; j < n; ++j) {
//        boolean Rij = false;
//        for (int k = 0; k < n && !Rij; ++k) {
//          Rij = A[i][k] && B[k][j];
//        }
//        R[i][j] = Rij;
//      }
//    }
//  }

  SvBitSet[] createIdentity() {
    SvBitSet[] E = createEmpty();
    for (int i = 0; i < n; ++i) {
      E[i].set(i);
    }
    return E;
  }

  SvBitSet[] createEmpty() {
    SvBitSet[] E = new SvBitSet[n];
    for (int i = 0; i < n; ++i) {
      E[i] = new SvBitSet(n);
    }
    return E;
  }

  // Does it exist a path from city 0 to any other city?
  boolean existsPath(SvBitSet[] A) {
    return A[0].isAnySet();
  }
}
