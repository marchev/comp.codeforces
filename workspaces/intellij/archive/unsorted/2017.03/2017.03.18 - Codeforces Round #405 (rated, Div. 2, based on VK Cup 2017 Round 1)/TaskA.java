/**
 * CodeForces
 * Codeforces Round #405 (rated, Div. 2, based on VK Cup 2017 Round 1)
 * http://codeforces.com/contest/791/problems
 * A - Bear and Big Brother
 *
 * Status: Accepted
 * Submission: http://codeforces.com/contest/791/submission/25612769
 * Niceness: 4/10
 * Date: 2017-03-18
 * Author: Svilen Marchev
 * Tags: implementation
 */

package bg.svilen.codeforces;

import bg.svilen.comp.lib.io.SvScanner;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class TaskA {

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    SvScanner sc = new SvScanner(in);
    long a = sc.nextInt();
    long b = sc.nextInt();
    int years = 0;
    while (a <= b) {
      a *= 3;
      b *= 2;
      years += 1;
    }
    out.println(years);
  }
}
