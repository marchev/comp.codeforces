/**
 * CodeForces
 * Codeforces Round #405 (rated, Div. 2, based on VK Cup 2017 Round 1)
 * http://codeforces.com/contest/791/problems
 * B - Bear and Friendship Condition
 *
 * Status: Accepted
 * Submission: http://codeforces.com/contest/791/submission/25612769
 * Niceness: 8/10
 * Date: 2017-03-18
 * Author: Svilen Marchev
 * Tags: graph, connected components, fully connected graphs, bfs
 */

/*
Analysis by Svilen Marchev:
--------------------
For every connected component:
 - compute the size of the component K
 - compute the sum of all degrees of vertices inside it S

The answer is YES, iff for each connected component holds that K(K-1) = S.
(Since each edge in the full subgraph is counted twice in S.)
*/

package bg.svilen.codeforces;

import bg.svilen.comp.lib.io.SvScanner;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

public class TaskB {

  final int MAXN = 150010;

  ArrayList<Integer>[] g = new ArrayList[MAXN];
  boolean[] u = new boolean[MAXN];
  int n, m;

  int[] q = new int[MAXN];
  int qs, qsz;

  boolean bfs(int start) {
    q[0] = start;
    qs = 0;
    qsz = 1;
    u[start] = true;
    for (; qs < qsz; qs++) {
      int v = q[qs];
      for (int kid : g[v]) {
        if (!u[kid]) {
          u[kid] = true;
          q[qsz++] = kid;
        }
      }
    }

    long clusterSz = qsz;
    long expectedNum = clusterSz * (clusterSz - 1);
    long actualNum = 0;
    for (int i = 0; i < qsz; ++i) {
      actualNum += g[q[i]].size();
    }
    return (actualNum == expectedNum);
  }

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    SvScanner sc = new SvScanner(in);
    n = sc.nextInt();
    m = sc.nextInt();
    for (int i = 0; i < n; ++i) {
      g[i] = new ArrayList<>();
    }
    for (int i = 0; i < m; ++i) {
      int a = sc.nextInt() - 1;
      int b = sc.nextInt() - 1;
      g[a].add(b);
      g[b].add(a);
    }

    for (int v = 0; v < n; ++v) {
      if (!u[v]) {
        if (!bfs(v)) {
          out.println("NO");
          return;
        }
      }
    }
    out.println("YES");
  }
}
