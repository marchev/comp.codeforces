/**
 * CodeForces
 * Codeforces Round #405 (rated, Div. 2, based on VK Cup 2017 Round 1)
 * http://codeforces.com/contest/791/problems
 * C. Bear and Different Names
 *
 * Status: Accepted
 * Submission: http://codeforces.com/contest/791/submission/25626568
 * Niceness: 8/10
 * Date: 2017-03-18
 * Author: Svilen Marchev
 * Tags: greedy, sweeping
 */

/*
Analysis by Svilen Marchev:
--------------------
Pretty neat one. Start by using k-1 different names. Then for the k-th name you can decide
based on the first note:
 - if it's "NO", then use the same name you used at the 1st position. (That way, on the next step
 there will be no duplication, since the 1st name would not count in the next subsequence of k
 names.)
 - if it's "YES", use another unique name.

At the next step the situation is similar - we have the last k-1 names being different and we can
apply the same logic for picking the next name.
*/

package bg.svilen.codeforces;

import bg.svilen.comp.lib.io.SvScanner;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

public class TaskC {

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    ArrayList<String> names = new ArrayList<>(26 * 26);
    for (char c = 'A'; c <= 'Z'; ++c) {
      for (char d = 'a'; d <= 'z'; ++d) {
        names.add("" + c + "" + d);
      }
    }

    SvScanner sc = new SvScanner(in);
    int n = sc.nextInt();
    int k = sc.nextInt();

    String[] assignment = new String[n];
    int nextNameIx = 0;
    for (int i = 0; i < k - 1; ++i) {
      assignment[i] = names.get(nextNameIx++);
    }
    for (int i = 0; i < n - k + 1; ++i) {
      String note = sc.next();
      if ("NO".equals(note)) {
        assignment[i + k - 1] = assignment[i];
      } else {
        assignment[i + k - 1] = names.get(nextNameIx++);
      }
    }

    for (int i = 0; i < n; ++i) {
      if (i == n - 1) {
        out.println(assignment[i]);
      } else {
        out.print(assignment[i] + " ");
      }
    }
  }
}
