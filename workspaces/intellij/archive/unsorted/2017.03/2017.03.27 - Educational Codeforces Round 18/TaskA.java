package bg.svilen.codeforces;

import bg.svilen.comp.lib.io.SvScanner;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TaskA {

  /**
   * CodeForces
   * Educational Codeforces Round 18
   * http://codeforces.com/contest/792/problems
   * A. New Bus Route
   *
   * Status: Accepted
   * Submission: ?
   * Niceness: 4/10
   * Date: 2017-03-17
   * Author: Svilen Marchev
   * Tags: implementation, sorting, sweeping
   */

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    SvScanner sc = new SvScanner(in);
    int n = sc.nextInt();
    List<Integer> a = new ArrayList<>(n);
    for (int i = 0; i < n; ++i) {
      a.add(sc.nextInt());
    }

    Collections.sort(a);
    int minDist = a.get(1) - a.get(0);
    int cnt = 1;
    for (int i = 2; i < n; ++i) {
      int d = a.get(i) - a.get(i - 1);
      if (d < minDist) {
        minDist = d;
        cnt = 1;
      } else if (d == minDist) {
        cnt++;
      }
    }
    out.print(minDist + " " + cnt);
  }
}
