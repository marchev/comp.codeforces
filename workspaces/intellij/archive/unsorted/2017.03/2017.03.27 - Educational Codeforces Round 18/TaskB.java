/**
 * CodeForces
 * Educational Codeforces Round 18
 * http://codeforces.com/contest/792/problems
 * B. Counting-out Rhyme
 *
 * Status: Accepted
 * Submission: http://codeforces.com/contest/792/submission/25845409
 * Niceness: 4/10
 * Date: 2017-03-27
 * Author: Svilen Marchev
 * Tags: implementation, cyclic lists, simulation, linked lists
 */

package bg.svilen.codeforces;

import bg.svilen.comp.lib.io.SvScanner;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

public class TaskB {

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    SvScanner sc = new SvScanner(in);
    int n = sc.nextInt();
    int k = sc.nextInt();

    ArrayList<Integer> arr = new ArrayList<>(n);
    for (int i = 0; i < n; ++i) {
      arr.add(i + 1);
    }
    int leaderIx = 0;

    ArrayList<Integer> killOrder = new ArrayList<>(n);

    for (int i = 0; i < k; ++i) {
      int cycles = sc.nextInt();
      int killIx = (leaderIx + cycles) % arr.size();
      killOrder.add(arr.get(killIx));
      arr.remove(killIx);
      leaderIx = killIx % arr.size();
//      out.println("new leader:"+arr.get(leaderIx));
//      out.println(arr);
    }

    for (int i = 0; i < killOrder.size(); ++i) {
      if (i == killOrder.size() - 1) {
        out.println(killOrder.get(i));
      } else {
        out.print(killOrder.get(i) + " ");
      }
    }
  }
}
