/**
 * CodeForces
 * Educational Codeforces Round 18
 * http://codeforces.com/contest/792/problems
 * C. Divide by Three
 *
 * Status: Accepted
 * Submission: http://codeforces.com/contest/792/submission/25850317
 * Niceness: 5/10
 * Date: 2017-03-27
 * Author: Svilen Marchev
 * Tags: dp, divisibility
 */

package bg.svilen.codeforces;

import bg.svilen.comp.lib.io.SvScanner;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

public class TaskC {

  final int MAXN = 100100;
  final int INF = 10000000;

  int[] a = new int[MAXN];
  int n;

  int[][] f = new int[MAXN][3];
  int[][] prev = new int[MAXN][3];
  int[][] action = new int[MAXN][3];

  void computeAnswer(PrintWriter out) {
    ArrayList<Integer> ans = new ArrayList<>(n);
    int i = n, rem = 0;
    while (i > 0 && rem >= 0) {
      if (action[i][rem] == 1) {
        ans.add(a[i]);
        break;
      } else if (action[i][rem] == 2) {
        ans.add(a[i]);
      }
      rem = prev[i][rem];
      i -= 1;
    }
    for (int h = ans.size() - 1; h >= 0; --h) {
      out.print(ans.get(h));
    }
    out.println();
  }

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    SvScanner sc = new SvScanner(in);
    String as = sc.next();
    n = as.length();
    boolean has0 = false;
    for (int i = 1; i <= n; ++i) {
      a[i] = as.charAt(i - 1) - '0';
      has0 |= a[i] == 0;
    }

    for (int rem = 0; rem < 3; ++rem) {
      f[0][rem] = INF;
      prev[0][rem] = -2;
    }
    for (int i = 1; i <= n; ++i) {
      for (int rem = 0; rem < 3; ++rem) {
        int fval = 1 + f[i - 1][rem];
        int pval = rem;
        int aval = 0; // skip

        if (a[i] != 0 && a[i] % 3 == rem) {
          if (fval > i - 1) {
            fval = i - 1;
            pval = -1;
            aval = 1; // start
          }
        }

        int rem0 = (rem - a[i] + 33) % 3;
        if (fval > f[i - 1][rem0]) {
          fval = f[i - 1][rem0];
          pval = rem0;
          aval = 2; // keep
        }

        f[i][rem] = fval;
        prev[i][rem] = pval;
        action[i][rem] = aval;
//        out.println(i+","+rem+": "+ fval);
      }
    }

    int minVal = f[n][0];
    if (minVal < INF) {
//      out.println(minVal);
      computeAnswer(out);
    } else if (has0) {
      // Special case: there is a 0 in s
      out.println("0");
    } else {
      out.println("-1");
    }
  }
}
