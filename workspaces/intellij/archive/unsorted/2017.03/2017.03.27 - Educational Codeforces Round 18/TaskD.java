/**
 * CodeForces
 * Educational Codeforces Round 18
 * http://codeforces.com/contest/792/problems
 * D. Paths in a Complete Binary Tree
 *
 * Status: Accepted
 * Submission: http://codeforces.com/contest/792/submission/25854021
 * Niceness: 7/10
 * Date: 2017-03-27
 * Author: Svilen Marchev
 * Tags: trees, binary trees, complete binary tree, infix numbering
 */

package bg.svilen.codeforces;

import bg.svilen.comp.lib.io.SvScanner;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class TaskD {

  SvScanner sc;
  long n;

  long u;
  String com;
  char[] st = new char[100100];
  int stsz;

  void compute(PrintWriter out) {
    long v = (n + 1) / 2;
    long add = 0;

    stsz = 0;

    while (v + add != u) {
      if (u < v + add) {
        v = v / 2;
        st[stsz++] = 'L';
      } else {
        add += v;
        v = v / 2;
        st[stsz++] = 'R';
      }
    }

    for (int comIx = 0; comIx < com.length(); ++comIx) {
      char c = com.charAt(comIx);
      if (c == 'L' && v > 1) {
        v = v / 2;
        st[stsz++] = 'L';
      }
      if (c == 'R' && v > 1) {
        add += v;
        v = v / 2;
        st[stsz++] = 'R';
      }
      if (c == 'U' && v != (n + 1) / 2) {
        char last = st[stsz - 1];
        stsz--;
        if (last == 'L') {
          v *= 2;
        }
        if (last == 'R') {
          v *= 2;
          add -= v;
        }
      }
    }

    out.println(v + add);
  }

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    sc = new SvScanner(in);
    n = sc.nextLong();
    int q = sc.nextInt();
    for (int i = 0; i < q; ++i) {
      u = sc.nextLong();
      com = sc.next();
      compute(out);
    }
  }
}
