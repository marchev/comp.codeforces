package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;

/**
 * CodeForces
 * Educational Codeforces Round 18
 * http://codeforces.com/contest/792/problem/E
 * E. Colored Balls
 * <p>
 * Status: Accepted
 * Submission: http://codeforces.com/contest/792/submission/35455084
 * Niceness: 6/10
 * Difficulty: 7/10
 * Date: 2018-02-18
 * Author: Svilen Marchev
 * Tags: math
 * Analysis: http://codeforces.com/blog/entry/51254
 * <p>
 * Let's settle some random box, e.g. a_0. If we want to divide all balls from a_0 into k subsets with sizes
 * s and s + 1, then either s<=sqrt(a_0) (case 1) or k<=sqrt(a_0) (case 2). The solution just tries both these
 * cases by iterating through all possible s=0,...,sqrt(a_0) (case 1) and all possible k=1,...,sqrt(a_0),
 * checking if a solution is possible, and if so, how many subsets it would result into.
 * <p>
 * Time: O(N sqrt A0).
 */
public class TaskE {
  int[] a;
  int n;

  /**
   * Computes into how many subsets in total would have to be split the N sets,
   * so that each subset is either of size s or s+1. If such a split is not possible, returns INF.
   */
  long calcNumSetsWhenSizeIs(int s) {
    long numSets = 0;
    for (int i = 0; i < n; ++i) {
      numSets += (a[i] + s) / (s + 1);
      if (a[i] % (s + 1) == 0) continue;
      if (a[i] / (s + 1) >= s - a[i] % (s + 1)) continue;
      return Long.MAX_VALUE;
    }
    return numSets;
  }

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    n = in.readInt();
    a = new int[n];
    for (int i = 0; i < n; ++i) {
      a[i] = in.readInt();
    }

    int sqrtA0 = Math.min((int) Math.sqrt(a[0]) + 100, a[0]);

    long minNumSets = Long.MAX_VALUE;

    // case 1: the set size s is <= sqrt(a0)
    for (int s = 0; s <= sqrtA0; ++s) {
      minNumSets = Math.min(minNumSets, calcNumSetsWhenSizeIs(s));
    }

    // case 2: the number of subsets k that a0 would be split into is <= sqrt(a0)
    for (int k = 1; k <= sqrtA0; ++k) {
      int s = a[0] / k;
      minNumSets = Math.min(minNumSets, calcNumSetsWhenSizeIs(s));
      if (a[0] % k == 0) {
        minNumSets = Math.min(minNumSets, calcNumSetsWhenSizeIs(s - 1));
      }
    }

    out.println(minNumSets);
  }
}
