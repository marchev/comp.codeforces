package bg.svilen.codeforces;

import bg.svilen.comp.lib.io.SvScanner;
import java.io.InputStreamReader;
import java.io.PrintWriter;

/**
 * CodeForces
 * Educational Codeforces Round 29
 * http://codeforces.com/contest/863/problems
 * A. Quasi-palindrome
 *
 * Status: Accepted
 * Niceness: 6/10
 * Date: 2017-12-09
 * Author: Svilen Marchev
 * Tags: implementation, sequences, palindromes
 * Analysis: http://codeforces.com/blog/entry/54708
 */
public class TaskA {

  boolean isPalindrom(String s) {
    return s.equals(new StringBuilder(s).reverse().toString());
  }

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    SvScanner sc = new SvScanner(in);
    int n = sc.nextInt();

    String s = Integer.toString(n);
    for (int i = 0; i < 10; ++i) {
      if (isPalindrom(s)) {
        out.println("YES");
        return;
      }
      s = "0" + s;
    }
    out.println("NO");
  }
}
