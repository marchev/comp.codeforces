package bg.svilen.codeforces;

import bg.svilen.comp.lib.io.SvScanner;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;

/**
 * CodeForces
 * Educational Codeforces Round 29
 * http://codeforces.com/contest/863/problems
 * B. Kayaking
 *
 * Status: Accepted
 * Niceness: 7/10
 * Date: 2017-12-09
 * Author: Svilen Marchev
 * Tags: greedy
 * Analysis: http://codeforces.com/blog/entry/54708
 */
public class TaskB {

  int n;
  int[] w;

  void excludeAndSort(int[] z, int i, int j) {
    int zz = 0;
    for (int k = 0; k < n; ++k) {
      if (k != i && k != j) {
        z[zz++] = w[k];
      }
    }
    Arrays.sort(z);
  }

  int calcInstab(int[] z) {
    int instab = 0;
    for (int i = 0; i < z.length / 2; ++i) {
      instab += z[2 * i + 1] - z[2 * i];
    }
    return instab;
  }

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    SvScanner sc = new SvScanner(in);
    n = sc.nextInt() * 2;
    w = new int[n];
    for (int i = 0; i < n; ++i) {
      w[i] = sc.nextInt();
    }

    int[] z = new int[n - 2];
    int minInstab = Integer.MAX_VALUE;
    for (int i = 0; i < n; ++i) {
      for (int j = i + 1; j < n; ++j) {
        excludeAndSort(z, i, j);
        minInstab = Math.min(minInstab, calcInstab(z));
      }
    }
    out.println(minInstab);
  }
}
