package bg.svilen.codeforces;

import bg.svilen.comp.lib.io.SvScanner;
import java.io.InputStreamReader;
import java.io.PrintWriter;

/**
 * CodeForces
 * Educational Codeforces Round 29
 * http://codeforces.com/contest/863/problems
 * C. 1-2-3
 *
 * Status: Accepted
 * Niceness: 5/10
 * Date: 2017-12-09
 * Author: Svilen Marchev
 * Tags: sequences, cycles, memoization, graph
 * Analysis: http://codeforces.com/blog/entry/54708
 */
public class TaskC {

  long k;
  int startA, startB;
  int[][] nextA = new int[3][3];
  int[][] nextB = new int[3][3];

  int[][] winningPlayer = {
      {0, 2, 1},
      {1, 0, 2},
      {2, 1, 0},
  };

  Long[][] stateOccurrence = new Long[3][3];
  long[][] stateScoreA = new long[3][3];
  long[][] stateScoreB = new long[3][3];

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    SvScanner sc = new SvScanner(in);
    k = sc.nextLong();
    startA = sc.nextInt() - 1;
    startB = sc.nextInt() - 1;
    for (int i = 0; i < 3; ++i) {
      for (int j = 0; j < 3; ++j) {
        nextA[i][j] = sc.nextInt() - 1;
      }
    }
    for (int i = 0; i < 3; ++i) {
      for (int j = 0; j < 3; ++j) {
        nextB[i][j] = sc.nextInt() - 1;
      }
    }

    long scoreA = 0;
    long scoreB = 0;
    int a = startA;
    int b = startB;
    for (long timeNow = 0; timeNow < k; ++timeNow) {
      int winner = winningPlayer[a][b];
      Long lastTimeSeen = stateOccurrence[a][b];
      if (lastTimeSeen != null) {
        long toCycleLen = lastTimeSeen;
        long cycleLen = timeNow - lastTimeSeen;
        long cycleReps = (k - toCycleLen) / cycleLen;

        long cycleScoreA = scoreA - stateScoreA[a][b] + (winner == 1 ? 1 : 0);
        long cycleScoreB = scoreB - stateScoreB[a][b] + (winner == 2 ? 1 : 0);
        // We have already added the score of the first loop, so don't include it now
        scoreA += (cycleReps - 1) * cycleScoreA;
        scoreB += (cycleReps - 1) * cycleScoreB;

        timeNow += (cycleReps - 1) * cycleLen;
        if (timeNow >= k) {
          break;
        }
      }

//      out.println((a + 1) + " " + (b + 1) + " -> " + winner);
      scoreA += winner == 1 ? 1 : 0;
      scoreB += winner == 2 ? 1 : 0;

      stateOccurrence[a][b] = timeNow;
      stateScoreA[a][b] = scoreA;
      stateScoreB[a][b] = scoreB;

      int newA = nextA[a][b];
      int newB = nextB[a][b];
      a = newA;
      b = newB;
    }

    out.println(scoreA + " " + scoreB);
  }
}
