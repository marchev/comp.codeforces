package bg.svilen.codeforces;

import bg.svilen.comp.lib.io.SvScanner;
import java.io.InputStreamReader;
import java.io.PrintWriter;

/**
 * CodeForces
 * Educational Codeforces Round 29
 * http://codeforces.com/contest/863/problems
 * D - Yet Another Array Queries Problem
 *
 * Status: Accepted
 * Niceness: 7/10
 * Date: 2017-12-10
 * Author: Svilen Marchev
 * Tags: queries, reconstruction, sequence
 * Analysis: http://codeforces.com/blog/entry/54708
 */
public class TaskD {

  static class Query {

    int type;
    int l, r;
  }

  int n, q, m;
  int[] a;
  int[] b;
  Query[] queries;

  /**
   * Go back through the queries backwards and reconstruct the original index.
   */
  int computeOriginalIxOf(int ix) {
    for (int i = q - 1; i >= 0; --i) {
      if (queries[i].l <= ix && ix <= queries[i].r) {
        if (queries[i].type == 2) { // reverse
          ix = queries[i].r - (ix - queries[i].l);
        } else {  // right shift, which we negate by left shift
          ix = ix == queries[i].l ? queries[i].r : ix - 1;
        }
      }
    }
    return ix;
  }

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    SvScanner sc = new SvScanner(in);
    n = sc.nextInt();
    q = sc.nextInt();
    m = sc.nextInt();
    a = new int[n];
    for (int i = 0; i < n; ++i) {
      a[i] = sc.nextInt();
    }
    queries = new Query[q];
    for (int i = 0; i < q; ++i) {
      Query query = new Query();
      query.type = sc.nextInt();
      query.l = sc.nextInt() - 1;
      query.r = sc.nextInt() - 1;
      queries[i] = query;
    }
    b = new int[m];
    for (int i = 0; i < m; ++i) {
      b[i] = sc.nextInt() - 1;
    }

    for (int i = 0; i < m; ++i) {
      int origIx = computeOriginalIxOf(b[i]);
      out.print(a[origIx]);
      out.print(i == m - 1 ? "\n" : " ");
    }
  }
}
