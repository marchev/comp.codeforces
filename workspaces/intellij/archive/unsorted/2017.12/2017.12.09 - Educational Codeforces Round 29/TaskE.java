package bg.svilen.codeforces;

import bg.svilen.comp.lib.debug.SvLogger;
import bg.svilen.comp.lib.io.SvScanner;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

/**
 * CodeForces
 * Educational Codeforces Round 29
 * http://codeforces.com/contest/863/problems
 * E. Turn Off The TV
 *
 * Status: Accepted
 * Submission: http://codeforces.com/contest/863/submission/33081039
 * Niceness: 8/10
 * Date: 2017-12-09
 * Author: Svilen Marchev
 * Tags: interval tree, compression, intervals, sorting
 * Analysis: http://codeforces.com/blog/entry/54708
 */
public class TaskE {

  final int N = 1 << 20; // to fit ~600000 coordinates

  List<Ival> ivals;
  int n;

  static class Ival {

    int l, r;
  }

  static class Node {

    int timesCovered;
    int minimallyCoveredSection;
  }

  Node[] tree = new Node[2 * N];

  void insertIval(int nodeIx, int treeL, int treeR, Ival ival) {
    if (ival.l <= treeL && treeR <= ival.r) {
      tree[nodeIx].timesCovered += 1;
      return;
    }
    int treeM = (treeL + treeR) / 2;
    if (ival.l <= treeM) {
      insertIval(nodeIx * 2, treeL, treeM, ival);
    }
    if (treeM + 1 <= ival.r) {
      insertIval(nodeIx * 2 + 1, treeM + 1, treeR, ival);
    }
  }

  void updateTree(int nodeIx, int treeL, int treeR, int timesCoveredByUpperNodes) {
    if (treeL == treeR) {
      tree[nodeIx].minimallyCoveredSection = tree[nodeIx].timesCovered + timesCoveredByUpperNodes;
      return;
    }
    int treeM = (treeL + treeR) / 2;
    updateTree(nodeIx * 2, treeL, treeM, timesCoveredByUpperNodes + tree[nodeIx].timesCovered);
    updateTree(nodeIx * 2 + 1, treeM + 1, treeR,
        timesCoveredByUpperNodes + tree[nodeIx].timesCovered);

    tree[nodeIx].minimallyCoveredSection = Math.min(tree[2 * nodeIx].minimallyCoveredSection,
        tree[2 * nodeIx + 1].minimallyCoveredSection);
  }

  int query(int nodeIx, int treeL, int treeR, Ival ival) {
    if (ival.l <= treeL && treeR <= ival.r) {
      return tree[nodeIx].minimallyCoveredSection;
    }
    int val = Integer.MAX_VALUE;
    int treeM = (treeL + treeR) / 2;
    if (ival.l <= treeM) {
      val = Math.min(val, query(nodeIx * 2, treeL, treeM, ival));
    }
    if (treeM + 1 <= ival.r) {
      val = Math.min(val, query(nodeIx * 2 + 1, treeM + 1, treeR, ival));
    }
    return val;
  }

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    SvLogger log = new SvLogger().setPrintTimeEllapsed(true).setEnabled(false);

    SvScanner sc = new SvScanner(in);
    n = sc.nextInt();
    ivals = new ArrayList<>(n);
    List<Integer> uncompressedCoords = new ArrayList<>(2 * n);
    for (int i = 0; i < n; ++i) {
      Ival ival = new Ival();
      ival.l = sc.nextInt();
      ival.r = sc.nextInt();
      ivals.add(ival);

      uncompressedCoords.add(ival.l);
      uncompressedCoords.add(ival.r);
      // very important!, so that [1,2],[3,4] is compressed differently than [1,2],[4,5]
      uncompressedCoords.add(ival.l - 1);
    }
    log.p("input read");
    ArrayList<Integer> deduped = new ArrayList<>(new HashSet<>(uncompressedCoords));
    log.p("built deduped");
    Collections.sort(deduped);
    log.p("sorted deduped");
    // Remap coords to compress
    for (int i = 0; i < n; ++i) {
      ivals.get(i).l = Collections.binarySearch(deduped, ivals.get(i).l);
      ivals.get(i).r = Collections.binarySearch(deduped, ivals.get(i).r);
//      out.println(ivals.get(i).l +" "+ivals.get(i).r);
    }
    log.p("compressed");

    // Build tree and add all ivals
    for (int i = 1; i < 2 * N; ++i) {
      tree[i] = new Node();
    }
    for (int i = 0; i < n; ++i) {
      insertIval(1, 0, N - 1, ivals.get(i));
    }
    log.p("ivals added");

    // Build the minimally covered value for each node
    updateTree(1, 0, N - 1, 0);
    log.p("tree updated");

    // Check by how many other ivals each ival is covered. If more than 1, it can be an answer.
    for (int i = 0; i < n; ++i) {
      int minimallyCoveredSection = query(1, 0, N - 1, ivals.get(i));
//      out.println(ivals.get(i).l + "," + ivals.get(i).r + " -> " + minimallyCoveredSection);
      if (minimallyCoveredSection > 1) {
        out.println(i + 1);
        return;
      }
    }
    out.println("-1");
    log.p("ivals checked");
  }
}
