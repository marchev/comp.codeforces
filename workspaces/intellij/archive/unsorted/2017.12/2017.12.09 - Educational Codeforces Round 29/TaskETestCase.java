package bg.svilen.codeforces;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Collections;
import java.util.Random;
import net.egork.chelper.task.Test;
import net.egork.chelper.tester.TestCase;

public class TaskETestCase {

  @TestCase
  public Collection<Test> createTests() {
    System.out.println("! called createTests");
//    Test test = createRandomTest();
    Test test = createPerformanceTest();
    return Collections.singleton(test);
  }

  private Test createRandomTest() {
    StringWriter sw = new StringWriter();
    PrintWriter writer = new PrintWriter(sw);
    int n = 200;
    writer.println(n);
    Random rand = new Random(29);
    for (int i = 0; i < n; ++i) {
      int l = rand.nextInt(1000);
      int r = rand.nextInt(1000);
      if (l > r) {
        int tmp = l;
        l = r;
        r = tmp;
      }
      writer.println(l + " " + r);
    }
    writer.flush();

    String input = sw.toString();
    String output = "-1\n";
    return new Test(input, output);
  }


  private Test createPerformanceTest() {
    StringWriter sw = new StringWriter();
    PrintWriter writer = new PrintWriter(sw);
    int n = 200000;
    writer.println(n);
    for (int i = 0; i < n; ++i) {
      writer.println((2 * i) + " " + (2 * i + 1));
    }
    writer.flush();

    String input = sw.toString();
    String output = "-1\n";
    return new Test(input, output);
  }
}
