package bg.svilen.codeforces;

import bg.svilen.comp.lib.io.SvScanner;
import net.egork.collections.Pair;
import net.egork.graph.Graph;
import net.egork.graph.MinCostFlow;
import net.egork.misc.ArrayUtils;

import java.io.InputStreamReader;
import java.io.PrintWriter;

/**
 * CodeForces
 * Educational Codeforces Round 29
 * http://codeforces.com/contest/863/problems
 * F - Almost Permutation
 *
 * Status: Accepted
 * Submission: http://codeforces.com/contest/863/submission/33964749
 * Niceness: 8/10
 * Date: 2018-01-06
 * Author: Svilen Marchev
 * Tags: graphs, flows, min-cost max flow
 * Analysis: http://codeforces.com/blog/entry/54708
 */
public class TaskF {

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    SvScanner sc = new SvScanner(in);
    int n = sc.nextInt();
    int q = sc.nextInt();

    int[] lowerBound = ArrayUtils.createArray(n + 1, 1);
    int[] upperBound = ArrayUtils.createArray(n + 1, n);
    for (int i = 0; i < q; ++i) {
      int t = sc.nextInt();
      int l = sc.nextInt();
      int r = sc.nextInt();
      int limit = sc.nextInt();

      for (int x = l; x <= r; ++x) {
        if (t == 1) {
          lowerBound[x] = Math.max(lowerBound[x], limit);
        } else {
          upperBound[x] = Math.min(upperBound[x], limit);
        }
      }
    }

    Graph graph = new Graph(2 * n + 2, 2 * n * n + n);
    for (int pos = 1; pos <= n; ++pos) {
      if (lowerBound[pos] > upperBound[pos]) {
        out.println("-1");
        return;
      }
      for (int x = lowerBound[pos]; x <= upperBound[pos]; ++x) {
        graph.addFlowWeightedEdge(pos, n + x, 0, 1);
      }
    }
    for (int x = 1; x <= n; ++x) {
      for (int cnt = 1; cnt <= n; ++cnt) {
        int cost = 2 * cnt - 1;
        graph.addFlowWeightedEdge(n + x, 2 * n + 1, cost, 1);
      }
    }
    for (int pos = 1; pos <= n; ++pos) {
      graph.addFlowWeightedEdge(0, pos, 0, 1);
    }

    Pair<Long, Long> pair = MinCostFlow.minCostMaxFlow(graph, 0, 2 * n + 1, false, n);
    out.println(pair.first);
  }
}
