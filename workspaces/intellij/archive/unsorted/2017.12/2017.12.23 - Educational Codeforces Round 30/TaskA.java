package bg.svilen.codeforces;

import bg.svilen.comp.lib.io.SvScanner;

import java.io.InputStreamReader;
import java.io.PrintWriter;

/**
 * CodeForces
 * Educational Codeforces Round 30
 * http://codeforces.com/contest/873/problems
 * A. Chores
 *
 * Status: Accepted
 * Niceness: 5/10
 * Date: 2017-12-23
 * Author: Svilen Marchev
 * Tags: implementation
 * Analysis: http://codeforces.com/blog/entry/55171
 */
public class TaskA {
  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    SvScanner sc = new SvScanner(in);
    int n = sc.nextInt();
    int k = sc.nextInt();
    int x = sc.nextInt();
    int sum = k * x;
    for (int i = 0; i < n-k; ++i) {
      sum += sc.nextInt();
    }
    out.println(sum);
  }
}
