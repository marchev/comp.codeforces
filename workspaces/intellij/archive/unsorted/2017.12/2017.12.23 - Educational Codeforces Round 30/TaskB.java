package bg.svilen.codeforces;

import bg.svilen.comp.lib.io.SvScanner;

import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashMap;

/**
 * CodeForces
 * Educational Codeforces Round 30
 * http://codeforces.com/contest/873/problems
 * B. Balanced Substring
 *
 * Status: Accepted
 * Niceness: 8/10
 * Date: 2017-12-23
 * Author: Svilen Marchev
 * Tags: dp, sequences, balance, strings
 * Analysis: http://codeforces.com/blog/entry/55171
 */
public class TaskB {
  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    SvScanner sc = new SvScanner(in);
    int n = sc.nextInt();
    String s = sc.next();

    HashMap<Integer, Integer> bal2Ix = new HashMap<>(n);
    bal2Ix.put(0, 0);
    int balance = 0;
    int ans = 0;
    for (int i = 0; i < n; ++i) {
      balance += s.charAt(i) == '0' ? -1 : +1;
      Integer lastIxWithThatBalance = bal2Ix.putIfAbsent(balance, i+1);
      if (lastIxWithThatBalance != null) {
        ans = Math.max(ans, i+1 - lastIxWithThatBalance);
      }
    }
    out.println(ans);
  }
}
