package bg.svilen.codeforces;

import bg.svilen.comp.lib.io.SvScanner;

import java.io.InputStreamReader;
import java.io.PrintWriter;

/**
 * CodeForces
 * Educational Codeforces Round 30
 * http://codeforces.com/contest/873/problems
 * C. Strange Game On Matrix
 *
 * Status: Accepted
 * Niceness: 8/10
 * Date: 2017-12-23
 * Author: Svilen Marchev
 * Tags: sweeping, dp, partial sums
 * Analysis: http://codeforces.com/blog/entry/55171
 */
public class TaskC {

  int[][] a;
  int n, m, k;

  int[] sum;

  int next1InColumn(int j, int startI) {
    for (int i = startI; i <= n; ++i) {
      if (a[i][j] == 1) {
        return i;
      }
    }
    return n + 1;
  }

  int calcSumOfNextKInColumn(int startI) {
    if (startI > n) return 0;
    return sum[Math.min(startI + k - 1, n)] - sum[startI - 1];
  }

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    SvScanner sc = new SvScanner(in);
    n = sc.nextInt();
    m = sc.nextInt();
    k = sc.nextInt();
    a = new int[n + 1][m + 1];
    for (int i = 1; i <= n; ++i) {
      for (int j = 1; j <= m; ++j) {
        a[i][j] = sc.nextInt();
      }
    }

    int ansScore = 0;
    int ansRemoved = 0;
    sum = new int[n + 1];

    for (int j = 1; j <= m; ++j) {
      sum[0] = 0;
      for (int i = 1; i <= n; ++i) {
        sum[i] = sum[i - 1] + a[i][j];
      }

      int maxScoreInCol = Integer.MIN_VALUE;
      int minRemovedInCol = Integer.MAX_VALUE;
      int rowWith1 = 0;
      for (int numRemoved = 0; rowWith1 <= n; ++numRemoved) {
        rowWith1 = next1InColumn(j, rowWith1 + 1);
        int sumOfNextK = calcSumOfNextKInColumn(rowWith1);
        if (maxScoreInCol < sumOfNextK) {
          maxScoreInCol = sumOfNextK;
          minRemovedInCol = numRemoved;
        }
      }

      ansScore += maxScoreInCol;
      ansRemoved += minRemovedInCol;
    }

    out.println(ansScore + " " + ansRemoved);
  }
}
