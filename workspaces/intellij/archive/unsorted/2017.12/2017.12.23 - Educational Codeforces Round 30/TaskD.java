package bg.svilen.codeforces;

import bg.svilen.comp.lib.io.SvScanner;

import java.io.InputStreamReader;
import java.io.PrintWriter;

/**
 * CodeForces
 * Educational Codeforces Round 30
 * http://codeforces.com/contest/873/problems
 * C. Strange Game On Matrix
 *
 * Status: Accepted
 * Niceness: 8/10
 * Date: 2017-12-24
 * Author: Svilen Marchev
 * Tags: merge sort, reconstruction, sequences, recursion
 * Analysis: http://codeforces.com/blog/entry/55171
 */
public class TaskD {

  int[] a;
  int n, k;

  int operationsLeft;
  boolean isSolved = false;
  boolean hasSolution = false;

  void reverse(int l, int r) {
    r--; // Convert [l,r) to [l,r-1]
    while (l < r) {
      int tmp = a[l];
      a[l] = a[r];
      a[r] = tmp;
      l++;
      r--;
    }
  }

  void build(int l, int r) {
    int len = r - l;
    int mid = (r + l) / 2;

    if (isSolved) {
      if (hasSolution) {
        // Sort this subsequence, so that it doesn't require additional mergesort calls.
        reverse(l, r);
      }
      return;
    }

    if (operationsLeft == 0) {
      isSolved = true;
      hasSolution = true;
      // Sort this subsequence, so that it doesn't require additional mergesort calls.
      reverse(l, r);
      return;
    }
    if (operationsLeft < 0) {
      isSolved = true;
      hasSolution = false;
      return;
    }

    if (len == 1) {
      return;
    }

    // Account for the 2 calls needed to sort the left and right parts.
    operationsLeft -= 2;
    build(l, mid);
    build(mid, r);
  }

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    SvScanner sc = new SvScanner(in);
    n = sc.nextInt();
    k = sc.nextInt();

    a = new int[n];
    for (int i = 0; i < n; ++i) {
      a[i] = n - i;
    }

    operationsLeft = k - 1; // Subtract the first mergesort call.
    build(0, n);

    if (hasSolution) {
      for (int i = 0; i < n; ++i) {
        out.print(a[i] + (i == n - 1 ? "\n" : " "));
      }
    } else {
      out.println("-1");
    }
  }
}
