package bg.svilen.codeforces;


import bg.svilen.comp.lib.io.SvScanner;
import bg.svilen.comp.lib.sequences.ReadOnlySequenceRmq;

import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Comparator;

/**
 * CodeForces
 * Educational Codeforces Round 30
 * http://codeforces.com/contest/873/problems
 * 873E - Awards For Contestants
 * <p>
 * Status: Accepted
 * Submission: http://codeforces.com/contest/873/submission/34163907
 * Niceness: 8/10
 * Date: 2018-01-14
 * Author: Svilen Marchev
 * Tags: RMQ, range minimum query, optimization of the inner most loop
 * Analysis: http://codeforces.com/blog/entry/55171
 * <p>
 * Make two loops, but instead of a third one, use RMQ to query for the maximum difference.
 */
public class TaskE {

  static class Student {
    int tasks;
    int id;
  }

  Student[] students;
  int n;

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    SvScanner sc = new SvScanner(in);
    n = sc.nextInt();
    students = new Student[n];
    for (int i = 0; i < n; ++i) {
      students[i] = new Student();
      students[i].tasks = sc.nextInt();
      students[i].id = i;
    }
    Arrays.sort(students, Comparator.comparing(value -> -value.tasks));

    int[] diffs = new int[n];
    for (int i = 0; i < n - 1; ++i) {
      diffs[i] = students[i].tasks - students[i + 1].tasks;
    }
    diffs[n - 1] = students[n - 1].tasks;

    int[] diffsRev = new int[n];
    for (int i = 0; i < n; ++i) {
      diffsRev[i] = -diffs[i];
    }
    ReadOnlySequenceRmq rmq = new ReadOnlySequenceRmq(diffsRev);

    int maxDf1 = Integer.MIN_VALUE;
    int maxDf2 = Integer.MIN_VALUE;
    int maxDf3 = Integer.MIN_VALUE;
    int maxDfC1 = 0, maxDfC2 = 0, maxDfC3 = 0;

    for (int c1 = 1; c1 <= n - 2; ++c1) {
      for (int c2 = 1; c1 + c2 <= n - 1; ++c2) {
        if (c1 <= 2 * c2 && c2 <= 2 * c1) {
          int df1 = diffs[c1 - 1];
          if (maxDf1 > df1) continue;
          int df2 = diffs[c1 + c2 - 1];
          if (maxDf1 == df1 && maxDf2 > df2) continue;

          int minC3 = Math.max(Math.max((c1 + 1) / 2, (c2 + 1) / 2), 1);
          int maxC3 = Math.min(Math.min(2 * c1, 2 * c2), n - c1 - c2);
          int left = c1 + c2 + minC3 - 1;
          int right = c1 + c2 + maxC3 - 1;
          if (left <= right) {
            int lastOfBronzeIx = rmq.queryMinIndex(left, right);
            int df3 = diffs[lastOfBronzeIx];
            if (maxDf1 == df1 && maxDf2 == df2 && maxDf3 > df3) continue;
            maxDf1 = df1;
            maxDf2 = df2;
            maxDf3 = df3;
            maxDfC1 = c1;
            maxDfC2 = c2;
            maxDfC3 = lastOfBronzeIx + 1 - c1 - c2;
          }
        }
      }
    }

    int[] medals = new int[n];
    Arrays.fill(medals, -1);
    int k = 0;
    for (int i = 0; i < maxDfC1; ++i) {
      medals[students[k++].id] = 1;
    }
    for (int i = 0; i < maxDfC2; ++i) {
      medals[students[k++].id] = 2;
    }
    for (int i = 0; i < maxDfC3; ++i) {
      medals[students[k++].id] = 3;
    }
    for (int i = 0; i < n; ++i) {
      out.print(medals[i] + (i == n - 1 ? "\n" : " "));
    }
  }
}
