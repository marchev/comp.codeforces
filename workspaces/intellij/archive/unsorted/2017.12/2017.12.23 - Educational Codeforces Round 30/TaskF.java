package bg.svilen.codeforces;

import bg.svilen.comp.lib.io.SvScanner;
import bg.svilen.comp.lib.string.SvSuffixAutomaton;
import bg.svilen.comp.lib.string.SvSuffixAutomatonUtils;
import net.egork.generated.collections.list.IntList;

import java.io.InputStreamReader;
import java.io.PrintWriter;

/**
 * CodeForces
 * Educational Codeforces Round 30
 * http://codeforces.com/contest/873/problems
 * F. Forbidden Indices
 *
 * Status: Accepted
 * Submission: http://codeforces.com/contest/873/submission/33991735
 * Niceness: 8/10
 * Date: 2018-01-07
 * Author: Svilen Marchev
 * Tags: suffix automaton, suffix array, suffix ds, strings, dp
 * Analysis: http://codeforces.com/blog/entry/55171
 */
public class TaskF {
  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    SvScanner sc = new SvScanner(in);
    int n = sc.nextInt();
    String s = sc.next();
    String t = sc.next();

    SvSuffixAutomaton automaton = new SvSuffixAutomaton(n);
    long[] cnt = new long[SvSuffixAutomaton.getMaxStates(n)];
    for (int i = 0; i < s.length(); ++i) {
      int state = automaton.add(s.charAt(i) - 'a');
      cnt[state] = t.charAt(i) == '0' ? 1 : 0;
    }

    IntList order = SvSuffixAutomatonUtils.getStatesInTopologicalOrder(automaton);
    long ans = 0;
    for (int state : order) {
      ans = Math.max(ans, (long) cnt[state] * automaton.length[state]);

      // Update the parent. It will be processed later.
      int parent = automaton.link[state];
      if (parent != -1) {
        cnt[parent] += cnt[state];
      }
    }
    out.println(ans);
  }
}
