package bg.svilen.codeforces;

import bg.svilen.comp.lib.io.SvScanner;

import java.io.InputStreamReader;
import java.io.PrintWriter;

/**
 * CodeForces
 * Hello 2018
 * http://codeforces.com/contest/913
 * A - Modular Exponentiation
 * <p>
 * Status: Accepted
 * Note: Solved during the contest.
 * Niceness: 7/10
 * Date: 2018-01-08
 * Author: Svilen Marchev
 * Tags: remainders, exponentiation
 * Analysis: ?
 */
public class TaskA {

  long expWithCap(int n) {
    long res = 1;
    for (int i = 1; i <= n; ++i) {
      if (res > 10000000000L) break;
      res *= 2;
    }
    return res;
  }

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    SvScanner sc = new SvScanner(in);
    int n = sc.nextInt();
    long m = sc.nextInt();

    out.println(m % expWithCap(n));
  }
}
