package bg.svilen.codeforces;

import bg.svilen.comp.lib.io.SvScanner;
import net.egork.graph.Graph;

import java.io.InputStreamReader;
import java.io.PrintWriter;

/**
 * CodeForces
 * Hello 2018
 * http://codeforces.com/contest/913
 * B - Christmas Spruce
 * <p>
 * Status: Accepted
 * Note: Solved during the contest.
 * Niceness: 7/10
 * Date: 2018-01-08
 * Author: Svilen Marchev
 * Tags: graph, tree, DAG, DFS, DP
 * Analysis: ?
 */
public class TaskB {

  boolean[] isLeaf;
  int[] numLeafs;
  int n;
  Graph g;

  void dfs(int v) {
    if (g.firstOutbound(v) == -1) {
      isLeaf[v] = true;
      return;
    }
    for (int e = g.firstOutbound(v); e != -1; e = g.nextOutbound(e)) {
      int u = g.destination(e);
      dfs(u);
      if (isLeaf[u]) {
        numLeafs[v]++;
      }
    }
  }

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    SvScanner sc = new SvScanner(in);
    n = sc.nextInt();
    g = new Graph(n, n - 1);

    for (int i = 1; i <= n - 1; ++i) {
      int parent = sc.nextInt() - 1;
      g.addSimpleEdge(parent, i);
    }

    isLeaf = new boolean[n];
    numLeafs = new int[n];
    dfs(0);

    boolean isSpruce = true;
    for (int i = 0; i < n; ++i) {
      if (!isLeaf[i] && numLeafs[i] < 3) {
        isSpruce = false;
        break;
      }
    }
    out.println(isSpruce ? "Yes" : "No");
  }
}
