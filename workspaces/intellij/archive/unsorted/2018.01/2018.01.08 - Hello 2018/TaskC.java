package bg.svilen.codeforces;

import bg.svilen.comp.lib.io.SvScanner;

import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;

/**
 * CodeForces
 * Hello 2018
 * http://codeforces.com/contest/913
 * C - Party Lemonade
 * <p>
 * Status: Accepted
 * Note: Solved during the contest.
 * Niceness: 7/10
 * Date: 2018-01-08
 * Author: Svilen Marchev
 * Tags: greedy, math
 * Analysis: ?
 */
public class TaskC {

  int n, L;
  Bottle[] bottles;

  static class Bottle implements Comparable<Bottle> {
    int cost;
    int cap;

    @Override
    public int compareTo(Bottle o) {
      long v1 = (long) cost * o.cap;
      long v2 = (long) cap * o.cost;
      return Long.compare(v1, v2);
    }
  }

  long f(long remCap, int i) {
    if (remCap <= 0) {
      return 0;
    }
    long times = remCap / bottles[i].cap;
    long v1 = f(remCap - times * bottles[i].cap, i + 1) + times * bottles[i].cost;
    long v2 = (times + 1) * bottles[i].cost;
    return Math.min(v1, v2);
  }

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    SvScanner sc = new SvScanner(in);
    n = sc.nextInt();
    L = sc.nextInt();
    bottles = new Bottle[n];
    for (int i = 0; i < n; ++i) {
      bottles[i] = new Bottle();
      bottles[i].cost = sc.nextInt();
      bottles[i].cap = 1 << i;
    }
    Arrays.sort(bottles);

    out.println(f(L, 0));
  }
}
