package bg.svilen.codeforces;

import bg.svilen.comp.lib.io.SvScanner;

import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;

/**
 * CodeForces
 * Hello 2018
 * http://codeforces.com/contest/913
 * D - Too Easy Problems
 * <p>
 * Status: Accepted
 * Note: Solved during the contest.
 * Niceness: 8/10
 * Date: 2018-01-08
 * Author: Svilen Marchev
 * Tags: binary search for answer, greedy
 * Analysis: ?
 */
public class TaskD {

  static class Problem implements Comparable<Problem> {
    int a, t;
    int id;

    @Override
    public int compareTo(Problem o) {
      return Integer.compare(t, o.t);
    }
  }

  int n, TIME;
  Problem[] allProblems;

  Problem[] problems;
  int problemsSz;

  int canDo(int targetNumTasks) {
    problemsSz = 0;
    for (int i = 0; i < n; ++i) {
      if (allProblems[i].a >= targetNumTasks) {
        problems[problemsSz++] = allProblems[i];
      }
    }
    problemsSz = Math.min(problemsSz, targetNumTasks);

    int numTasks = 0;
    int time = 0;
    for (int i = 0; i < problemsSz; ++i) {
      time += problems[i].t;
      if (time > TIME) break;
      numTasks++;
    }
    return numTasks;
  }

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    SvScanner sc = new SvScanner(in);
    n = sc.nextInt();
    TIME = sc.nextInt();
    allProblems = new Problem[n];
    for (int i = 0; i < n; ++i) {
      allProblems[i] = new Problem();
      allProblems[i].a = sc.nextInt();
      allProblems[i].t = sc.nextInt();
      allProblems[i].id = i + 1;
    }
    Arrays.sort(allProblems);

    problems = new Problem[n];

    int l = 0;
    int r = n + 1;
    while (l < r - 1) {
      int m = (l + r) / 2;
      if (canDo(m) == m) {
        l = m;
      } else {
        r = m;
      }
    }

    int numToSolve = canDo(l);
    out.println(l);
    out.println(numToSolve);
    for (int i = 0; i < numToSolve; ++i) {
      out.print(problems[i].id + (i == numToSolve - 1 ? "\n" : " "));
    }
  }
}
