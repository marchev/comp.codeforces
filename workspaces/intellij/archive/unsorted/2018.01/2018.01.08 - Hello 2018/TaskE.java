package bg.svilen.codeforces;

import bg.svilen.comp.lib.debug.SvLogger;
import bg.svilen.comp.lib.io.SvScanner;

import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * CodeForces
 * Hello 2018
 * http://codeforces.com/contest/913
 * E - Logical Expression
 * <p>
 * Status: Accepted
 * Niceness: 9/10
 * Date: 2018-01-08
 * Author: Svilen Marchev
 * Tags: DP, grammar, generation, expressions, logical expressions, brute force, greedy
 * Analysis: ?
 */
public class TaskE {

  SvLogger log = new SvLogger().setEnabled(false);

  // Memoization
  HashMap<Integer, Map<Integer, String>> memE = new HashMap<>();
  HashMap<Integer, Map<Integer, String>> memT = new HashMap<>();
  HashMap<Integer, Map<Integer, String>> memF = new HashMap<>();

  void putIfBetter(Map<Integer, String> map, int descriptor, String formula) {
    String existingFormula = map.putIfAbsent(descriptor, formula);
    if (existingFormula == null) return;
    if (formula.length() < existingFormula.length() ||
        (formula.length() == existingFormula.length() && formula.compareTo(existingFormula) < 0)) {
      map.put(descriptor, formula); // overwrite
    }
  }

  //  E ::= E '|' T | T
  Map<Integer, String> genE(int maxLen) {
    if (memE.containsKey(maxLen)) return memE.get(maxLen);

    Map<Integer, String> ret = new HashMap<>(genT(maxLen));
    if (maxLen >= 3) {
      for (int e = 1; e <= maxLen - 2; ++e) {
        Map<Integer, String> subexpsE = genE(e);
        Map<Integer, String> subexpsT = genT(maxLen - 1 - e);
        for (Map.Entry entry1 : subexpsE.entrySet()) {
          for (Map.Entry entry2 : subexpsT.entrySet()) {
            putIfBetter(ret,
                (int) entry1.getKey() | (int) entry2.getKey(),
                entry1.getValue() + "|" + entry2.getValue());
          }
        }
      }
    }

    memE.put(maxLen, ret);
    return ret;
  }

  //  T ::= T '&' F | F
  Map<Integer, String> genT(int maxLen) {
    if (memT.containsKey(maxLen)) return memT.get(maxLen);

    Map<Integer, String> ret = new HashMap<>(genF(maxLen));
    if (maxLen >= 3) {
      for (int t = 1; t <= maxLen - 2; ++t) {
        Map<Integer, String> subexpsT = genT(t);
        Map<Integer, String> subexpsF = genF(maxLen - 1 - t);
        for (Map.Entry entry1 : subexpsT.entrySet()) {
          for (Map.Entry entry2 : subexpsF.entrySet()) {
            putIfBetter(ret,
                (int) entry1.getKey() & (int) entry2.getKey(),
                entry1.getValue() + "&" + entry2.getValue());
          }
        }
      }
    }

    memT.put(maxLen, ret);
    return ret;
  }

  //  F ::= '!' F | '(' E ')' | 'x' | 'y' | 'z'
  Map<Integer, String> genF(int maxLen) {
    if (memF.containsKey(maxLen)) return memF.get(maxLen);

    Map<Integer, String> ret = new HashMap<>();
    if (maxLen >= 1) {
      ret.put(15, "x"); // 00001111 in decimal
      ret.put(51, "y"); // 00110011 in decimal
      ret.put(85, "z"); // 01010101 in decimal
    }
    if (maxLen >= 2) {
      Map<Integer, String> subexps = genF(maxLen - 1);
      for (Map.Entry<Integer, String> entry : subexps.entrySet()) {
        putIfBetter(ret, (~entry.getKey()) & ((1 << 8) - 1), "!" + entry.getValue());
      }
    }
    if (maxLen >= 3) {
      Map<Integer, String> subexps = genE(maxLen - 2);
      for (Map.Entry<Integer, String> entry : subexps.entrySet()) {
        putIfBetter(ret, entry.getKey(), "(" + entry.getValue() + ")");
      }
    }

    memF.put(maxLen, ret);
    return ret;
  }

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    // The limit is chosen so that it generates formulas for all the 256 descriptors.
    Map<Integer, String> formulas = genE(28);
    log.p(formulas);
    log.p(formulas.size());

    SvScanner sc = new SvScanner(in);
    int n = sc.nextInt();
    for (int i = 0; i < n; ++i) {
      String binary = sc.next();
      int descriptor = 0; // represent the descriptor via a bitmask
      for (int k = 0; k < 8; ++k) {
        if (binary.charAt(k) == '1') {
          descriptor |= 1 << (7 - k);
        }
      }
      out.println(formulas.get(descriptor));
    }
  }
}
