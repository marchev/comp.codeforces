package bg.svilen.codeforces;

import bg.svilen.comp.lib.io.SvScanner;

import java.io.InputStreamReader;
import java.io.PrintWriter;

/**
 * CodeForces
 * Educational Codeforces Round 36
 * http://codeforces.com/contest/915/problems
 * A. Garden
 * <p>
 * Status: Accepted
 * Note: Solved during contest
 * Submission: http://codeforces.com/contest/915/submission/34129990
 * Niceness: 4/10
 * Date: 2018-01-13
 * Author: Svilen Marchev
 * Tags: math, modulo, exponentiation
 * Analysis: http://codeforces.com/blog/entry/57123
 */
public class TaskA {
  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    SvScanner sc = new SvScanner(in);
    int n = sc.nextInt();
    int k = sc.nextInt();
    int min = Integer.MAX_VALUE;
    for (int i = 0; i < n; ++i) {
      int a = sc.nextInt();
      if (k % a == 0) {
        min = Math.min(min, k / a);
      }
    }
    out.println(min);
  }
}
