package bg.svilen.codeforces;

import bg.svilen.comp.lib.io.SvScanner;
import net.egork.misc.ArrayUtils;

import java.io.InputStreamReader;
import java.io.PrintWriter;

/**
 * CodeForces
 * Educational Codeforces Round 36
 * http://codeforces.com/contest/915/problems
 * B. Browser
 * <p>
 * Status: Accepted
 * Submission: http://codeforces.com/contest/915/submission/35187587
 * Niceness: 6/10
 * Difficulty: 3/10
 * Date: 2018-02-12
 * Author: Svilen Marchev
 * Tags: greedy
 * Analysis: http://codeforces.com/blog/entry/57123
 */
public class TaskB {
  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    SvScanner sc = new SvScanner(in);
    int n = sc.nextInt();
    int pos = sc.nextInt() - 1;
    int l = sc.nextInt() - 1;
    int r = sc.nextInt() - 1;

    int ans;
    if (l == 0 && r == n - 1) ans = 0;
    else if (l == 0 && r != n - 1) ans = Math.abs(r - pos) + 1;
    else if (l != 0 && r == n - 1) ans = Math.abs(l - pos) + 1;
    else ans = Math.min(Math.abs(r - pos), Math.abs(l - pos)) + (r - l) + 2;
    out.println(ans);
  }
}
