package bg.svilen.codeforces;

import bg.svilen.comp.lib.io.SvScanner;
import net.egork.misc.ArrayUtils;

import java.io.InputStreamReader;
import java.io.PrintWriter;

/**
 * CodeForces
 * Educational Codeforces Round 36
 * http://codeforces.com/contest/915/problems
 * B. Browser
 * <p>
 * Status: Accepted
 * Note: Solved during contest
 * Submission: http://codeforces.com/contest/915/submission/34136234
 * Niceness: 6/10
 * Date: 2018-01-13
 * Author: Svilen Marchev
 * Tags: DP, greedy
 * Analysis: http://codeforces.com/blog/entry/57123
 * <p>
 * Can be solved directly, without need of DP.
 */
public class TaskBDp {
  static final int INF = Integer.MAX_VALUE / 2;

  int N, POS, L, R;
  int[][][] f;

  int solve(int l, int r, int pos) {
    if (l == L && r == R) {
      return 0;
    }
    if (r < R || l > L) {
      return INF;
    }
    if (f[l][r][pos] != -1) {
      return f[l][r][pos];
    }
    int val = INF;

    for (int i = l; i <= r; ++i) {
      int cost = Math.abs(pos - i);
      if (i > l) {
        val = Math.min(val, cost + 1 + solve(i, r, i)); // close left
      }
      if (i < r) {
        val = Math.min(val, cost + 1 + solve(l, i, i)); // close right
      }
    }

    f[l][r][pos] = val;
    return val;
  }

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    SvScanner sc = new SvScanner(in);
    N = sc.nextInt();
    POS = sc.nextInt() - 1;
    L = sc.nextInt() - 1;
    R = sc.nextInt() - 1;

    f = new int[N][N][N];
    ArrayUtils.fill(f, -1);

    out.println(solve(0, N - 1, POS));
  }
}
