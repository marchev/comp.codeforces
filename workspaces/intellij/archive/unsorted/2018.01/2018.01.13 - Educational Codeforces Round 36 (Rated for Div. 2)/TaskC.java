package bg.svilen.codeforces;

import bg.svilen.comp.lib.io.SvScanner;
import net.egork.misc.ArrayUtils;

import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;

/**
 * CodeForces
 * Educational Codeforces Round 36
 * http://codeforces.com/contest/915/problems
 * C. Permute Digits
 * <p>
 * Status: Accepted
 * Submission: http://codeforces.com/contest/915/submission/35182200
 * Niceness: 6/10
 * Difficulty: 4/10
 * Date: 2018-02-12
 * Author: Svilen Marchev
 * Tags: greedy, construction
 * Analysis: http://codeforces.com/blog/entry/57123
 * <p>
 * Builds the answer digit by digit, from left to right. For each position it selects the largest
 * from the remaining digits, such that (!) there is always a solution after it is chosen. Such a check
 * uses a greedy strategy: build the smallest possible number from the remaining digits and verify that
 * that number is <= b; if that holds, then we can select the digit; if not - we check on the next
 * smaller digit.
 * <p>
 * Time: O(N^2 A), where A=10 is the alphabet size.
 */
public class TaskC {

  char[] b;

  boolean hasSomeSolution(int i, int[] freq) {
    for (int d = 0; d <= 9; ++d) {
      for (int times = 0; times < freq[d]; ++times) {
        if (d < b[i] - '0') return true;
        if (d > b[i] - '0') return false;
        i++;
      }
    }
    return true;
  }

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    SvScanner sc = new SvScanner(in);
    char[] a = sc.next().toCharArray();
    b = sc.next().toCharArray();

    if (b.length > a.length) {
      Arrays.sort(a);
      ArrayUtils.reverse(a);
      out.println(String.valueOf(a));
      return;
    }

    int[] freq = new int[10];
    for (int i = 0; i < a.length; ++i) {
      freq[a[i] - '0']++;
    }

    char[] ans = new char[a.length];
    boolean areEqualSoFar = true;
    nextPosition:
    for (int i = 0; i < b.length; ++i) {
      for (int d = areEqualSoFar ? b[i] - '0' : 9; d >= 0; --d) {
        if (d < (b[i] - '0')) {
          areEqualSoFar = false;
        }
        if (freq[d] > 0) {
          freq[d]--;
          if (!areEqualSoFar || hasSomeSolution(i + 1, freq)) {
            ans[i] = (char) (d + '0');
            continue nextPosition;
          }
          freq[d]++;
        }
      }
    }

    out.println(String.valueOf(ans));
  }
}
