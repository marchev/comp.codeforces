package bg.svilen.codeforces;

import bg.svilen.comp.lib.io.SvScanner;
import net.egork.misc.ArrayUtils;

import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;

/**
 * CodeForces
 * Educational Codeforces Round 36
 * http://codeforces.com/contest/915/problems
 * C. Permute Digits
 * <p>
 * Status: Accepted
 * Note: Solved during contest
 * Submission: http://codeforces.com/contest/915/submission/34147494
 * Niceness: 6/10
 * Date: 2018-01-13
 * Author: Svilen Marchev
 * Tags: greedy, construction
 * Analysis: http://codeforces.com/blog/entry/57123
 * <p>
 * Can be solved only greedily, no need of the recursion. That was my initial approach,
 * but it didn't get accepted during the contest.
 */
public class TaskCRecursive {

  char[] ANS;
  String b;

  int getMaxDigLessOrEqTo(int[] dig, int upper) {
    for (int d = upper; d >= 0; --d) {
      if (dig[d] > 0) {
        return d;
      }
    }
    return -1;
  }

  void f(boolean allEqualSoFar, int i, int[] dig, char[] ans) {
    if (i == ans.length) {
      if (ANS == null) ANS = Arrays.copyOf(ans, ans.length);
      return;
    }
    if (allEqualSoFar) {
      int bDig = b.charAt(i) - '0';
      int maxD = getMaxDigLessOrEqTo(dig, bDig);
      if (maxD == -1) {
        return;
      }
      for (int d = maxD; d >= 0 && ANS == null; --d) {
        if (dig[d] > 0) {
          dig[d]--;
          ans[i] = (char) (d + '0');
          f(d == bDig, i + 1, dig, ans);
          dig[d]++;
        }
      }
    } else {
      int d = getMaxDigLessOrEqTo(dig, 9);
      dig[d]--;
      ans[i] = (char) (d + '0');
      f(false, i + 1, dig, ans);
      dig[d]++;
    }
  }

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    SvScanner sc = new SvScanner(in);
    String a = sc.next();
    b = sc.next();

    if (b.length() > a.length()) {
      char[] chars = a.toCharArray();
      Arrays.sort(chars);
      ArrayUtils.reverse(chars);
      out.println(String.valueOf(chars));
      return;
    }

    int[] dig = new int[10];
    for (int i = 0; i < a.length(); ++i) {
      dig[a.charAt(i) - '0']++;
    }

    char[] ans = new char[a.length()];
    f(true, 0, dig, ans);

    out.println(String.valueOf(ANS));
  }
}
