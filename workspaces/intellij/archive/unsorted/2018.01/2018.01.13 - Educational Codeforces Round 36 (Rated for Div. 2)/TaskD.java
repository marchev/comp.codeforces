package bg.svilen.codeforces;

import bg.svilen.comp.lib.io.SvScanner;
import net.egork.graph.Graph;

import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * CodeForces
 * Educational Codeforces Round 36
 * http://codeforces.com/contest/915/problems
 * D - Almost Acyclic Graph
 * <p>
 * Status: Accepted
 * Submission: http://codeforces.com/contest/915/submission/34198891
 * Niceness: 8/10
 * Date: 2018-01-15
 * Author: Svilen Marchev
 * Tags: graphs, cycles, cycles in directed graph
 * Analysis: http://codeforces.com/blog/entry/57123
 * <p>
 * Find a simple cycle. Since it's simple, it consists of at most N vertices.
 * It is clear that in order to make the graph acyclic, we'dneed to break that cycle.
 * So what we do is: we remove each edge of the graph and check if the new graph has cycles.
 * If not, we have proved that we can make the graph acyclic by removing one edge.
 * <p>
 * Complexity is O(N*(N+M)).
 */
public class TaskD {
  Graph g;
  int n, m;

  boolean[] inTree;
  int[] path;
  boolean[] used;

  List<Integer> extractCycle(int lastEdge, int lastV, int pathLen) {
    List<Integer> cycleEdges = new ArrayList<>();
    cycleEdges.add(lastEdge);
    for (int cycleEdgeIx = pathLen - 1; cycleEdgeIx >= 0 && g.destination(path[cycleEdgeIx]) != lastV; --cycleEdgeIx) {
      cycleEdges.add(path[cycleEdgeIx]);
    }
    return cycleEdges;
  }

  List<Integer> findCycleDfs(int v, int pathLen) {
    used[v] = true;
    inTree[v] = true;
    for (int e = g.firstOutbound(v); e != -1; e = g.nextOutbound(e)) {
      int kid = g.destination(e);
      if (inTree[kid]) {
        return extractCycle(e, kid, pathLen);
      } else if (!used[kid]) {
        path[pathLen] = e;
        List<Integer> cycleEdges = findCycleDfs(kid, pathLen + 1);
        if (cycleEdges != null) {
          return cycleEdges;
        }
      }
    }
    inTree[v] = false;
    return null;
  }

  List<Integer> getRandomSimpleCycle() {
    Arrays.fill(inTree, false);
    Arrays.fill(used, false);
    List<Integer> cycleEdges = null;
    for (int i = 0; i < n && cycleEdges == null; ++i) {
      if (!used[i]) {
        cycleEdges = findCycleDfs(i, 0);
      }
    }
    return cycleEdges;
  }

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    SvScanner sc = new SvScanner(in);
    n = sc.nextInt();
    m = sc.nextInt();
    g = new Graph(n, m);

    for (int i = 0; i < m; ++i) {
      int u = sc.nextInt() - 1;
      int v = sc.nextInt() - 1;
      g.addSimpleEdge(u, v);
    }

    inTree = new boolean[n];
    used = new boolean[n];
    path = new int[n];

    List<Integer> cycleEdges = getRandomSimpleCycle();
    if (cycleEdges == null) {
      out.println("YES");
    } else {
      for (int edgeToRemove : cycleEdges) {
        g.removeEdge(edgeToRemove);
        if (getRandomSimpleCycle() == null) {
          out.println("YES");
          return;
        }
        g.restoreEdge(edgeToRemove);
      }
      out.println("NO");
    }
  }
}
