package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * CodeForces
 * Educational Codeforces Round 36
 * http://codeforces.com/contest/915/problems
 * E - Physical Education Lessons
 * <p>
 * Status: Accepted
 * Submission: http://codeforces.com/contest/915/submission/34210034
 * Niceness: 9/10
 * Date: 2018-01-15
 * Author: Svilen Marchev
 * Tags: intervals, intervals in a map, interval tree
 * Analysis: http://codeforces.com/blog/entry/57123
 * <p>
 * Keep intervals only of the vacation days and makes sure they are not overlapping.
 * Keep them in a map, sorted by end coord. For each query [L,R] (no matter the type):
 * Search for the first ival in the map, whose end coord is >= L. Starting from it,
 * traverse all overlapping with [L,R] ivals in the map, and remove/modify some of them,
 * so that the whole [L,R] would be become uncovered.
 * If the query is of type 1, add an ival [L,R] to the map.
 * <p>
 * Since the ivals in the map are non overlapping, complexity is O(Q log Q).
 * <p>
 * Can be solved with an "overwriting" interval tree and compressing the coordinates.
 * E.g. see uwi's solution: http://codeforces.com/contest/915/submission/34141965
 */
public class TaskE {
  static class VacationIval {
    int l, r;

    public VacationIval(int l, int r) {
      this.l = l;
      this.r = r;
    }
  }

  // Sorted map of "end of the interval" => "the interval itself".
  // Note that the intervals do not overlap and all end coordinates are unique.
  TreeMap<Integer, VacationIval> ivals;

  // Returns by how many the balance of vacation days changes after this operation.
  // I.e. it will be either 0 or a negative value.
  long removeIntervalsIn(int l, int r) {
    List<VacationIval> toRemove = new ArrayList<>();
    List<VacationIval> toAdd = new ArrayList<>();
    for (Map.Entry<Integer, VacationIval> entry : ivals.tailMap(l).entrySet()) {
      VacationIval ival = entry.getValue();
      if (ival.l > r) {
        break;
      } else if (ival.l >= l) {
        toRemove.add(ival);
        if (r < ival.r) {
          toAdd.add(new VacationIval(r + 1, ival.r));
        }
      } else {
        toRemove.add(ival);
        toAdd.add(new VacationIval(ival.l, l - 1));
        if (r < ival.r) {
          toAdd.add(new VacationIval(r + 1, ival.r));
        }
      }
    }

    long balance = 0;
    for (VacationIval ival : toRemove) {
      balance -= ival.r - ival.l + 1;
      ivals.remove(ival.r);
    }
    for (VacationIval ival : toAdd) {
      balance += ival.r - ival.l + 1;
      ivals.put(ival.r, ival);
    }
    return balance;
  }

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    int n = in.readInt();
    int q = in.readInt();

    long numVacationDays = 0;
    ivals = new TreeMap<>();
    for (int i = 0; i < q; ++i) {
      int l = in.readInt();
      int r = in.readInt();
      int type = in.readInt();
      numVacationDays += removeIntervalsIn(l, r);
      if (type == 1) {
        ivals.put(r, new VacationIval(l, r));
        numVacationDays += r - l + 1;
      }
      out.println(n - numVacationDays);
    }
  }
}
