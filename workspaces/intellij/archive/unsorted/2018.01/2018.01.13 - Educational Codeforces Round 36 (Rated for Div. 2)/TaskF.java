package bg.svilen.codeforces;

import bg.svilen.comp.lib.ds.SvDisjointSetUnion;
import bg.svilen.comp.lib.io.SvScanner;
import net.egork.graph.Graph;

import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Comparator;

/**
 * CodeForces
 * Educational Codeforces Round 36
 * http://codeforces.com/contest/915/problems
 * F - Imbalance Value of a Tree
 * <p>
 * Status: Accepted
 * Submission: http://codeforces.com/contest/915/submission/34427742
 * Niceness: 9/10
 * Date: 2018-01-15
 * Author: Svilen Marchev
 * Tags: graph, trees, disjoint set union, counting paths in trees, prefix sums, cutting the inner loop
 * Analysis: http://codeforces.com/blog/entry/57123
 * <p>
 * Very nice one. Modification of the classic problem: Given an oriented tree with weights on the nodes, find
 * sum{MAX(x,y) | x,y - vertices in the tree} and MAX(x,y) gives the max weight of vertex across the simple
 * path between x and y.
 * <p>
 * The solution adds the vertices one by one in an increasing order of their weight. At every addition, we can
 * count all routes that pass through the vertex being added in which this vertex sets the maximum.
 */
public class TaskF {

  static class Vertex {
    int id;
    int value;
  }

  int n;
  Vertex[] vertices;
  Graph graph;

  long sumMinOrMaxOverAllPaths() {
    SvDisjointSetUnion dsu = new SvDisjointSetUnion(n);
    boolean[] isAdded = new boolean[n];
    int[] addedKids = new int[n];
    int[] addedKidsSubtreeSizes = new int[n];
    long[] addedKidsSubtreeSizesAccum = new long[n + 1]; // prefix sums, 1-based
    long sum = 0;

    // Add the vertices in the provided order:
    // - If it is increasing, we will be summing up max-s over all paths.
    // - If it is decreasing, we will be summing up min-s over all paths.
    for (Vertex v : vertices) {
      int numAddedKids = 0;
      for (int e = graph.firstOutbound(v.id); e != -1; e = graph.nextOutbound(e)) {
        int kid = graph.destination(e);
        if (isAdded[kid]) {
          addedKids[numAddedKids] = kid;
          addedKidsSubtreeSizes[numAddedKids] = dsu.getSetSize(kid);
          addedKidsSubtreeSizesAccum[numAddedKids + 1]
              = addedKidsSubtreeSizesAccum[numAddedKids] + addedKidsSubtreeSizes[numAddedKids];
          numAddedKids++;
        }
      }

      long numRoutes = 1;
      for (int i = 0; i < numAddedKids; ++i) {
        numRoutes += addedKidsSubtreeSizes[i];
      }
      for (int i = 1; i < numAddedKids; ++i) {
        numRoutes += addedKidsSubtreeSizesAccum[i] * addedKidsSubtreeSizes[i];
      }
      // For numRoutes routes in the graph that the max (or min) element would be determined by v.id.
      sum += v.value * numRoutes;

      isAdded[v.id] = true;
      for (int i = 0; i < numAddedKids; ++i) {
        dsu.join(v.id, addedKids[i]);
      }
    }

    return sum;
  }

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    SvScanner sc = new SvScanner(in);
    n = sc.nextInt();
    vertices = new Vertex[n];
    for (int i = 0; i < n; ++i) {
      vertices[i] = new Vertex();
      vertices[i].id = i;
      vertices[i].value = sc.nextInt();
    }
    graph = new Graph(n, 2 * (n - 1));
    for (int i = 1; i < n; ++i) {
      int x = sc.nextInt() - 1;
      int y = sc.nextInt() - 1;
      graph.addSimpleEdge(x, y);
      graph.addSimpleEdge(y, x);
    }

    Arrays.sort(vertices, Comparator.comparingInt(v -> v.value));
    long sumOfMax = sumMinOrMaxOverAllPaths();
    Arrays.sort(vertices, Comparator.comparingInt(v -> -v.value));
    long sumOfMin = sumMinOrMaxOverAllPaths();
    out.println(sumOfMax - sumOfMin);
  }
}
