package bg.svilen.codeforces;

import bg.svilen.comp.lib.io.SvScanner;
import bg.svilen.comp.lib.math.SvCommonMathUtils;
import bg.svilen.comp.lib.math.SvNumberTheoryUtils;

import java.io.InputStreamReader;
import java.io.PrintWriter;

/**
 * CodeForces
 * Educational Codeforces Round 36
 * http://codeforces.com/contest/915/problems
 * G - Coprime Arrays
 * <p>
 * Status: Accepted
 * Submission: http://codeforces.com/contest/915/submission/34949167
 * Niceness: 7/10
 * Difficulty: 8/10
 * Date: 2018-02-05
 * Author: Svilen Marchev
 * Tags: number theory, moebius function, math, inclusion-exclusion principle, factorization, DP, divisibility
 * Analysis: http://codeforces.com/blog/entry/57123
 * <p>
 * For a fixed upper bound i, this is a well-known problem that can be solved using inclusion-exclusion:
 * Let's denote by f(g) the number of arrays with elements in range [1,i] such that gcd(a1,..,an) is divisible by g.
 * f(g) = floor(i/g)^n. With the help of inclusion-exclusion formula we can prove that the number of arrays with gcd=1
 * is the sum of the following values over all possible sets S: ((-1) ^ |S|) f(product(S)), where S is some
 * set of prime numbers (possibly an empty set), and product(S) is the product of all elements in the set.
 * f(product(S)) in this formula denotes the number of arrays such that their gcd is divisible by every number from set S.
 * <p>
 * We can rewrite the sum over every set S in such a way: sum{moebius(g) f(g) | g=1..i}, where moebius(g) is 0
 * if there is no any set of prime numbers S such that product(S)=g, |moebius(g)|=1 if this set S exists,
 * and the sign is determined by the size of S.
 * <p>
 * Okay, so we found a solution for one upper bound i. How can we calculate it for every i from 1 to k?
 * <p>
 * Suppose we have calculated all values of f(g) for some i-1 and we want to recalculate them for i.
 * The important fact is that these values change (and thus need recalculation) only for the numbers j such that j|i.
 * So if we recalculate only these values f(g) (and each recalculation can be done in O(1)),
 * then we will have to do only O(k ln k) recalculations overall.
 */
public class TaskG {

  final int MOD = 1_000_000_007;

  int n, k;

  /** Moebius function for 1..k. */
  byte[] moebius;
  /** i^n for i=1..k. */
  int[] powN;
  /** val[i] stores the number of coprime arrays with n elements, whole values are in [1, i]. */
  long[] val;

  public void solve(int testNumber, InputStreamReader in, PrintWriter out) {
    SvScanner sc = new SvScanner(in);
    n = sc.nextInt();
    k = sc.nextInt();

    moebius = SvNumberTheoryUtils.genMoebiusFuncForIntsUpTo(k);

    powN = new int[k + 1];
    for (int i = 1; i <= k; ++i) {
      powN[i] = SvCommonMathUtils.pow(i, n, MOD);
    }

    val = new long[k + 1];
    for (int g = 1; g <= k; ++g) {
      for (int i = g; i <= k; i += g) {
        val[i] += moebius[g] * powN[i / g];
        val[i] -= moebius[g] * powN[(i - 1) / g];
      }
    }
    for (int i = 2; i <= k; ++i) {
      val[i] = (val[i] + val[i - 1]) % MOD;
    }

    long ans = 0;
    for (int i = 1; i <= k; ++i) {
      long v = (val[i] % MOD + MOD) % MOD;
      ans = (ans + (v ^ i)) % MOD;
    }
    out.println(ans);
  }
}
