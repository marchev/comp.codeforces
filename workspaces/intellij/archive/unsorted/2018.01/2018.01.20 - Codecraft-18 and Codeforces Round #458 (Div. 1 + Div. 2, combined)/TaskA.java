package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Set;

/**
 * CodeForces
 * Codecraft-18 and Codeforces Round #458 (Div. 1 + Div. 2, combined)
 * http://codeforces.com/contest/914
 * A - Perfect Squares
 * <p>
 * Status: Accepted
 * Submission: http://codeforces.com/contest/914/submission/34362525
 * Note: Solved during the contest
 * Niceness: 5/10
 * Date: 2018-01-20
 * Author: Svilen Marchev
 * Tags: square roots, implementation
 * Analysis: http://codeforces.com/blog/entry/57250
 * <p>
 * First prepopulate perfect squares, then go through the sequence and look them up.
 * <p>
 * Complexity is O(N).
 */
public class TaskA {

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    Set<Integer> sqs = new HashSet<>(1001);
    for (int i = 0; i <= 1000; ++i) {
      sqs.add(i * i);
    }

    int n = in.readInt();
    int max = Integer.MIN_VALUE;
    for (int i = 0; i < n; ++i) {
      int ai = in.readInt();
      if (!sqs.contains(ai)) {
        max = Math.max(max, ai);
      }
    }
    out.println(max);
  }
}
