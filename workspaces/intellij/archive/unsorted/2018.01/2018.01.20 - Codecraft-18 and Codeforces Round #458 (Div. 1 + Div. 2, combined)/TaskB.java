package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * CodeForces
 * Codecraft-18 and Codeforces Round #458 (Div. 1 + Div. 2, combined)
 * http://codeforces.com/contest/914
 * B - Conan and Agasa play a Card Game
 * <p>
 * Status: Accepted
 * Submission: http://codeforces.com/contest/914/submission/34395335
 * Note: Almost solved during the contest (the type boxing screwed me)
 * Niceness: 8/10
 * Date: 2018-01-20
 * Author: Svilen Marchev
 * Tags: game theory, standard game
 * Analysis: http://codeforces.com/blog/entry/57250
 * <p>
 * A state is winning iff there exists a group of
 * equal elements with an odd number. The optimal move for the player is to pick the largest element, which
 * occurs an odd number of times. That way, the player would leave their opponent only with groups of even sizes,
 * which is ultimately a losing position.
 * <p>
 * Time complexity is O(N).
 */
public class TaskB {

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    int n = in.readInt();
    List<Integer> a = new ArrayList<>(n);
    for (int i = 0; i < n; ++i) {
      a.add(in.readInt());
    }
    Collections.sort(a);

    for (int j, i = 0; i < n; i = j) {
      for (j = i + 1; j < n && a.get(i).equals(a.get(j)); ++j) ;
      if ((j - i) % 2 == 1) {
        out.println("Conan");
        return;
      }
    }
    out.println("Agasa");
  }
}
