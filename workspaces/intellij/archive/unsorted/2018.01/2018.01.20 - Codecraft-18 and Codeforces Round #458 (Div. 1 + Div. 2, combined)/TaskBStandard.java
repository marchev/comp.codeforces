package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * CodeForces
 * Codecraft-18 and Codeforces Round #458 (Div. 1 + Div. 2, combined)
 * http://codeforces.com/contest/914
 * B - Conan and Agasa play a Card Game
 * <p>
 * Status: Accepted
 * Submission: http://codeforces.com/contest/914/submission/34394576
 * Note: Almost solved during the contest (the type boxing screwed me)
 * Niceness: 8/10
 * Date: 2018-01-20
 * Author: Svilen Marchev
 * Tags: game theory, standard game, inner loop optimization
 * Analysis: http://codeforces.com/blog/entry/57250
 * <p>
 * My implementation uses the generic approach for games. To achieve O(N) complexity, it cuts down the inner loop.
 * <p>
 * Alternatively, can be solved much simpler by observing if a state is winning iff there exists a group of
 * equal elements with an odd number. The optimal move for the player is to pick the largest element, which
 * occurs an odd number of times. That way, the player would leave their opponent only with groups of even sizes,
 * which is ultimately a losing position. See the other file for that implementation.
 * <p>
 * Time complexity is O(N).
 */
public class TaskBStandard {

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    int n = in.readInt();
    List<Integer> a = new ArrayList<>(n);
    for (int i = 0; i < n; ++i) {
      a.add(in.readInt());
    }
    Collections.sort(a);

    boolean[] startsNewGroup = new boolean[n + 1];
    int[] nextGroup = new int[n + 1];
    for (int j, i = 0; i < n; i = j) {
      startsNewGroup[i] = true;
      for (j = i + 1; j < n && a.get(i).equals(a.get(j)); ++j) ;
      for (int k = i; k < j; ++k) nextGroup[k] = j;
    }

    boolean[] isWin = new boolean[n + 1];
    boolean[] existsLoseAfter = new boolean[n + 1];
    isWin[n] = false;
    existsLoseAfter[n] = false;
    for (int i = n - 1; i >= 0; --i) {
      isWin[i] = !isWin[i + 1] || existsLoseAfter[nextGroup[i]];
      if (startsNewGroup[i]) {
        existsLoseAfter[i] = existsLoseAfter[nextGroup[i]] || !isWin[i + 1];
      }
    }
    out.println(isWin[0] ? "Conan" : "Agasa");
  }
}
