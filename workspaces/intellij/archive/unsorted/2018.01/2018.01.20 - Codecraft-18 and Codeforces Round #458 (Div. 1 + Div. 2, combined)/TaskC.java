package bg.svilen.codeforces;

import net.egork.misc.ArrayUtils;
import net.egork.utils.io.InputReader;

import java.io.PrintWriter;

/**
 * CodeForces
 * Codecraft-18 and Codeforces Round #458 (Div. 1 + Div. 2, combined)
 * http://codeforces.com/contest/914
 * C - Travelling Salesman and Special Numbers
 * <p>
 * Status: Accepted
 * Submission: http://codeforces.com/contest/914/submission/34394037
 * Niceness: 7/10
 * Date: 2018-01-20
 * Author: Svilen Marchev
 * Tags: dp, meet-in-the-middle, binary representation
 * Analysis: http://codeforces.com/blog/entry/57250
 * <p>
 * Complexity is O(N^2).
 */
public class TaskC {

  final int MOD = 1_000_000_007;
  final int MAXL = 1024;

  String s;
  int K;

  int[][][] f;

  // Counts the number of integers <=N that have exactly `onesTotal` 1-s in their binary representation
  // AND are different than the number of 1-s in their binary representation. The latter guarantees that
  // their is _exactly one_ "operation" that squeezes the number to its number of 1-s in its binary representation.
  // For example: the number of 1-s in the number 1 is 1, i.e. it's the same as the number itself, so we don't want to count it.
  // The only two such cases are 0 and 1.
  int calc(int i, int eq, int onesLeft, int onesTotal, int cappedNum) {
    if (i == s.length()) {
      // Count the current number only if it has the required number of 1-s in its binary representation AND
      // the number itself is not equal to the number of 1s in its binary representation.
      return onesLeft == 0 && onesTotal != cappedNum ? 1 : 0;
    }
    if (f[i][eq][onesLeft] != -1) {
      return f[i][eq][onesLeft];
    }

    int val = 0;
    // put '1'
    if (onesLeft > 0 && (eq == 0 || s.charAt(i) == '1')) {
      val = (val + calc(i + 1, eq, onesLeft - 1, onesTotal, Math.min(2 * MAXL, 2 * cappedNum + 1))) % MOD;
    }
    // put '0'
    val = (val + calc(i + 1, (eq == 1 && s.charAt(i) == '0') ? 1 : 0, onesLeft, onesTotal, Math.min(2 * MAXL, 2 * cappedNum + 0))) % MOD;

    f[i][eq][onesLeft] = val;
    return val;
  }

  int countOperationsToReduce(int k) {
    int num = 0;
    while (k > 1) {
      k = Integer.bitCount(k);
      num++;
    }
    return num;
  }

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    s = in.readString();
    K = in.readInt();

    if (K == 0) {
      out.println("1");
      return;
    }

    f = new int[MAXL][2][MAXL + 1];
    ArrayUtils.fill(f, -1);

    int ans = 0;
    for (int k = 1; k <= s.length(); ++k) {
      if (countOperationsToReduce(k) == K - 1) {
        int fval = calc(0, 1, k, k, 0);
        ans = (ans + fval) % MOD;
      }
    }
    out.println(ans);
  }
}
