package bg.svilen.codeforces;

import bg.svilen.comp.lib.math.SvCommonMathUtils;
import net.egork.utils.io.InputReader;

import java.io.PrintWriter;

/**
 * CodeForces
 * Codecraft-18 and Codeforces Round #458 (Div. 1 + Div. 2, combined)
 * http://codeforces.com/contest/914
 * D - Bash and a Tough Math Puzzle
 * <p>
 * Status: Accepted
 * Submission: http://codeforces.com/contest/914/submission/34416713
 * Niceness: 9/10
 * Date: 2018-01-21
 * Author: Svilen Marchev
 * Tags: interval trees, gcd, divisibility, simple math
 * Analysis: http://codeforces.com/blog/entry/57250
 * <p>
 * Complexity is O(Q log N).
 */
public class TaskD {

  /** Leaf nodes keep the actual numbers. Non-leaf nodes keep GCD of the subtrees. */
  int[] tree;
  int treeSz;

  void initTree(int i, int treeL, int treeR) {
    if (treeL == treeR) return;
    int mid = (treeL + treeR) / 2;
    initTree(2 * i, treeL, mid);
    initTree(2 * i + 1, mid + 1, treeR);
    tree[i] = SvCommonMathUtils.gcd(tree[2 * i], tree[2 * i + 1]);
  }

  void update(int numIx, int newVal) {
    int i = treeSz / 2 + numIx;
    tree[i] = newVal;
    for (i >>= 1; i > 0; i >>= 1) {
      tree[i] = SvCommonMathUtils.gcd(tree[2 * i], tree[2 * i + 1]);
    }
  }

  /**
   * Returns how many numbers would need to be changed in order to make the gcd of the interval [ql, qr]
   * of numbers divisible by x. We are interested only whether the result is <=1 or it is more, so we cap it.
   */
  int query(int i, int treeL, int treeR, int ql, int qr, int x) {
    if (treeL == treeR) {
      return tree[i] % x == 0 ? 0 : 1;
    }
    if (tree[i] % x == 0) return 0;
    int mid = (treeL + treeR) / 2;
    int val = 0;
    if (ql <= mid) {
      val += query(2 * i, treeL, mid, ql, qr, x);
      if (val > 1) return val;
    }
    if (mid + 1 <= qr) {
      val += query(2 * i + 1, mid + 1, treeR, ql, qr, x);
    }
    return val;
  }

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    int n = in.readInt();
    treeSz = Math.max(1, Integer.highestOneBit(n - 1)) << 2;
    tree = new int[treeSz];
    for (int i = 0; i < n; ++i) {
      tree[treeSz / 2 + i] = in.readInt();
    }
    for (int i = treeSz / 2 + n; i < treeSz; ++i) {
      tree[i] = 1;
    }
    initTree(1, 0, treeSz / 2 - 1);

    int q = in.readInt();
    for (int i = 0; i < q; ++i) {
      int type = in.readInt();
      if (type == 1) {
        int ql = in.readInt() - 1;
        int qr = in.readInt() - 1;
        int x = in.readInt();

        int numChangesNeeded = query(1, 0, treeSz / 2 - 1, ql, qr, x);
        // numChangesNeeded are needed to make the GCD of the numbers in the interval [ql,qr] divisible by x.
        // It can be that the GCD is > x, though, whereas we want it equal to x. But that is not a problem:
        //  - if numOpersNeeded=1, then we could set that single changed number to x, and the whole GCD would be capped by x.
        //  - if numOpersNeeded=0, then we could pick any number from the interval, set it to x, and the whole GCD would be capped by x.
        out.println(numChangesNeeded <= 1 ? "YES" : "NO");
      } else {
        int numIx = in.readInt() - 1;
        int newVal = in.readInt();
        update(numIx, newVal);
      }
    }
  }
}
