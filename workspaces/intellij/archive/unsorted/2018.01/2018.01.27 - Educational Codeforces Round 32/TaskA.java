package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;

/**
 * CodeForces
 * Educational Codeforces Round 32
 * http://codeforces.com/contest/888/problems
 * A. Local Extrema
 * <p>
 * Status: Accepted
 * Note: Solved during contest
 * Submission:
 * Niceness: 5/10
 * Difficulty: 1/10
 * Date: 2018-01-29
 * Author: Svilen Marchev
 * Tags: implementation
 * Analysis: http://codeforces.com/blog/entry/55701
 */
public class TaskA {
  public void solve(int testNumber, InputReader in, PrintWriter out) {
    int n = in.readInt();
    int[] a = new int[n];
    for (int i = 0; i < n; ++i) {
      a[i] = in.readInt();
    }
    int ans = 0;
    for (int i = 1; i < n - 1; ++i) {
      if ((a[i] < a[i - 1] && a[i] < a[i + 1]) || (a[i] > a[i - 1] && a[i] > a[i + 1])) {
        ans++;
      }
    }
    out.println(ans);
  }
}
