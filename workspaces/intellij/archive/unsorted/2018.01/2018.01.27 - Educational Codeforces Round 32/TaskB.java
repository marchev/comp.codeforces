package bg.svilen.codeforces;

import net.egork.misc.ArrayUtils;
import net.egork.utils.io.InputReader;

import java.io.PrintWriter;

/**
 * CodeForces
 * Educational Codeforces Round 32
 * http://codeforces.com/contest/888/problems
 * B - Buggy Robot
 * <p>
 * Status: Accepted
 * Note: Solved during contest
 * Submission:
 * Niceness: 7/10
 * Difficulty: 3/10
 * Date: 2018-01-29
 * Author: Svilen Marchev
 * Tags: implementation
 * Analysis: http://codeforces.com/blog/entry/55701
 * <p>
 * I solved it overly complicated - using DP, whereas there is a much simpler solution
 * that simply finds at which point (dx,dy) the robot ends up and then n-dx-dy would be the answer.
 * <p>
 * Consider the final cell after original path. It has some distance dx to x=0 and dy to y=0.
 * That means the path included at least dx and dy in corresponding directions. Let's remove just these minimal numbers of moves.
 * Finally, the answer will be n-dx-dy where (dx,dy) are distances from the final cell of the original path to (0,0).
 * Overall complexity: O(n).
 */
public class TaskB {

  static final int MN = 100;
  static final int MC = 100;

  static final int[] dx = {0, 1, 0, -1};
  static final int[] dy = {1, 0, -1, 0};

  int getDir(char c) {
    int dir = 0;
    switch (c) {
      case 'U':
        dir = 0;
        break;
      case 'R':
        dir = 1;
        break;
      case 'D':
        dir = 2;
        break;
      case 'L':
        dir = 3;
        break;
    }
    return dir;
  }

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    int n = in.readInt();
    String command = in.readString();

    int[][][] f = new int[MN + 1][2 * MC + 1][2 * MC + 1];
    ArrayUtils.fill(f, Integer.MIN_VALUE);
    f[0][MC][MC] = 0;

    for (int i = 0; i < command.length(); ++i) {
      int dir = getDir(command.charAt(i));
      for (int x = 0; x <= 2 * MC; ++x) {
        for (int y = 0; y <= 2 * MC; ++y) {
          if (f[i][x][y] != Integer.MIN_VALUE) {
            int nx = x + dx[dir];
            int ny = y + dy[dir];
            f[i + 1][nx][ny] = Math.max(f[i + 1][nx][ny], 1 + f[i][x][y]);

            f[i + 1][x][y] = Math.max(f[i + 1][x][y], f[i][x][y]);
          }
        }
      }
    }
    out.println(f[n][MC][MC] != Integer.MIN_VALUE ? f[n][MC][MC] : 0);
  }
}
