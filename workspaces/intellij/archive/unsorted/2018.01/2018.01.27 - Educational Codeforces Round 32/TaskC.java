package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;

/**
 * CodeForces
 * Educational Codeforces Round 32
 * http://codeforces.com/contest/888/problems
 * C - K-Dominant Character
 * <p>
 * Status: Accepted
 * Note: Solved during contest
 * Submission:
 * Niceness: 8/10
 * Difficulty: 4/10
 * Date: 2018-01-29
 * Author: Svilen Marchev
 * Tags: strings, sweeping, sequences, finding distances between equal elements in a sequence
 * Analysis: http://codeforces.com/blog/entry/55701
 * <p>
 * At first, notice that the final answer is minimum over answers for each character.
 * The answer for one character can be obtained like this. Write down lengths of segments between two consecutive
 * occurrences of this character, from the first occurrence to the start of the string and from the last to
 * the end of the string. Take maximum of these values. Answer will be this maximum + 1.
 * Overall complexity: O(|Alpha| n).
 */
public class TaskC {

  int findKForChar(char[] a, char c) {
    int ans = 0;
    a[0] = a[a.length - 1] = c;
    for (int j, i = 0; i < a.length; i = j) {
      for (j = i + 1; j < a.length && a[i] != a[j]; ++j) ;
      ans = Math.max(ans, j - i);
    }
    return ans;
  }

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    String s = "A" + in.readString() + "A";
    char[] a = s.toCharArray();

    int ans = a.length;
    for (char c = 'a'; c <= 'z'; ++c) {
      int k = findKForChar(a, c);
      if (k != 0) {
        ans = Math.min(ans, k);
      }
    }
    out.println(ans);
  }
}
