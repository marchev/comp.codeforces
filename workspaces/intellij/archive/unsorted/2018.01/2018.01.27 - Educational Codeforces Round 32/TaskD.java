package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;

/**
 * CodeForces
 * Educational Codeforces Round 32
 * http://codeforces.com/contest/888/problems
 * D - Almost Identity Permutations
 * <p>
 * Status: Accepted
 * Note: Solved during contest
 * Submission:
 * Niceness: 8/10
 * Difficulty: 5/10
 * Date: 2018-01-29
 * Author: Svilen Marchev
 * Tags: math, combinatorics, inclusion-exclusion principle, binomial coefficients, permutations, permutations with fixed elements
 * Analysis: http://codeforces.com/blog/entry/55701
 */
public class TaskD {

  public long[][] binomForSize(int n) {
    long[][] bin = new long[n + 1][n + 1];
    bin[0][0] = 1;
    for (int i = 1; i <= n; ++i) {
      bin[i][0] = bin[i][i] = 1;
      for (int j = 1; j < i; ++j) {
        bin[i][j] = bin[i - 1][j] + bin[i - 1][j - 1];
      }
    }
    return bin;
  }

  long[] factorialForSize(int n) {
    long[] fact = new long[n + 1];
    fact[0] = 1;
    for (int i = 1; i <= n; ++i) {
      fact[i] = i * fact[i - 1];
    }
    return fact;
  }

  long[][] bin;
  long[] fact;

  long calcPermutationsWithNoDigitsInPlace(int k) {
    long ret = 0;
    for (int i = 0; i <= k; ++i) {
      long sign = i % 2 == 0 ? +1 : -1;
      ret += sign * bin[k][i] * fact[k - i];
    }
    return ret;
  }

  long calc(int n, int k) {
    return bin[n][k] * calcPermutationsWithNoDigitsInPlace(k);
  }

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    int N = in.readInt();
    int K = in.readInt();

    bin = binomForSize(1000);
    fact = factorialForSize(10);

    long sum = 0;
    for (int k = 0; k <= K; ++k) {
      sum += calc(N, k);
    }
    out.println(sum);
  }
}
