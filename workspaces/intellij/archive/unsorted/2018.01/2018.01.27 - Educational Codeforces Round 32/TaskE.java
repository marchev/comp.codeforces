package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;
import java.util.NavigableSet;
import java.util.TreeSet;

/**
 * CodeForces
 * Educational Codeforces Round 32
 * http://codeforces.com/contest/888/problems
 * E - Maximum Subsequence
 * <p>
 * Status: Accepted
 * Note: Solved during contest
 * Submission:
 * Niceness: 8/10
 * Difficulty: 6/10
 * Date: 2018-01-29
 * Author: Svilen Marchev
 * Tags: meet in the middle, modulo
 * Analysis: http://codeforces.com/blog/entry/55701
 * <p>
 * Split the set in two halves. Generate all sums under modulo m from the first set and put them in a TreeSet.
 * Generate all sums under modulo m from the second set, and for each sum S from the second set:
 * - find the greatest element T from the TreeSet that is less than n-S. (S+T)%m is a potential candidate
 * for the smallest possible sum.
 * - find the greatest element M from the TreeSet. Another candidate for the smallest sum is (S+M)%m.
 * <p>
 * Complexity: O(2^(n/2) * log(2^(n/2))).
 */
public class TaskE {

  int n, m;
  int[] a;

  long buildSum(int b, int size, int offset) {
    long sum = 0;
    for (int i = 0; i < size; ++i) {
      if (((b >>> i) & 1) == 1) {
        sum = (sum + a[offset + i]) % m;
      }
    }
    return sum;
  }

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    n = in.readInt();
    m = in.readInt();
    a = new int[n];
    for (int i = 0; i < n; ++i) {
      a[i] = in.readInt() % m;
    }

    if (n == 1) {
      out.println(a[0]);
      return;
    }

    int setASz = n / 2;
    long setAMax = 0;
    NavigableSet<Long> setA = new TreeSet<>();
    for (int b = 0; b < (1 << setASz); ++b) {
      long sum = buildSum(b, setASz, 0);
      setA.add(sum);
      setAMax = Math.max(setAMax, sum);
    }

    int setBSz = n - setASz;
    long ans = 0;
    for (int b = 0; b < (1 << setBSz); ++b) {
      long sum = buildSum(b, setBSz, setASz);

      long greatestSmallerNum = setA.lower(m - sum);
      ans = Math.max(ans, (sum + greatestSmallerNum) % m);

      ans = Math.max(ans, (sum + setAMax) % m);
    }
    out.println(ans);
  }
}
