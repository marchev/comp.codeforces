package bg.svilen.codeforces;

import net.egork.misc.ArrayUtils;
import net.egork.utils.io.InputReader;

import java.io.PrintWriter;

/**
 * CodeForces
 * Educational Codeforces Round 32
 * http://codeforces.com/contest/888/problems
 * F - Connecting Vertices
 * <p>
 * Status: Accepted
 * Submission: http://codeforces.com/contest/888/submission/34929674
 * Niceness: 8/10
 * Difficulty: 8/10
 * Date: 2018-02-04
 * Author: Svilen Marchev
 * Tags: DP, optimum triangulation
 * Analysis: http://codeforces.com/blog/entry/55701
 * <p>
 * Uses standard DP, similar to the optimum triangulation DP, but there is a trick to avoid counting
 * some configurations multiple times.
 * <p>
 * Complexity: O(N^3).
 */
public class TaskF {

  final int MOD = 1_000_000_007;

  int n;
  boolean[][] canConnect;

  int[][][] f;

  /**
   * Returns how many ways there are to connect together all points in [i,j], assuming that i and j are already
   * connected somehow, and all the points in [i,j] should be connected only among themselves (i.e. they
   * should not connect to other points outside of this range). {@code canConnectFromI} determines whether it is
   * allowed to connect from point i (this is used to avoid counting multiple times some configurations).
   */
  int calc(int i, int j, int canConnectFromI) {
    if (i == j - 1) {
      return 1;
    }
    if (f[i][j][canConnectFromI] != -1) {
      return f[i][j][canConnectFromI];
    }
    int val = 0;

    for (int k = i + 1; k < j; ++k) {
      if (canConnectFromI == 1 && canConnect[i][k]) {
        val = (int) ((val + (long) calc(i, k, 1) * calc(k, j, 1)) % MOD);
      }
      if (canConnect[j][k]) {
        val = (int) ((val + (long) calc(i, k, 0) * calc(k, j, 1)) % MOD);
      }
    }

    f[i][j][canConnectFromI] = val;
    return val;
  }

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    n = in.readInt();
    canConnect = new boolean[n + 1][n + 1];
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < n; ++j) {
        int v = in.readInt();
        canConnect[i][j] = v == 1;
      }
    }
    // Add a fake copy of point 0 at the end - as point n.
    for (int i = 0; i < n; ++i) {
      canConnect[n][i] = canConnect[0][i];
      canConnect[i][n] = canConnect[i][0];
    }

    f = new int[n + 1][n + 1][2];
    ArrayUtils.fill(f, -1);
    out.println(calc(0, n, 0));
  }
}
