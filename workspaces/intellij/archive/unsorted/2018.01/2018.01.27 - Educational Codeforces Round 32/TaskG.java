package bg.svilen.codeforces;

import bg.svilen.comp.lib.sequences.SvArrays;
import net.egork.utils.io.InputReader;

import java.io.PrintWriter;

/**
 * CodeForces
 * Educational Codeforces Round 32
 * http://codeforces.com/contest/888/problems
 * G - Xor-MST
 * <p>
 * Status: Accepted
 * Note: Nice solution
 * Submission: http://codeforces.com/contest/888/submission/34879345
 * Niceness: 10/10
 * Difficulty: 8/10
 * Date: 2018-02-02
 * Author: Svilen Marchev
 * Tags: divide and conquer, recursion, minimum spanning tree, kruskal, XOR, min XOR of numbers in large sets
 * Analysis: http://codeforces.com/blog/entry/55701
 * <p>
 * Analysis by TimonKnigge (http://codeforces.com/blog/entry/55701?#comment-395075):
 * ------
 * You can solve this by starting with Kruskal's algorithm too. If you only look at the highest bit at b=29,
 * and split your vertex set into two halves V0 and V1 (one with all vertices where bit b is turned on,
 * the other all the vertices where this bit is turned off). It follows that internal edges in either
 * V0 or V1 will have bit b turned off, but edges between the two components will have this bit turned on.
 * Thus, if you were to run Kruskal's algorithm you would first enumerate all internal edges in V0 and V1
 * before considering edges between the two components. It follows that Kruskal's algorithm would first connect
 * all of V0 into a single tree (V1 respectively), and then find a single connecting edge between the two components.
 * <p>
 * This gives you a simple recursive algorithm: Split on the highest order bit b, solve these two halves recursively
 * (only considering bits < b) and then find the weight of the single connecting edge, i.e. given two sequences
 * of values, find the pair of values with minimal exclusive-or. This last problem can be solved using a very similar
 * recursion.
 * ------
 * <p>
 * Complexity: O(N max(logN,maxA) max(logN,maxA)).
 */
public class TaskG {

  int n;
  int[] a;

  /**
   * Finds the min XOR between two elements - one in a[al..ar] and one in a[bl..br].
   * {@code bit} determines which bit to consider next in the grouping.
   */
  int fastXor(int al, int ar, int bl, int br, int bit) {
    // If we have run out of bits, min XOR is just the XOR of any of the number.
    if (bit < 0) {
      return a[al] ^ a[bl];
    }
    // If there is just one element left in any of the groups, just compute the min XOR.
    if (ar == al) {
      int minXor = Integer.MAX_VALUE;
      for (int i = bl; i <= br; ++i) minXor = Math.min(minXor, a[al] ^ a[i]);
      return minXor;
    }
    if (br == bl) {
      return fastXor(bl, br, al, ar, bit);
    }

    // Find the longest blocks of elements inside the two groups, for which the bit-th bit is 0.
    int ai = al, bi = bl;
    for (; ai <= ar && ((a[ai] >>> bit) & 1) == 0; ++ai) ;
    for (; bi <= br && ((a[bi] >>> bit) & 1) == 0; ++bi) ;

    int minXor = Integer.MAX_VALUE;
    boolean hasHomogeneousGroups = false;

    // If both groups have elements for which the bit-th bit is 0, then the min XOR might be between them.
    // (When XOR-ing those numbers, the bit-th bit will become 0.)
    if (ai > al && bi > bl) {
      minXor = Math.min(minXor, fastXor(al, ai - 1, bl, bi - 1, bit - 1));
      hasHomogeneousGroups = true;
    }
    // If both groups have elements for which the bit-th bit is 1, then the min XOR might be between them.
    // (When XOR-ing those numbers, the bit-th bit will become 0.)
    if (ai <= ar && bi <= br) {
      minXor = Math.min(minXor, fastXor(ai, ar, bi, br, bit - 1));
      hasHomogeneousGroups = true;
    }
    // If the first set has only 0s for the bit-th bit and the second set has only 1s for the bit-th bit
    // (or vice versa), then we would have to search for min XOR in those groups.
    // (When XOR-ing those numbers, the bit-th bit will become 1.)
    if (!hasHomogeneousGroups) {
      if (ai > al && bi <= br) {
        minXor = Math.min(minXor, fastXor(al, ai - 1, bi, br, bit - 1));
      }
      if (ai <= ar && bi > bl) {
        minXor = Math.min(minXor, fastXor(ai, ar, bl, bi - 1, bit - 1));
      }
    }

    return minXor;
  }

  long fastMst(int l, int r, int bit) {
    // Min MST of 0 or 1 vertices is 0.
    if (l >= r) {
      return 0;
    }
    // If we have run out of bits, but there are still nodes to build MST for, then those nodes are all with
    // equal values, so XOR-ing them would give 0.
    if (bit < 0) {
      return 0;
    }

    // Find the longest block of elements, for which the bit-th bit is 0.
    int i = l;
    for (; i <= r && ((a[i] >>> bit) & 1) == 0; ++i) ;

    // Split the vertices in two sets - one that has a 0 for its bit-th bit and one that has 1 for its bit-th bit,
    // and find the MST for both sets independently.
    // The Kruskal algorithm would have built MSTs for those subgraphs first, as the XOR of the bit-th bit inside those
    // subgraphs would be 0, whereas the XOR between vertices from different sets would be 1.
    long leftMst = fastMst(l, i - 1, bit - 1);
    long rightMst = fastMst(i, r, bit - 1);

    // Find the smallest edge between between the two sets (in case both sets are non-empty).
    long bridgeEdge = 0;
    if (i > l && i <= r) {
      bridgeEdge = fastXor(l, i - 1, i, r, bit - 1);
    }

    return leftMst + rightMst + bridgeEdge;
  }

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    n = in.readInt();
    a = new int[n];
    for (int i = 0; i < n; ++i) {
      a[i] = in.readInt();
    }
    SvArrays.sort(a);

    out.println(fastMst(0, n - 1, 29));
  }
}
