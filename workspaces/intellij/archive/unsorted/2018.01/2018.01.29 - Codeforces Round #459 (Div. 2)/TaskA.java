package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;

/**
 * CodeForces
 * Codeforces Round #459 (Div. 2)
 * http://codeforces.com/contest/918/problems
 * A. Eleven
 * <p>
 * Status: Accepted
 * Note: Solved during contest
 * Submission: http://codeforces.com/contest/918/submission/34668148
 * Niceness: 5/10
 * Difficulty: 2/10
 * Date: 2018-01-29
 * Author: Svilen Marchev
 * Tags: implementation, fibonacci
 * Analysis: ?
 */
public class TaskA {
  public void solve(int testNumber, InputReader in, PrintWriter out) {
    int n = in.readInt();
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < n; ++i) sb.append('o');

    sb.setCharAt(0, 'O');
    int f0 = 1, f1 = 1;
    while (true) {
      int f2 = f0 + f1;
      if (f2 > n) break;
      sb.setCharAt(f2 - 1, 'O');

      f0 = f1;
      f1 = f2;
    }
    out.println(sb.toString());
  }
}
