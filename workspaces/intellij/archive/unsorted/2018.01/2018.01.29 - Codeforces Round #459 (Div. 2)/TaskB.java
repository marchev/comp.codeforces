package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * CodeForces
 * Codeforces Round #459 (Div. 2)
 * http://codeforces.com/contest/918/problems
 * B. Radio Station
 * <p>
 * Status: Accepted
 * Note: Solved during contest
 * Submission: http://codeforces.com/contest/918/submission/34670857
 * Niceness: 3/10
 * Difficulty: 1/10
 * Date: 2018-01-29
 * Author: Svilen Marchev
 * Tags: implementation
 * Analysis: ?
 */
public class TaskB {
  public void solve(int testNumber, InputReader in, PrintWriter out) {
    int n = in.readInt();
    int m = in.readInt();
    Map<String, String> ipToName = new HashMap<>(1000);
    for (int i = 0; i < n; ++i) {
      String name = in.readString();
      String ip = in.readString();
      ipToName.put(ip + ";", name);
    }
    for (int i = 0; i < m; ++i) {
      String command = in.readString();
      String ip = in.readString();
      out.println(command + " " + ip + " #" + ipToName.get(ip));
    }
  }
}
