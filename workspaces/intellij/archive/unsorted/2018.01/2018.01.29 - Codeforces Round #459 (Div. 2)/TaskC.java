package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;

/**
 * CodeForces
 * Codeforces Round #459 (Div. 2)
 * http://codeforces.com/contest/918/problems
 * C. The Monster
 * <p>
 * Status: Accepted
 * Note: Solved during contest
 * Submission: http://codeforces.com/contest/918/submission/34681204
 * Niceness: 8/10
 * Difficulty: 5/10
 * Date: 2018-01-29
 * Author: Svilen Marchev
 * Tags: sweeping, sequences, brackets
 * Analysis: ?
 * <p>
 * TL;DR: Given a string of brackets with some of the symbols not filled (e.g. "((??)?)"), how many substrings
 * there are that can be completed to correct bracket sequences.
 * Desired complexity is O(N^2).
 */
public class TaskC {

  int n;
  String s;
  int ans = 0;

  void calc(int offset) {
    int minb = 0;
    int maxb = 0;
    for (int i = offset; i < n; ++i) {
      if (s.charAt(i) == ')') {
        minb--;
        maxb--;
      } else if (s.charAt(i) == '(') {
        minb++;
        maxb++;
      } else { // '?'
        maxb++;
        minb--;
      }

      if (minb < 0) {
        minb += 2;
      }
      if (minb > maxb) {
        return;
      }
      if (maxb < 0) {
        return;
      }

      if (minb == 0) {
        ans++;
      }
    }
  }

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    s = in.readString();
    n = s.length();
    for (int i = 0; i < n; ++i) {
      calc(i);
    }
    out.println(ans);
  }
}
