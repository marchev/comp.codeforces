package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;

/**
 * CodeForces
 * Codeforces Round #459 (Div. 2)
 * http://codeforces.com/contest/918/problems
 * D. MADMAX
 * <p>
 * Status: Accepted
 * Note: Solved during contest
 * Submission: http://codeforces.com/contest/918/submission/34677515
 * Niceness: 7/10
 * Difficulty: 4/10
 * Date: 2018-01-29
 * Author: Svilen Marchev
 * Tags: game theory, graphs, DAG
 * Analysis: ?
 * <p>
 * TL;DR: Standard game. Given as a direct acyclic graph.
 * Complexity is O(L * N^3), where L is the alphabet size, i.e. L=26.
 */
public class TaskD {

  char[][] a;
  int n, m;

  boolean[][][] isComputed;
  boolean[][][] isWin;

  boolean isWinning(int va, int vb, char c) {
    if (isComputed[va][vb][c]) {
      return isWin[va][vb][c];
    }

    boolean win = false;

    for (int kid = 0; kid < n && !win; ++kid) {
      if (a[va][kid] != 0 && a[va][kid] >= c) {
        win |= !isWinning(vb, kid, a[va][kid]);
      }
    }

    isComputed[va][vb][c] = true;
    isWin[va][vb][c] = win;
    return win;
  }

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    n = in.readInt();
    m = in.readInt();
    a = new char[n][n];
    for (int i = 0; i < m; ++i) {
      int va = in.readInt() - 1;
      int vb = in.readInt() - 1;
      String label = in.readString();
      a[va][vb] = label.charAt(0);
    }

    isComputed = new boolean[n][n][128];
    isWin = new boolean[n][n][128];

    for (int i = 0; i < n; ++i) {
      StringBuilder row = new StringBuilder();
      for (int j = 0; j < n; ++j) {
        row.append(isWinning(i, j, 'a') ? 'A' : 'B');
      }
      out.println(row.toString());
    }
  }
}
