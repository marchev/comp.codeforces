package bg.svilen.codeforces;

import net.egork.chelper.task.Test;
import net.egork.chelper.tester.TestCase;

import java.util.Collection;
import java.util.Collections;

public class TaskDTestCase {
  @TestCase
  public Collection<Test> createTests() {
    return Collections.singleton(createBigTest());
  }

  Test createBigTest() {
    int n = 100;
    int m = n * (n - 1) / 2;
    StringBuilder sb = new StringBuilder(n + " " + m + "\n");
    for (int i = 1; i <= n; ++i) {
      for (int j = i + 1; j <= n; ++j) {
        sb.append(i + " " + j + " a\n");
      }
    }
    return new Test(sb.toString(), "");
  }
}
