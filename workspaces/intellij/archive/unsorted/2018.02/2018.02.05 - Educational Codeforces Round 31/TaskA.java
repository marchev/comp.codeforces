package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;

/**
 * CodeForces
 * Educational Codeforces Round 31
 * http://codeforces.com/contest/884/problems
 * A. Book Reading
 * <p>
 * Status: Accepted
 * Note: Solved during the contest.
 * Submission: http://codeforces.com/contest/884/submission/34952522
 * Niceness: 3/10
 * Difficulty: 1/10
 * Date: 2018-02-05
 * Author: Svilen Marchev
 * Tags: implementation
 * Analysis: http://codeforces.com/blog/entry/55470
 * <p>
 * Time complexity: O(N).
 */
public class TaskA {
  public void solve(int testNumber, InputReader in, PrintWriter out) {
    int n = in.readInt();
    int t = in.readInt();
    for (int day = 1; day <= n; ++day) {
      int work = in.readInt();
      t -= 86400 - work;
      if (t <= 0) {
        out.println(day);
        return;
      }
    }
  }
}
