package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;

/**
 * CodeForces
 * Educational Codeforces Round 31
 * http://codeforces.com/contest/884/problems
 * B. Japanese Crosswords Strike Back
 * <p>
 * Status: Accepted
 * Note: Solved during the contest.
 * Submission: http://codeforces.com/contest/884/submission/34952676
 * Niceness: 4/10
 * Difficulty: 2/10
 * Date: 2018-02-05
 * Author: Svilen Marchev
 * Tags: implementation
 * Analysis: http://codeforces.com/blog/entry/55470
 * <p>
 * Time complexity: O(N).
 */
public class TaskB {
  public void solve(int testNumber, InputReader in, PrintWriter out) {
    int n = in.readInt();
    int x = in.readInt();
    int sum = 0;
    for (int i = 0; i < n; ++i) {
      sum += in.readInt();
    }
    out.println(sum + n - 1 == x ? "YES" : "NO");
  }
}
