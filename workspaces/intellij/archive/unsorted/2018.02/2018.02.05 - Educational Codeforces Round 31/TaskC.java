package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.stream.Collectors;

/**
 * CodeForces
 * Educational Codeforces Round 31
 * http://codeforces.com/contest/884/problems
 * C - Bertown Subway
 * <p>
 * Status: Accepted
 * Note: Solved during the contest.
 * Submission: http://codeforces.com/contest/884/submission/34953175
 * Niceness: 7/10
 * Difficulty: 4/10
 * Date: 2018-02-05
 * Author: Svilen Marchev
 * Tags: cycles, permutations, graphs
 * Analysis: http://codeforces.com/blog/entry/55470
 * <p>
 * Find all cycles, merge the longest two of them, and then count.
 * <p>
 * Time complexity: O(N log N). Can be solved in O(N).
 */
public class TaskC {
  public void solve(int testNumber, InputReader in, PrintWriter out) {
    int n = in.readInt();
    int[] next = new int[n];
    for (int i = 0; i < n; ++i) {
      next[i] = in.readInt() - 1;
    }

    ArrayList<Long> sizes = new ArrayList<>();
    boolean[] u = new boolean[n];
    for (int i = 0; i < n; ++i) {
      if (!u[i]) {
        long sz = 0;
        for (int j = i; !u[j]; j = next[j]) {
          u[j] = true;
          sz++;
        }
        sizes.add(sz);
      }
    }

    Collections.sort(sizes);
    if (sizes.size() > 1) {
      long newSz = sizes.get(sizes.size() - 1) + sizes.get(sizes.size() - 2);
      sizes.remove(sizes.size() - 1);
      sizes.set(sizes.size() - 1, newSz);
    }

    long ans = sizes.stream().collect(Collectors.summingLong(value -> value * value));
    out.println(ans);
  }
}
