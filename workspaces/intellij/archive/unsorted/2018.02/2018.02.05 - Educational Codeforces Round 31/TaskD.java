package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;
import java.util.PriorityQueue;

/**
 * CodeForces
 * Educational Codeforces Round 31
 * http://codeforces.com/contest/884/problems
 * D - Boxes And Balls
 * <p>
 * Status: Accepted
 * Submission: http://codeforces.com/contest/884/submission/34957470
 * Niceness: 7/10
 * Difficulty: 7/10
 * Date: 2018-02-05
 * Author: Svilen Marchev
 * Tags: greedy, huffman codes
 * Analysis: http://codeforces.com/blog/entry/55470
 * <p>
 * Just replay backwards the process of merging balls of different colors. The problem is identical to
 * building a Huffman prefix code with alphabet of size 3.
 * Initially, if n is even, add a formal group of 0 balls to make it odd.
 * Until you have just one group left: At every step backwards merge the 3 smallest groups.
 * <p>
 * Time complexity: O(N log N).
 */
public class TaskD {
  public void solve(int testNumber, InputReader in, PrintWriter out) {
    int n = in.readInt();
    PriorityQueue<Long> queue = new PriorityQueue<>(n);
    for (int i = 0; i < n; ++i) {
      long a = in.readInt();
      queue.add(a);
    }
    if (n % 2 == 0) {
      queue.add(0L);
    }

    long ans = 0;
    while (queue.size() > 1) {
      long a = queue.poll();
      long b = queue.poll();
      long c = queue.poll();
      ans += a + b + c;
      queue.add(a + b + c);
    }
    out.println(ans);
  }
}
