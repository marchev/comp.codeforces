package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;
import java.util.Arrays;

/**
 * CodeForces
 * Educational Codeforces Round 31
 * http://codeforces.com/contest/884/problems
 * E - Binary Matrix
 * <p>
 * Status: Accepted
 * Submission: http://codeforces.com/contest/884/submission/35074226
 * Niceness: 7/10
 * Difficulty: 7/10
 * Date: 2018-02-09
 * Author: Svilen Marchev
 * Tags: dsu, disjoint set union
 * Analysis: http://codeforces.com/blog/entry/55470
 * <p>
 * Go through the rows, keeping in memory only the last 2 of them. Represent the connected components
 * in the last two rows as disjoint set union to preserve memory. After processing each row,
 * restructure the DSU to remove the previous row. The answer is: number-of-1s-in-the-grid - number-of-merge-operations.
 * <p>
 * Time complexity: O(N M log M). Space: O(M).
 */
public class TaskE {

  int n, m;
  int[] parent;

  int convertCharToBitmask(char c) {
    if ('0' <= c && c <= '9') {
      return c - '0';
    }
    return c - 'A' + 10;
  }

  int getRoot(int i) {
    int root = i;
    while (parent[root] != -1) {
      root = parent[root];
    }
    while (i != root) {
      int oldparent = parent[i];
      parent[i] = root;
      i = oldparent;
    }
    return root;
  }

  int join(int i, int j) {
    int rooti = getRoot(i);
    int rootj = getRoot(j);
    if (rooti == rootj) {
      return 0;
    }
    parent[rootj] = rooti;
    return 1;
  }

  void readRow(byte[] row, InputReader in) {
    for (int colblock = 0; colblock < m / 4; ++colblock) {
      int block = convertCharToBitmask(in.readCharacter());
      for (int c = 0; c < 4; ++c) {
        row[colblock * 4 + c] = ((block >>> (3 - c)) & 1) == 1 ? (byte) 1 : 0;
      }
    }
  }

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    n = in.readInt();
    m = in.readInt();

    byte[][] rows = new byte[2][m];

    // DSU
    parent = new int[2 * m];
    Arrays.fill(parent, -1);

    int[] remap = new int[2 * m];
    int[] rootsInCurRow = new int[m];

    int numJoins = 0;
    int numOnes = 0;

    for (int i = 0; i < n; ++i) {
      byte[] currow = rows[i % 2];
      byte[] prevrow = rows[(i + 1) % 2];
      readRow(currow, in);

      for (int j = 0; j < m; ++j) {
        if (currow[j] == 0) continue;
        numOnes++;
        if (i > 0 && prevrow[j] == 1) numJoins += join(j, m + j);
        if (j > 0 && currow[j - 1] == 1) numJoins += join(m + j - 1, m + j);
      }

      // Save the roots of each element of the cur row, before we remove the elements of the
      // previous row.
      for (int j = 0; j < m; ++j) {
        rootsInCurRow[j] = getRoot(m + j);
      }
      Arrays.fill(remap, -1);
      for (int j = 0; j < m; ++j) {
        int curroot = rootsInCurRow[j];
        if (remap[curroot] == -1) {
          remap[curroot] = j;
          parent[j] = -1;
        } else {
          parent[j] = remap[curroot];
        }
      }
      for (int j = 0; j < m; ++j) {
        parent[m + j] = -1;
      }
    }

    out.println(numOnes - numJoins);
  }
}
