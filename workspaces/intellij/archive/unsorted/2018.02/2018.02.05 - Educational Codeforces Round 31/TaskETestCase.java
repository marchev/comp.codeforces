package bg.svilen.codeforces;

import net.egork.chelper.task.Test;
import net.egork.chelper.tester.TestCase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class TaskETestCase {
  @TestCase
  public Collection<Test> createTests() {
    return Arrays.asList(new Test[]{createLargeTest()});
  }

  Test createLargeTest() {
    int n = 4096;
    int m = 16384;

    StringBuilder sb = new StringBuilder(n*m/4);
    sb.append(n + " " + m + "\n");
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < m / 4; ++j) {
        sb.append("F");
      }
      sb.append("\n");
    }
    return new Test(sb.toString(), "1\n");
  }
}
