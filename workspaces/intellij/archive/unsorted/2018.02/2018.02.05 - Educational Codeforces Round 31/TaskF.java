package bg.svilen.codeforces;

import net.egork.collections.Pair;
import net.egork.graph.Graph;
import net.egork.graph.MinCostFlow;
import net.egork.utils.io.InputReader;

import java.io.PrintWriter;

/**
 * CodeForces
 * Educational Codeforces Round 31
 * http://codeforces.com/contest/884/problems
 * F - Anti-Palindromize
 * <p>
 * Status: Accepted
 * Submission: http://codeforces.com/contest/884/submission/35179228
 * Niceness: 8/10
 * Difficulty: 7/10
 * Date: 2018-02-12
 * Author: Svilen Marchev
 * Tags: sequences, min-cost max flow
 * Analysis: http://codeforces.com/blog/entry/55470
 * <p>
 * Analysis by zscoder (in a comment below the official analysis):
 * <p>
 * Build a flow graph where there is a vertex corresponding to each letter of the alphabet and 0.5n vertices
 * corresponding to each pair of letters (s_i, s_n - 1 - i). For each letter of the alphabet c, add an edge of capacity 1
 * to each of the 0.5n vertices, where the cost should be  - max(a_i, a_n - 1 - i) if both characters are originally c,
 * cost 0 of neither was equal to c, cost  - a_i if only the i-th character is c and vice versa.
 * <p>
 * Add an edge of cap cnt_i and cost 0 from sink to each letter, where cnt_i is the frequency of the i-th letter.
 * Add an edge from each vertex denoting a pair with cap 2, cost 0 to sink. It is easy to see that the minimum cost
 * of the min cost max flow is the negative of the answer.
 * <p>
 * Time complexity: O(min-cost max flow).
 */
public class TaskF {

  int n;
  char[] s;
  int[] ugliness;

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    n = in.readInt();
    s = in.readString().toCharArray();
    ugliness = new int[n];
    for (int i = 0; i < n; ++i) {
      ugliness[i] = -in.readInt();
    }

    Graph graph = new Graph(1 + 26 + n / 2 + 1);
    for (char c = 'a'; c <= 'z'; ++c) {
      for (int i = 0; i < n / 2; ++i) {
        int cost = 0;
        if (c == s[i]) {
          cost = Math.min(cost, ugliness[i]);
        }
        if (c == s[n - 1 - i]) {
          cost = Math.min(cost, ugliness[n - 1 - i]);
        }
        graph.addFlowWeightedEdge(1 + (c - 'a'), 1 + 26 + i, cost, 1);
      }
    }

    int[] occurs = new int[128];
    for (int i = 0; i < n; ++i) occurs[s[i]]++;
    for (char c = 'a'; c <= 'z'; ++c) {
      graph.addFlowWeightedEdge(0, 1 + (c - 'a'), 0, occurs[c]);
    }

    for (int i = 0; i < n / 2; ++i) {
      graph.addFlowWeightedEdge(1 + 26 + i, 1 + 26 + n / 2, 0, 2);
    }

    Pair<Long, Long> flow = MinCostFlow.minCostMaxFlow(graph, 0, 1 + 26 + n / 2, true, n);
    out.println(-flow.first);
  }
}
