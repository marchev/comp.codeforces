package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * CodeForces
 * Educational Codeforces Round 31
 * http://codeforces.com/contest/884/problems
 * F - Anti-Palindromize
 * <p>
 * Status: Accepted
 * Submission: http://codeforces.com/contest/884/submission/35177218
 * Niceness: 8/10
 * Difficulty: 7/10
 * Date: 2018-02-12
 * Author: Svilen Marchev
 * Tags: greedy, sequences
 * Analysis: http://codeforces.com/blog/entry/55470
 * <p>
 * This problem has two different solutions: a mincost maxflow approach and a greedy one. We will tell you about
 * the latter.
 * <p>
 * First of all, let t = s. Then find all pairs of indices (i, n - i + 1) such that ti = tn - i + 1
 * (let the number of these pairs be m). It's obvious that we have to replace at least one letter in each of these pairs.
 * For each of these pairs let's replace the letter with lower bi with something. Let's analyze the letters we are
 * going to replace.
 * <p>
 * Let cnt(x) be the number of occurences of letter x that we have to replace. There are two cases:
 * <li>There is no letter x such that 2 * cnt(x) > m. Then we can replace these letters without involving anything
 * else and get an antipalindromic string with minimal possible cost;
 * <li>There is a letter x such that 2 * cnt(x) > m. It's obvious that there is at most one such letter. Let's replace
 * some occurences of x with other letters that are to be replaced. Then we will still have some occurences of x that
 * need to be replaced. Let's take one letter from each pair such that both of letters in a pair are not equal to x.
 * Among these possibilities choose the required number of letters with minimum values of bi. Then we can replace
 * remaining occurences of x with these letters.
 * <p>
 * Time complexity: O(N log N). Space: O(N).
 */
public class TaskFGreedy {

  int n;
  char[] s;
  int[] beauty;

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    n = in.readInt();
    s = in.readString().toCharArray();
    beauty = new int[n];
    int totalBeauty = 0;
    for (int i = 0; i < n; ++i) {
      beauty[i] = in.readInt();
      totalBeauty += beauty[i];
    }

    // Find all pairs of indices (i, n-1-i) for which s[i]=s[n-1-i] and the most common char among those pairs.
    List<Integer> equalPairs = new ArrayList<>(n / 2);
    int[] charOccurrencesInEqualPairs = new int[128];
    char mostCommonCharInEqualPairs = 'a';
    for (int i = 0; i < n / 2; ++i) {
      if (s[i] == s[n - 1 - i]) {
        equalPairs.add(i);
        if (++charOccurrencesInEqualPairs[s[i]] > charOccurrencesInEqualPairs[mostCommonCharInEqualPairs]) {
          mostCommonCharInEqualPairs = s[i];
        }
      }
    }

    // These pairs must be broken up in any case, so account for them in the answer.
    int ans = totalBeauty;
    for (int equalPair : equalPairs) {
      ans -= Math.min(beauty[equalPair], beauty[n - 1 - equalPair]);
    }

    // If more than half of the pairs are with the same char, then we would not be able to reshuffle
    // the chars within the pairs, so we would need to break up some of the pairs with non-equal chars.
    // We would need to consider only pairs for which the chars are different AND neither of the two
    // chars is equal to the most common char in the "equal" pairs (otherwise swapping that char with
    // either of the chars in the "non-equal" pair would result in a non-palindromic pair).
    if (charOccurrencesInEqualPairs[mostCommonCharInEqualPairs] > equalPairs.size() / 2) {
      List<Integer> nonEqualPairsMinBeautyPenalties = new ArrayList<>(n / 2);
      for (int i = 0; i < n / 2; ++i) {
        if (s[i] != s[n - 1 - i] && s[i] != mostCommonCharInEqualPairs && s[n - 1 - i] != mostCommonCharInEqualPairs) {
          nonEqualPairsMinBeautyPenalties.add(Math.min(beauty[i], beauty[n - 1 - i]));
        }
      }
      Collections.sort(nonEqualPairsMinBeautyPenalties);
      int rem = 2 * charOccurrencesInEqualPairs[mostCommonCharInEqualPairs] - equalPairs.size();
      for (int i = 0; i < rem; ++i) {
        ans -= nonEqualPairsMinBeautyPenalties.get(i);
      }
    }

    out.println(ans);
  }
}
