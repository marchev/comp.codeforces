package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;

/**
 * CodeForces
 * Codeforces Round #464 (Div. 2)
 * http://codeforces.com/contest/939/problems
 * A - Love Triangle
 * <p>
 * Status: Accepted
 * Note: Solved during the contest
 * Submission: http://codeforces.com/contest/939/submission/35397786
 * Niceness: 4/10
 * Difficulty: 3/10
 * Date: 2018-02-17
 * Author: Svilen Marchev
 * Tags: sequence, cycles
 * Analysis: ?
 * <p>
 * Time: O(N).
 */
public class TaskA {

  int n;
  int[] f;

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    n = in.readInt();
    f = new int[n];
    for (int i = 0; i < n; ++i) {
      f[i] = in.readInt() - 1;
    }

    for (int i = 0; i < n; ++i) {
      int ni = f[i];
      int nni = f[ni];
      if (nni != i) {
        int nnni = f[nni];
        if (nnni == i) {
          out.println("YES");
          return;
        }
      }
    }
    out.println("NO");
  }
}
