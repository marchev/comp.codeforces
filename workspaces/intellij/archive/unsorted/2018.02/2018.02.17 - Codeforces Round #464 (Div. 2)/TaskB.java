package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;

/**
 * CodeForces
 * Codeforces Round #464 (Div. 2)
 * http://codeforces.com/contest/939/problems
 * B - Hamster Farm
 * <p>
 * Status: Accepted
 * Note: Solved during the contest
 * Submission: http://codeforces.com/contest/939/submission/35395791
 * Niceness: 4/10
 * Difficulty: 2/10
 * Date: 2018-02-17
 * Author: Svilen Marchev
 * Tags: simple math, remainders
 * Analysis: ?
 * <p>
 * Time: O(k).
 */
public class TaskB {
  public void solve(int testNumber, InputReader in, PrintWriter out) {
    long n = in.readLong();
    int k = in.readInt();

    int bestId = 1;
    long bestA = 1;
    long minRem = Long.MAX_VALUE;

    for (int i = 0; i < k; ++i) {
      long a = in.readLong();
      long rem = n % a;
      if (rem < minRem) {
        minRem = rem;
        bestA = a;
        bestId = i + 1;
      }
    }

    out.println(bestId + " " + (n / bestA));
  }
}
