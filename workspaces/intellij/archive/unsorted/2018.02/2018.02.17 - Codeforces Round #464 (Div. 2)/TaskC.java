package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;

/**
 * CodeForces
 * Codeforces Round #464 (Div. 2)
 * http://codeforces.com/contest/939/problems
 * C - Convenient For Everybody
 * <p>
 * Status: Accepted
 * Submission: http://codeforces.com/contest/939/submission/35428536
 * Niceness: 4/10
 * Difficulty: 5/10
 * Date: 2018-02-17
 * Author: Svilen Marchev
 * Tags: interval tree
 * Analysis: ?
 * <p>
 * Add all intervals to an interval tree and then query the tree at all N time points. The answer is the time point
 * with largest value. My mistake during the contest was that I was adding the timezone offset instead of
 * subtracting it.
 * <p>
 * Can be solved with sweeping as well.
 * <p>
 * Time: O(n log n).
 */
public class TaskC {
  int N = 1 << 17;

  static class Ival {
    int l, r;

    Ival set(int l, int r) {
      this.l = l;
      this.r = r;
      return this;
    }
  }

  static class Node {
    int timesCovered;
  }

  Node[] tree = new Node[2 * N];

  void insertIval(int nodeIx, int treeL, int treeR, Ival ival, int delta) {
    if (ival.l <= treeL && treeR <= ival.r) {
      tree[nodeIx].timesCovered += delta;
      return;
    }
    int treeM = (treeL + treeR) / 2;
    if (ival.l <= treeM) {
      insertIval(nodeIx * 2, treeL, treeM, ival, delta);
    }
    if (treeM + 1 <= ival.r) {
      insertIval(nodeIx * 2 + 1, treeM + 1, treeR, ival, delta);
    }
  }

  int query(int point) {
    int nodeIx = N + point;
    int sum = 0;
    while (nodeIx > 0) {
      sum += tree[nodeIx].timesCovered;
      nodeIx /= 2;
    }
    return sum;
  }

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    int n = in.readInt();
    int[] a = new int[n];
    for (int i = 0; i < n; ++i) {
      a[i] = in.readInt();
    }
    int s = in.readInt() - 1;
    int f = in.readInt() - 2;

    for (int i = 0; i < 2 * N; ++i) {
      tree[i] = new Node();
    }

    Ival ival = new Ival();
    for (int timezone = 0; timezone < n; ++timezone) {
      int l = (s - timezone + n) % n;
      int r = (f - timezone + n) % n;
      if (l > r) {
        insertIval(1, 0, N - 1, ival.set(l, n - 1), a[timezone]);
        insertIval(1, 0, N - 1, ival.set(0, r), a[timezone]);
      } else {
        insertIval(1, 0, N - 1, ival.set(l, r), a[timezone]);
      }
    }

    int maxVal = 0;
    int maxTime = 0;
    for (int time = 0; time < n; ++time) {
      int val = query(time);
      if (maxVal < val) {
        maxVal = val;
        maxTime = time;
      }
    }

    out.println(maxTime + 1);
  }
}
