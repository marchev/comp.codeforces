package bg.svilen.codeforces;

import bg.svilen.comp.lib.ds.SvDisjointSetUnion;
import net.egork.collections.Pair;
import net.egork.utils.io.InputReader;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * CodeForces
 * Codeforces Round #464 (Div. 2)
 * http://codeforces.com/contest/939/problems
 * D - Love Rescue
 * <p>
 * Status: Accepted
 * Submission: http://codeforces.com/contest/939/submission/35421716
 * Niceness: 9/10
 * Difficulty: 6/10
 * Date: 2018-02-17
 * Author: Svilen Marchev
 * Tags: disjoint set union
 * Analysis: ?
 * <p>
 * Find all pairs of chars that are different in the two strings. It's clear that the chars in each pairs
 * have to be made equal, i.e. such a pair states that the two chars of it are in the same class of equivalence.
 * We can represent those classes of equivalence as disjoint set union. Initially, each chars is in its own set.
 * Then we go through all pairs and merge the two classes, to which the chars belong. If the chars were already
 * into the same set, then we don't do anything as they are already "made equal" (i.e. to the char that is a root
 * of the disjoint set); if they were not, then we need to buy a spell that that makes them equal.
 * <p>
 * Time: O(n log a), where a=26 is the alphabet size.
 */
public class TaskD {
  public void solve(int testNumber, InputReader in, PrintWriter out) {
    int n = in.readInt();
    char[] a = in.readString().toCharArray();
    char[] b = in.readString().toCharArray();

    SvDisjointSetUnion dsu = new SvDisjointSetUnion(26);
    List<Pair<Character, Character>> merges = new ArrayList<>();

    for (int i = 0; i < n; ++i) {
      if (a[i] != b[i]) {
        if (dsu.join(a[i] - 'a', b[i] - 'a')) {
          merges.add(Pair.makePair(a[i], b[i]));
        }
      }
    }

    out.println(merges.size());
    for (Pair<Character, Character> merge : merges) {
      out.println(merge.first + " " + merge.second);
    }
  }
}
