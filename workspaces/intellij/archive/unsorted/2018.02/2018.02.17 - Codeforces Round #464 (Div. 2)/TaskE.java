package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;

/**
 * CodeForces
 * Codeforces Round #464 (Div. 2)
 * http://codeforces.com/contest/939/problems
 * E - Maximize!
 * <p>
 * Status: Accepted
 * Submission: http://codeforces.com/contest/939/submission/35429927
 * Niceness: 8/10
 * Difficulty: 7/10
 * Date: 2018-02-17
 * Author: Svilen Marchev
 * Tags: cutting the inner loop, ternary search, simple math, induction
 * Analysis: ?
 * <p>
 * It's clear that when we have only one number a_1, the optimum subset is S={a_1} and max(S)-mean(S)=0.
 * Now let's assume that we know the optimum subset S for the first i numbers, a_1,a_2,...,a_i, and we add a new
 * element a_i+1 and we would like to find the new optimum subset S_new. There are two possibilities:
 * - a_i+1 does not participate in the new optimum subset S_new. This means that the optimum subset remains S, i.e. S_new = S.
 * - a_i+1 does participate in the new optimum subset S_new. It is clear that in this case a_i+1 will be the
 * max element of S_new. In order to find the max of f(S_new)=max(S_new)-mean(S_new) we take as many from the numbers
 * as possible, starting from the smallest, while the mean() goes down and we stop only when it starts growing again.
 * Note that the mean() first gets smaller, then starts increasing - this property can be used for an efficient
 * calculation of the lowest median - using one ternary search for each added number. Another efficient approach would
 * come after observing that whenever a new, larger number is added, the min mean() can just require more numbers
 * than the min mean() from the previous step. So we can just memorize at which index the lowest mean was
 * achieved and then start form there when we search the mean for the next added number. This approach gives us
 * a nice O(Q) amortized complexity.
 * <p>
 * Time: O(Q).
 */
public class TaskE {
  final int MQ = 500_000;

  int[] a = new int[MQ];
  int an = 0;

  double[] prefsum = new double[MQ + 1]; // prefix sums of a[]
  double maxDiff = 0;
  int lastI = 0; // used for cutting the inner loop

  void update(int newA) {
    double max = newA;
    double mean = newA;

    double sum = newA + prefsum[lastI];
    for (int i = lastI; i < an; ++i) {
      sum += a[i];
      double nextMean = sum / (i + 2);
      if (nextMean > mean) {
        break;
      }
      mean = nextMean;
      lastI = i;
    }

    if (max - mean > maxDiff) {
      maxDiff = max - mean;
    }

    prefsum[an + 1] = prefsum[an] + newA;
    a[an] = newA;
    an++;
  }

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    int q = in.readInt();
    for (int i = 0; i < q; ++i) {
      int type = in.readInt();
      if (type == 1) {
        int newA = in.readInt();
        update(newA);
      } else {
        out.println(String.format("%.10f", maxDiff));
      }
    }
  }
}
