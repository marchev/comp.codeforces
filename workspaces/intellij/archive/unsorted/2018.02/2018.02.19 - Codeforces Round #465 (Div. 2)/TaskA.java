package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;

// Accepted
// during contest
// 2018-02-19
public class TaskA {
  public void solve(int testNumber, InputReader in, PrintWriter out) {
    int n = in.readInt();
    int ans = 0;
    for (int l = 1; l < n; ++l) {
      if ((n - l) % l == 0 && (n - l) / l > 0) {
        ans++;
      }
    }
    out.println(ans);
  }
}
