package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;

// Accepted
// during contest
// 2018-02-19
public class TaskB {
  public void solve(int testNumber, InputReader in, PrintWriter out) {
    int n = in.readInt();
    String s = in.readString();

    int coins = 0;
    int lastKingdom = s.charAt(0) == 'U' ? 2 : 1;
    int x = 0;
    int y = 0;
    for (int i = 0; i < n; ++i) {
      boolean wasAtBorder = x == y;
      if (s.charAt(i) == 'U') y++;
      else x++;
      if (x != y && wasAtBorder) {
        int curKingdom = x > y ? 1 : 2;
        if (curKingdom != lastKingdom) {
          coins++;
          lastKingdom = curKingdom;
        }
      }
    }
    out.println(coins);
  }
}
