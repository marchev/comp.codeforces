package bg.svilen.codeforces;

import net.egork.geometry.Circle;
import net.egork.geometry.Point;
import net.egork.geometry.Segment;
import net.egork.utils.io.InputReader;

import java.io.PrintWriter;

// Accepted
// during contest
// 2018-02-19
public class TaskC {
  int dx, dy;

  void print(double x, double y, double r, PrintWriter out) {
    out.println(String.format("%.12f %.12f %.12f", dx + x, dy + y, r));
  }

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    int R = in.readInt();
    int x1 = in.readInt();
    int y1 = in.readInt();
    int x2 = in.readInt() - x1;
    int y2 = in.readInt() - y1;

    dx = x1;
    dy = y1;
    x1 = y1 = 0;

    Point originPoint = new Point(0, 0);
    Point badPoint = new Point(x2, y2);

    if (x2 == 0 && y2 == 0) {
      print(R / 2.0, 0.0, R / 2.0, out);
    } else {
      Segment seg = new Segment(originPoint, badPoint);
      if (seg.length() >= R) {
        print(0, 0, R, out);
      } else {
        Circle circle = new Circle(originPoint, R);
        Point[] inters = seg.line().intersect(circle);

        double d0 = new Segment(badPoint, inters[0]).length();
        double d1 = new Segment(badPoint, inters[1]).length();
        Point furtherPoint = inters[d0 > d1 ? 0 : 1];

        print((furtherPoint.x + badPoint.x) / 2.0, (furtherPoint.y + badPoint.y) / 2.0, Math.max(d0, d1) / 2.0, out);
      }
    }
  }
}
