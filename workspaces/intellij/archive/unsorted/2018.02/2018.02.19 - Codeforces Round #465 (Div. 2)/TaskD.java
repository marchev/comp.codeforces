package bg.svilen.codeforces;

import bg.svilen.comp.lib.debug.SvLogger;
import bg.svilen.comp.lib.math.SvNumberTheoryUtils;
import net.egork.utils.io.InputReader;

import java.io.PrintWriter;
import java.util.Arrays;

// Accepted
// 2018-02-19
// http://codeforces.com/contest/935/submission/35502510
public class TaskD {
  SvLogger logger = new SvLogger().setEnabled(false);

  final int M = 1_000_000_007;

  int n;
  long alpha;
  int[] a;
  int[] b;
  long[] ps;
  long[] all;

  long comp(int i) {
    if (i == n) {
      return 0;
    }
    if (ps[i] != -1) {
      return ps[i];
    }
    long p = 0;

    if (a[i] > 0 && b[i] > 0) {
      if (a[i] == b[i]) {
        p += comp(i + 1);
      } else if (a[i] > b[i]) {
        p += all[i + 1];
      }
    }

    if (a[i] == 0 && b[i] > 0) {
      p += comp(i + 1);
      p += ((alpha - b[i]) * all[i + 1]) % M;
    }

    if (a[i] > 0 && b[i] == 0) {
      p += comp(i + 1);
      p += ((a[i] - 1) * all[i + 1]) % M;
    }

    if (a[i] == 0 && b[i] == 0) {
      p += (alpha * comp(i + 1)) % M;
      p += (((alpha * (alpha - 1) / 2) % M) * all[i + 1]) % M;
    }

    p = p % M;
    ps[i] = p;
    return p;
  }

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    n = in.readInt();
    alpha = in.readInt();
    a = new int[n];
    b = new int[n];
    for (int i = 0; i < n; ++i) {
      a[i] = in.readInt();
    }
    for (int i = 0; i < n; ++i) {
      b[i] = in.readInt();
    }

    ps = new long[n + 1];
    Arrays.fill(ps, -1);

    all = new long[n + 1];
    all[n] = 1;
    for (int i = n - 1; i >= 0; --i) {
      all[i] = all[i + 1];
      if (a[i] == 0) all[i] = (all[i] * alpha) % M;
      if (b[i] == 0) all[i] = (all[i] * alpha) % M;
    }

    long p = comp(0);
    long q = all[0];
    logger.p(p, q);
    logger.p(Arrays.toString(all));

    long qm = SvNumberTheoryUtils.multiplicativeInverse((int) q, M);
    long ans = (p * qm) % M;
    out.println(ans);
  }
}
