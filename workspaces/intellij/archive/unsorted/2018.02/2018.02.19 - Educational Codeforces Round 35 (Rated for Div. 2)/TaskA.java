package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;

// Accepted
// 2018-02-19
public class TaskA {
  public void solve(int testNumber, InputReader in, PrintWriter out) {
    int n = in.readInt();
    int[] a = new int[n];
    int min = Integer.MAX_VALUE;
    for (int i = 0; i < n; ++i) {
      a[i] = in.readInt();
      min = Math.min(min, a[i]);
    }

    int minDist = Integer.MAX_VALUE;
    int lastIx = -1;
    for (int i = 0; i < n; ++i) {
      if (a[i] == min) {
        if (lastIx != -1) {
          minDist = Math.min(minDist, i - lastIx);
        }
        lastIx = i;
      }
    }
    out.println(minDist);
  }
}
