package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;

// Accepted
// 2018-02-19
public class TaskB {
  public void solve(int testNumber, InputReader in, PrintWriter out) {
    int n = in.readInt();
    int a = in.readInt();
    int b = in.readInt();
    for (int x = Math.min(a, b); x >= 1; --x) {
      if (a / x + b / x >= n) {
        out.println(x);
        break;
      }
    }
  }
}
