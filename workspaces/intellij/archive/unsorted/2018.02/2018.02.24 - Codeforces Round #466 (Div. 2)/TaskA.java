package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;
import java.util.Arrays;

/**
 * CodeForces
 * Codeforces Round #466 (Div. 2)
 * http://codeforces.com/contest/940/problems
 * A - Points on the line
 * <p>
 * Status: Accepted
 * Note: Solved during the contest
 * Submission: ?
 * Niceness: ?/10
 * Difficulty: ?/10
 * Date: 2018-02-24
 * Author: Svilen Marchev
 * Tags: ?
 * Analysis: ?
 */
public class TaskA {
  public void solve(int testNumber, InputReader in, PrintWriter out) {
    int n = in.readInt();
    int d = in.readInt();
    int[] a = new int[n];
    for (int i = 0; i < n; ++i) {
      a[i] = in.readInt();
    }

    Arrays.sort(a);
    int ans = n;
    for (int minix = 0; minix < n; ++minix) {
      for (int maxix = minix; maxix < n; ++maxix) {
        if (a[maxix] - a[minix] <= d) {
          ans = Math.min(ans, n - (maxix - minix + 1));
        }
      }
    }
    out.println(ans);
  }
}
