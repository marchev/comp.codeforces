package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;

/**
 * CodeForces
 * Codeforces Round #466 (Div. 2)
 * http://codeforces.com/contest/940/problems
 * B - Our Tanya is Crying Out Loud
 * <p>
 * Status: Accepted
 * Note: Solved during the contest
 * Submission: ?
 * Niceness: ?/10
 * Difficulty: ?/10
 * Date: 2018-02-24
 * Author: Svilen Marchev
 * Tags: ?
 * Analysis: ?
 */
public class TaskB {
  public void solve(int testNumber, InputReader in, PrintWriter out) {
    long n = in.readLong();
    long k = in.readLong();
    long A = in.readLong();
    long B = in.readLong();

    long ans = 0;
    if (n == 1) {
      ans = 0;
    } else if (k == 1) {
      ans = (n - 1) * A;
    } else {
      long x = n;
      while (x > 1) {
        long rem = x % k;
        if (rem != 0) {
          long toRemove = Math.min(rem, x - 1);
          ans += A * toRemove;
          x -= toRemove;
        } else {
          long reduce = x - x / k;
          if (B <= reduce * A) {
            ans += B;
          } else {
            ans += A * reduce;
          }
          x -= reduce;
        }
      }
    }

    out.println(ans);
  }
}
