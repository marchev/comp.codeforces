package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;
import java.util.Set;
import java.util.TreeSet;

/**
 * CodeForces
 * Codeforces Round #466 (Div. 2)
 * http://codeforces.com/contest/940/problems
 * C - Phone Numbers
 * <p>
 * Status: Accepted
 * Note: Solved during the contest
 * Submission: ?
 * Niceness: ?/10
 * Difficulty: ?/10
 * Date: 2018-02-24
 * Author: Svilen Marchev
 * Tags: ?
 * Analysis: ?
 */
public class TaskC {

  int n, k;
  char[] s;
  char[] alph;
  int alphSz;

  char nextChar(char c) {
    for (int i = 0; i < alphSz; ++i) {
      if (c == alph[i]) {
        return alph[i + 1];
      }
    }
    throw new IllegalStateException();
  }

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    n = in.readInt();
    k = in.readInt();
    s = in.readString().toCharArray();
    Set<Character> charSet = new TreeSet<>();
    for (char c : s) {
      charSet.add(c);
    }
    alph = new char[charSet.size()];
    alphSz = 0;
    for (Character c : charSet) {
      alph[alphSz++] = c;
    }

    StringBuilder ans = new StringBuilder();

    if (k > n) {
      ans.append(s);
      for (int i = 0; i < k - n; ++i) ans.append(alph[0]);
    } else {
      for (int i = k - 1; i >= 0; --i) {
        if (s[i] != alph[alphSz - 1]) {
          for (int p = 0; p < i; ++p) ans.append(s[p]);
          ans.append(nextChar(s[i]));
          for (int p = i + 1; p < k; ++p) ans.append(alph[0]);

          break;
        }
      }
    }

    out.println(ans.toString());
  }
}
