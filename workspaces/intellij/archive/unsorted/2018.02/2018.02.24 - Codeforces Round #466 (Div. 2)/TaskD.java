package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;

/**
 * CodeForces
 * Codeforces Round #466 (Div. 2)
 * http://codeforces.com/contest/940/problems
 * D - Alena And The Heater
 * <p>
 * Status: Accepted
 * Note: Solved during the contest
 * Submission: ?
 * Niceness: ?/10
 * Difficulty: ?/10
 * Date: 2018-02-24
 * Author: Svilen Marchev
 * Tags: ?
 * Analysis: ?
 */
public class TaskD {

  int n;
  int[] a;
  int[] b;

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    n = in.readInt();
    a = new int[n];
    b = new int[n];
    for (int i = 0; i < n; ++i) {
      a[i] = in.readInt();
    }
    String bs = in.readString();
    for (int i = 0; i < n; ++i) {
      b[i] = bs.charAt(i) == '0' ? 0 : 1;
    }

    int lowerL = -1_000_000_000;
    int upperR = +1_000_000_000;
    for (int i = 4; i < n; ++i) {
      if (b[i] != b[i - 1]) {
        if (b[i - 1] == 0 && b[i - 2] == 0 && b[i - 3] == 0 && b[i - 4] == 0) {
          for (int off = 0; off < 5; off++) {
            lowerL = Math.max(lowerL, a[i - off] + 1);
          }
        } else {
          for (int off = 0; off < 5; off++) {
            upperR = Math.min(upperR, a[i - off] - 1);
          }
        }
      }
    }

    out.println(lowerL + " " + upperR);
  }
}
