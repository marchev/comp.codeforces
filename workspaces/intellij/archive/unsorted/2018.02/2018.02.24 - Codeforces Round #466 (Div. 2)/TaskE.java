package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;
import java.util.Arrays;

// wrong
public class TaskE {
  int n, c;
  int[] a;

//  long sumNext(int startIx, int toRemoveInNextPartition, int nextPartionSz) {
//    A
//
//    IntList list = new IntArrayList(nextPartionSz);
//    for (int i = 0; i < nextPartionSz; ++i) {
//      a
//    }
//  }

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    n = in.readInt();
    c = in.readInt();
    a = new int[n];
    for (int i = 0; i < n; ++i) {
      a[i] = in.readInt();
    }

    long ans = 0;

    int i = 0;
    while (i < n) {
      int rem = n - i;
      int toRemoveInNextPartition = rem / c;
      if (toRemoveInNextPartition == 0) {
        for (int p = i; p < n; ++p) ans += a[p];
        i = n;
      } else {
        int nextPartionSz = toRemoveInNextPartition * c;
        Arrays.sort(a, i, i + nextPartionSz);
//        ans += sumNext(i, toRemoveInNextPartition, nextPartionSz);
        for (int p = i + toRemoveInNextPartition; p < i + nextPartionSz; ++p) ans += a[p];
        i += nextPartionSz;
      }
    }

    out.println(ans);
  }
}
