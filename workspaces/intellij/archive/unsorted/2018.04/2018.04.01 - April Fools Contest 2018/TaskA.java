package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;

/**
 * CodeForces
 * April Fools Contest 2018
 * http://codeforces.com/contest/952/problems
 * A. Quirky Quantifiers
 * <p>
 * Status: Accepted
 * Note: Solved during the contest
 * Submission: http://codeforces.com/contest/952/submission/36805147
 * Niceness: 2/10
 * Difficulty: 2/10
 * Date: 2018-04-01
 * Author: Svilen Marchev
 * Tags: implementation, guessing
 * Analysis: ?
 */
public class TaskA {
  public void solve(int testNumber, InputReader in, PrintWriter out) {
    int n = in.readInt();
    out.println(n % 2);
  }
}
