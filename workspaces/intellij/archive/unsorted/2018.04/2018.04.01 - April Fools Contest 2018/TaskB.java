package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;

/**
 * CodeForces
 * April Fools Contest 2018
 * http://codeforces.com/contest/952/problems
 * B - A Map of the Cat
 * <p>
 * Status: Accepted
 * Note: Solved during the contest
 * Submission: http://codeforces.com/contest/952/submission/36853481
 * Niceness: 2/10
 * Difficulty: 3/10
 * Date: 2018-04-01
 * Author: Svilen Marchev
 * Tags: implementation, guessing
 * Analysis: ?
 * <p>
 * Just look for keywords in the responses that can disambiguate the cat type, and
 * answer *immediately* after you can say what the cat type is.
 */
public class TaskB {
  public void solve(int testNumber, InputReader in, PrintWriter out) {
    for (int i = 0; i <= 9; ++i) {
      out.println(i);
      out.flush();

      String resp = in.readLine(true);
      if (resp.contains("great")
          || resp.contains("think")
          || resp.contains("not bad")
          || resp.contains("cool")
          || resp.contains("touch")) {
        out.println("normal");
        out.flush();
        return;
      }

      if (resp.contains("terrible")
          || resp.contains("die")
          || resp.contains("even")
          || resp.contains("serious")
          || resp.contains("hole")
          || resp.contains("worse")
          || resp.contains("no way")) {
        out.println("grumpy");
        out.flush();
        return;
      }
    }
    throw new RuntimeException("no deterministic");
  }
}
