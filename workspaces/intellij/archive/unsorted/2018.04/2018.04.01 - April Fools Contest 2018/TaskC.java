package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * CodeForces
 * April Fools Contest 2018
 * http://codeforces.com/contest/952/problems
 * C - Ravioli Sort
 * <p>
 * Status: Accepted
 * Note: Solved during the contest
 * Submission: http://codeforces.com/contest/952/submission/36844666
 * Niceness: 2/10
 * Difficulty: 3/10
 * Date: 2018-04-01
 * Author: Svilen Marchev
 * Tags: implementation
 * Analysis: ?
 * <p>
 * Just check if at any step some of the ravioli gets changed. If so, output NO.
 */
public class TaskC {
  public void solve(int testNumber, InputReader in, PrintWriter out) {
    int n = in.readInt();
    List<Integer> list = new ArrayList<>();
    for (int i = 0; i < n; ++i) {
      list.add(in.readInt());
    }

    List<Integer> output = new ArrayList<>();
    for (int i = 0; i < n; ++i) {
      if (isInstable(list)) {
        out.println("NO");
        return;
      }
      int max = list.stream().max(Comparator.naturalOrder()).get();
      int ix = list.indexOf(max);
      output.add(max);
      list.remove(ix);
    }

    if (isInstable(output)) {
      out.println("NO");
      return;
    }
    out.println("YES");
  }

  boolean isInstable(List<Integer> list) {
    for (int i = 1; i < list.size(); ++i) {
      if (Math.abs(list.get(i - 1) - list.get(i)) >= 2) {
        return true;
      }
    }
    return false;
  }
}
