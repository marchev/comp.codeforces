package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;

/**
 * CodeForces
 * April Fools Contest 2018
 * http://codeforces.com/contest/952/problems
 * D - I'm Feeling Lucky!
 * <p>
 * Status: Accepted
 * Note: Solved during the contest
 * Submission: http://codeforces.com/contest/952/submission/36846258
 * Niceness: 2/10
 * Difficulty: 4/10
 * Date: 2018-04-01
 * Author: Svilen Marchev
 * Tags: guessing
 * Analysis: ?
 * <p>
 * Just check several answers - even, odd, etc.
 */
public class TaskD {
  public void solve(int testNumber, InputReader in, PrintWriter out) {
    out.println("odd");
  }
}
