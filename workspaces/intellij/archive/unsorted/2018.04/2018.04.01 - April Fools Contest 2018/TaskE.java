package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;

import java.io.PrintWriter;

/**
 * CodeForces
 * April Fools Contest 2018
 * http://codeforces.com/contest/952/problems
 * E - Cheese Board
 * <p>
 * Status: Accepted
 * Note: Solved during the contest
 * Submission: http://codeforces.com/contest/952/submission/36860365
 * Niceness: 4/10
 * Difficulty: 7/10
 * Date: 2018-04-01
 * Author: Svilen Marchev
 * Tags: implementation, reverse engineering, guessing
 * Analysis: ?
 * <p>
 * Observe that the outputted number is the size of the smallest square board that can fit the given
 * cheeses, assuming they are distributed in a "chessboard"-style (alternating soft and hard,
 * like the way light and dark are alternated on a chess board) and there is no requirement all
 * positions to be filled.
 */
public class TaskE {
  public void solve(int testNumber, InputReader in, PrintWriter out) {
    int n = in.readInt();
    int numHard = 0;
    int numSoft = 0;
    for (int i = 0; i < n; ++i) {
      in.readString();
      String type = in.readString();
      numHard += type.equals("hard") ? 1 : 0;
      numSoft += type.equals("hard") ? 0 : 1;
    }

    int numLess = Math.min(numHard, numSoft);
    int numMore = Math.max(numHard, numSoft);
    for (int size = 1; size <= 15; ++size) {
      int numAll = size * size;
      if (numLess <= numAll / 2 && numMore <= (numAll + 1) / 2) {
        out.println(size);
        return;
      }
    }
  }
}
