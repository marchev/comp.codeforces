package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;
import java.io.PrintWriter;

// Codeforces Round #518 (Div. 2) [Thanks, Mail.Ru!] - Task A
// Accepted
// 2018-11-19
// Tags: math
// Analysis: https://codeforces.com/blog/entry/62688
public class TaskA {
  public void solve(int testNumber, InputReader in, PrintWriter out) {
    long n = in.readLong();
    long m = in.readLong();
    long k = in.readLong();
    long l = in.readLong();

    long lower = (k+l + m-1)/m;
    long upper = n/m;
    if (lower > upper) {
      out.println(-1);
    } else {
      out.println(lower);
    }
  }
}
