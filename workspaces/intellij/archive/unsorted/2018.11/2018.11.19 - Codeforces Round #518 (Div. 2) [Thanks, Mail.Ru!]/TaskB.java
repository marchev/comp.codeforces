package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;
import java.io.PrintWriter;

// Codeforces Round #518 (Div. 2) [Thanks, Mail.Ru!] - Task B
// Accepted
// 2018-11-19
// Tags: math, lcm, gcd, prime factors
// Analysis: https://codeforces.com/blog/entry/62688
public class TaskB {
  public void solve(int testNumber, InputReader in, PrintWriter out) {
    long b = in.readLong();
    long ans = 1;

    for (long p = 2; p * p <= b; ++p) {
      int deg = 0;
      for (; b % p == 0; b /= p) {
        deg++;
      }
      ans *= deg + 1;
    }

    if (b != 1) {
      ans *= 2;
    }

    out.println(ans);
  }
}
