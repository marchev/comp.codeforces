package bg.svilen.codeforces;

import java.io.PrintWriter;
import net.egork.generated.collections.list.IntArrayList;
import net.egork.generated.collections.list.IntList;
import net.egork.utils.io.InputReader;

// Codeforces Round #518 (Div. 2) [Thanks, Mail.Ru!] - Task C
// Accepted
// 2019-03-04
// Tags: graphs, constructive
// Niceness: 7/10
// Analysis: https://codeforces.com/blog/entry/62688
//
// Analysis (Svilen Marchev):
// Place all rooks of each color on its own row. The rooks for each color consist of:
//  - a sequence of consecutive rooks. Their number is >=1 and equals the number of colors that will
// be connected to this color later. Place it to the right of all rooks already placed.
//  - a few connecting rooks with rooks of previous colors. They are positioned on the same
// row as the "sequence of consecutive rooks" above, but their x-coords are determined by
// the color they connect the current group with. Each y-coordinate should used by at most one
// connecting pair, so that we can more easily control which groups are connected to each other.
public class TaskC {
  public void solve(int testNumber, InputReader in, PrintWriter out) {
    int n = in.readInt();
    int m = in.readInt();

    IntList[] colorToConnectedColors = new IntList[n];
    for (int i = 0; i < n; ++i) {
      colorToConnectedColors[i] = new IntArrayList();
    }
    for (int i = 0; i < m; ++i) {
      int a = in.readInt();
      int b = in.readInt();
      colorToConnectedColors[a - 1].add(b - 1);
      colorToConnectedColors[b - 1].add(a - 1);
    }

    int[] colorToFirstUnusedX = new int[n];
    int[] colorToConseqRooks = new int[n];
    int curX = 1;
    for (int i = 0; i < n; ++i) {
      colorToFirstUnusedX[i] = curX;
      colorToConseqRooks[i] = Math.max(1, colorToConnectedColors[i].size());
      curX += colorToConseqRooks[i];
    }

    for (int i = 0; i < n; ++i) {
      final int c = i;

      int numRooks = colorToConseqRooks[c] + colorToConnectedColors[c].count(a -> a < c);
      out.println(numRooks);

      for (int j = 0; j < colorToConseqRooks[c]; ++j) {
        out.println(String.format("%d %d", colorToFirstUnusedX[c] + j, 1 + c));
      }
      for (int otherC : colorToConnectedColors[c].filter(a -> a < c)) {
        out.println(String.format("%d %d", colorToFirstUnusedX[otherC], 1 + c));
        colorToFirstUnusedX[otherC]++;
      }
    }
  }
}
