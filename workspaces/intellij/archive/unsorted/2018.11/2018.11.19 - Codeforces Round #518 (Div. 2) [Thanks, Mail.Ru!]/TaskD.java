package bg.svilen.codeforces;

import java.io.PrintWriter;
import net.egork.utils.io.InputReader;

// Codeforces Round #518 (Div. 2) [Thanks, Mail.Ru!] - Task D
// Accepted
// 2019-03-16
// Tags: DP, sequences
// Niceness: 8/10
// Analysis: https://codeforces.com/blog/entry/62688
//
// Analysis (Svilen Marchev):
// ----------------------------------------------
// Define a function like this:
// f(i, last, bigger) = number of ways to reconstruct the sequence up to its i-th position,
// with `last` being the i-th element, and:
//    - (i-1)th element <  last, if bigger = 1
//    - (i-1)th element >= last, if bigger = 0
//
// Calculate f(i, last, bigger):
//    if a[i] != -1 and a[i] != last:
//      f(i, last, bigger) = 0
//    else if bigger == 1:
//      f(i, last, bigger) = sum { f(i-1, L, B) | L=1..last-1, B=0,1 }
//    else:
//      f(i, last, bigger) = sum { f(i-1, last, B) | B=0,1 }
//                         + sum { f(i-1, L, 0) | L=last+1..200 }
//
// Answer is given as:
//    ans = sum { f(n, last, 0) | last=1..200 }
//
// For the sums we use accumulative arrays to reduce the time complexity by MAXV.
//
// Time complexity: O(n * MAXV).
// Memory complexity: O(n + MAXV).
public class TaskD {

  static final int MOD = 998244353;
  static final int MAXV = 200;

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    int n = in.readInt();
    int[] a = new int[n + 1];
    for (int i = 1; i <= n; ++i) {
      a[i] = in.readInt();
    }

    int[][] prev = new int[MAXV + 1][2];
    int[][] next = new int[MAXV + 1][2];

    // Accumulative sums for `prev`.
    int[][] prevAcc = new int[MAXV + 1][2];

    if (a[1] == -1) {
      for (int last = 1; last <= MAXV; ++last) {
        prev[last][1] = 1;
      }
    } else {
      prev[a[1]][1] = 1;
    }

    for (int i = 2; i <= n; ++i) {
      for (int last = 1; last <= MAXV; ++last) {
        prevAcc[last][0] = (prevAcc[last - 1][0] + prev[last][0]) % MOD;
        prevAcc[last][1] = (prevAcc[last - 1][1] + prev[last][1]) % MOD;
      }

      for (int last = 1; last <= MAXV; ++last) {
        next[last][0] = 0;
        next[last][1] = 0;

        if (a[i] >= 1 && a[i] != last) {
          continue;
        }

        next[last][1] = (next[last][1] + prevAcc[last - 1][0]) % MOD;
        next[last][1] = (next[last][1] + prevAcc[last - 1][1]) % MOD;

        next[last][0] = (next[last][0] + prev[last][0]) % MOD;
        next[last][0] = (next[last][0] + prev[last][1]) % MOD;
        next[last][0] =
            (int) (((long) next[last][0] + prevAcc[MAXV][0] - prevAcc[last][0] + MOD) % MOD);
      }

      int[][] swap = prev;
      prev = next;
      next = swap;
    }

    int sum = 0;
    for (int last = 1; last <= MAXV; ++last) {
      sum = (sum + prev[last][0]) % MOD;
    }
    out.println(sum);
  }
}
