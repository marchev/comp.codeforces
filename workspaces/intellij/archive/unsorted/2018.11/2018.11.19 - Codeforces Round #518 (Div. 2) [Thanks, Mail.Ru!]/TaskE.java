package bg.svilen.codeforces;

import java.io.PrintWriter;
import java.util.Arrays;
import net.egork.generated.collections.queue.IntArrayQueue;
import net.egork.generated.collections.queue.IntQueue;
import net.egork.graph.Graph;
import net.egork.utils.io.InputReader;

// Codeforces Round #518 (Div. 2) [Thanks, Mail.Ru!] - Task E
// Accepted
// 2019-03-17
// Tags: graph, bfs, tree
// Niceness: 8/10
// Analysis: https://codeforces.com/blog/entry/62688
//
// Analysis (Svilen Marchev):
// ----------------------------------------------
// Build a non-directed graph G.
// Add all nodes of 0 degree to a queue and mark that they are at level 0.
// While the queue is not empty:
//   take the first node v from the queue.
//   mark v as processed
//   if the node is a non-leaf and it's degree is < 3, G is not a hedgehog.
//   for each neighbor u of v:
//     if u is not already processed:
//       increase degree of u by 1
//       if level[u] is already set and differs from level[v]+1:
//         G is not a hedgehog
//       if level[u] is not already set:
//          level[u] = level[v]+1
//          add u to the queue
//
// maxLev = max(level[v] | all v)
// if level.count(maxLev) > 1: G is not a hedgehog
// G is a k-hedgehog, if maxLev = k
//
// Time complexity: O(n).
// Memory complexity: O(n).
public class TaskE {
  int n, k;
  Graph g;

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    n = in.readInt();
    k = in.readInt();
    g = new Graph(n, 2 * n);
    for (int i = 1; i < n; ++i) {
      int a = in.readInt() - 1;
      int b = in.readInt() - 1;
      g.addSimpleEdge(a, b);
      g.addSimpleEdge(b, a);
    }

    out.println(computeHedgeDegree() == k ? "Yes" : "No");
  }

  int computeHedgeDegree() {
    IntQueue queue = new IntArrayQueue(n);
    int[] addedAtLevel = new int[n];
    Arrays.fill(addedAtLevel, -1);
    for (int v = 0; v < n; ++v) {
      int degree = 0;
      for (int edge = g.firstOutbound(v); edge != -1; edge = g.nextOutbound(edge)) {
        degree++;
      }
      if (degree <= 1) {
        queue.add(v);
        addedAtLevel[v] = 0;
      }
    }

    int[] numKids = new int[n];
    boolean[] processed = new boolean[n];

    while (!queue.isEmpty()) {
      int v = queue.poll();
      processed[v] = true;

      if (addedAtLevel[v] > 0 && numKids[v] < 3) {
        return -1;
      }

      for (int edge = g.firstOutbound(v); edge != -1; edge = g.nextOutbound(edge)) {
        int u = g.destination(edge);
        if (!processed[u]) {
          numKids[u]++;
          if (addedAtLevel[u] != -1 && addedAtLevel[u] != addedAtLevel[v] + 1) {
            return -1;
          }
          if (addedAtLevel[u] == -1) {
            addedAtLevel[u] = addedAtLevel[v] + 1;
            queue.add(u);
          }
        }
      }
    }

    int max = Arrays.stream(addedAtLevel).max().getAsInt();
    long numMaxes = Arrays.stream(addedAtLevel).filter(a -> a == max).count();
    return numMaxes == 1 ? max : -1;
  }
}
