package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;
import java.io.PrintWriter;

// Codeforces Round #518 (Div. 2) [Thanks, Mail.Ru!] - Task F
// Accepted
// 2019-03-17
// Tags: reconstructive, grid
// Niceness: 5/10
// Analysis: https://codeforces.com/blog/entry/62688

/*
Analysis (Svilen Marchev):
----------------------------------------------
The formation that we'll use is a straight line of knights at positions (x,0), for x=0..k-1,
and a "dotted" line of knights above them at add x positions (x,3), for x=1,..k-1, x is odd.
this formation fills in most of the 2 lines between those lines of knights. Now some calculations:

Let k is the number of knights on the straight line (the at the y=0 line). We put initially
n = k+k/2 = 3k/2 knights. Those n knights "generate" m knights:

m >= 2 ((k-6) + (k-10) + (k-14) + ... + 0)
  >= 2 (4 + 8 + ... + (k-6))
   = 8 (1 + 2 + 3 + ... + (k-6)/4)
   = 8 (1 + (k-6)/4) (k-6)/8
   = 1/4 (k-2) (k-6)
   = O(1/4 k^2)

By definition, n=3k/2 => k = 2n/3. We replace k in the formula:

m >= O(1/4 4n^2 1/9) = O(1/9 n^2).

In other words, if we use the formation above, for every n knights we put initially, we will
end up with O(1/9 n^2) knights in total on the board. Since this is bigger than the requirement
of (1/20 n^2), this is a solution to the problem.

Time complexity: O(n).
Memory complexity: O(1).
*/
public class TaskF {
  public void solve(int testNumber, InputReader in, PrintWriter out) {
    int n = in.readInt();
    int num = 0;
    for (int x = 0; x < n; ++x) {
      out.println(x + " " + 0);
      if (++num >= n) {
        break;
      }
      if (x % 2 == 1) {
        out.println(x + " " + 3);
        if (++num >= n) {
          break;
        }
      }
    }
  }
}
