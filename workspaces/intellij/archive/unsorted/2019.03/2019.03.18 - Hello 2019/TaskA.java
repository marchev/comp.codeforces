package bg.svilen.codeforces;

import net.egork.utils.io.InputReader;
import java.io.PrintWriter;

// Codeforces: Hello 2019 - Task A
// Accepted
// 2019-03-18
// Tags: simulation
// Niceness: 4/10
// Difficulty: 1/10
// Analysis: https://codeforces.com/blog/entry/64310

public class TaskA {
  public void solve(int testNumber, InputReader in, PrintWriter out) {
    String mine = in.readString();
    String all =
        in.readString() + in.readString() + in.readString() + in.readString() + in.readString();
    out.println(
        all.contains(mine.substring(0, 1)) || all.contains(mine.substring(1)) ? "YES" : "NO");
  }
}
