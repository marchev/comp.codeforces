package bg.svilen.codeforces;

import net.egork.io.IOUtils;
import net.egork.utils.io.InputReader;
import java.io.PrintWriter;

// Codeforces: Hello 2019 - Task B
// Accepted
// 2019-03-18
// Tags: recursion
// Niceness: 4/10
// Difficulty: 1/10
// Analysis: https://codeforces.com/blog/entry/64310

public class TaskB {
  int n;
  int[] a;

  boolean canDo(int i, int sum) {
    if (i == n) {
      return sum % 360 == 0;
    }
    return canDo(i + 1, sum + a[i]) || canDo(i + 1, sum - a[i]);
  }

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    n = in.readInt();
    a = IOUtils.readIntArray(in, n);
    out.println(canDo(0, 0) ? "YES" : "NO");
  }
}
