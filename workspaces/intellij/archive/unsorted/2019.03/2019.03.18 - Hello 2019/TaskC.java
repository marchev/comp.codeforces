package bg.svilen.codeforces;

import net.egork.io.IOUtils;
import net.egork.utils.io.InputReader;
import java.io.PrintWriter;

// Codeforces: Hello 2019 - Task C
// Accepted
// 2019-03-18
// Tags: sequences, greedy
// Niceness: 8/10
// Difficulty: 4/10
// Analysis: https://codeforces.com/blog/entry/64310

public class TaskC {

  final int ML = 500000;

  int[] computeDeficiencyAndRedundancy(String parenthesis) {
    int deficiency = 0;
    int balance = 0;
    for (char c : parenthesis.toCharArray()) {
      if (c == ')') {
        if (balance == 0) {
          deficiency++;
        } else {
          balance--;
        }
      } else {
        balance++;
      }
    }
    return new int[]{deficiency, balance};
  }


  public void solve(int testNumber, InputReader in, PrintWriter out) {
    int n = in.readInt();
    String[] parentheses = IOUtils.readStringArray(in, n);

    int[] numWithDeficiency = new int[ML + 1];
    int[] numWithRedundancy = new int[ML + 1];
    int numNeutral = 0;

    for (String parenthesis : parentheses) {
      int[] defBal = computeDeficiencyAndRedundancy(parenthesis);
      int deficiency = defBal[0];
      int redundancy = defBal[1];
      if (deficiency == 0 && redundancy == 0) {
        numNeutral++;
      } else if (deficiency > 0 && redundancy == 0) {
        numWithDeficiency[deficiency]++;
      } else if (deficiency == 0 && redundancy > 0) {
        numWithRedundancy[redundancy]++;
      }
    }

    int numPairs = numNeutral / 2;
    for (int i = 1; i <= ML; ++i) {
      numPairs += Math.min(numWithDeficiency[i], numWithRedundancy[i]);
    }
    out.println(numPairs);
  }
}
