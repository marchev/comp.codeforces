package bg.svilen.codeforces;

import java.io.PrintWriter;
import net.egork.numbers.IntegerUtils;
import net.egork.utils.io.InputReader;

// Codeforces: Hello 2019 - Task D
// Accepted
// 2019-03-18
// Tags: DP, probability, statistics, expectation, number theory, modulo arithmetic, fermat's little theorem, multiplicative inverse
// Niceness: 8/10
// Difficulty: 7/10
// Analysis: https://codeforces.com/blog/entry/64310

/*
Analysis (Svilen Marchev):
----------------------------------------------
Let's define a random variable X_n as the outcome of the process after k iterations, starting
from n. The problem asks for its expectation E[X_n].

Let's rewrite X_n using its factorization:
 E[X_n] = E[X_(p1^a1 p2^a2 p3^a3 ... pt^at)]

Now looking at the way the process works, we can see that the multiples of each prime factor
are getting reduced at each iteration of the process independently of each other. Using the property
of expectations that E[X Y] = E[X] E[Y] for independent random variables X and Y, we can
expand the formula above:

 E[X_n] = E[X_(p1^a1 p2^a2 p3^a3 ... pt^at)]
        = E[X_(p1^a1)] E[X_(p2^a2)] E[X_(p3^a3)] ... E[X_(pt^at)]

This means that we can find each of the expectations E[X_(pi^ai)] independently and then just
multiply them to get the answer.

For that, we first need to find the factorization of n. For each pair (pi, ai), where pi is a
prime factor of n and ai is its power, we need to calculate the expected outcome after k
iterations. For that, we use a DP:
 f(i, d) = probability that the outcome after i iterations is p^d.
For each pair (p, a) we compute it like this:
 f(0, a) = 1
 f(0, d) = 0, 0 <= d < a

 f(i, d) = sum { f(i-1, e)  1/(e+1) | e = d..a }
This can be simplified:
 f(i, d) = f(i, d+1) + f(i-1, d) 1/(d+1),  0 <= d < a
 f(i, a) = f(i-1, d) 1/(d+1)

All intermediate results can be stored in the P Q^-1 form. That would allow us to work with
integers, as opposed to rationals. For finding the multiplicative inverse of n modulo M, we can first
observe that the modulo is a prime, therefore we can use Fermat's Little theorem to find it:

n^(p-1) === 1 (mod p), p = prime
n n^(p-2) === 1 (mod p)
So to find the multiplicative inverse of n, we need to compute n^(MOD-2) under modulo MOD.

Time complexity: O(log(n) log(MOD) + sqrt(n) + k log(n)). First term is for finding the
multiplicative inverses. The max degree of a prime factor is around log(n). Second term is for
the factorization of n. Third term is for the DP.

Memory complexity: O(log(n)).
*/
public class TaskD {

  static final long MOD = 1_000_000_007;
  static final int MAX_DEGREE = 60;

  int k;
  long[] inverse;

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    long n = in.readLong();
    k = in.readInt();

    inverse = new long[MAX_DEGREE + 2];
    for (int d = 0; d <= MAX_DEGREE + 1; ++d) {
      inverse[d] = multiplicativeInverse(d);
    }

    long result = 1;

    for (long p = 2; p * p <= n; p++) {
      int degree = 0;
      for (; n % p == 0; n /= p) {
        degree++;
      }
      if (degree > 0) {
        result = (result * computeExpectation(p, degree)) % MOD;
      }
    }
    if (n > 1) {
      result = (result * computeExpectation(n, 1)) % MOD;
    }

    out.println(result);
  }

  long computeExpectation(long p, int degree) {
    long[] prev = new long[degree + 1];
    long[] next = new long[degree + 1];

    prev[degree] = 1;
    for (int i = 1; i <= k; ++i) {
      next[degree] = (prev[degree] * inverse[degree + 1]) % MOD;
      for (int d = degree - 1; d >= 0; d--) {
        next[d] = (next[d + 1] + prev[d] * inverse[d + 1]) % MOD;
      }

      long[] swap = prev;
      prev = next;
      next = swap;
    }

    long expectation = 0;
    long pd = 1;
    for (int d = 0; d <= degree; ++d) {
      expectation = (expectation + prev[d] * pd) % MOD;
      pd = (pd * p) % MOD;
    }

    return expectation;
  }

  long multiplicativeInverse(int n) {
    // Use the Little Fermat theorem.
    return IntegerUtils.power(n, MOD - 2, MOD);
  }
}
