package bg.svilen.codeforces;

import java.io.PrintWriter;
import net.egork.io.IOUtils;
import net.egork.utils.io.InputReader;

// Codeforces: April Fools Day Contest 2019 - Task A
// Accepted
// 2019-04-01
// Tags: implementation
// Niceness: 3/10
// Competition: https://codeforces.com/contest/1145
// Analysis: https://codeforces.com/blog/entry/66327
//
// Analysis: N/A
public class TaskA {

  int[] a;

  boolean isSorted(int i, int j) {
    boolean s = true;
    for (int k = i + 1; k <= j; ++k) {
      s &= (a[k - 1] <= a[k]);
    }
    return s;
  }

  int mx(int i, int j) {
    if (isSorted(i, j)) {
      return j - i + 1;
    }
    return Math.max(mx(i, i + (j - i + 1) / 2 - 1), mx(i + (j - i + 1) / 2, j));
  }

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    int n = in.readInt();
    a = IOUtils.readIntArray(in, n);
    out.println(mx(0, n - 1));
  }
}
