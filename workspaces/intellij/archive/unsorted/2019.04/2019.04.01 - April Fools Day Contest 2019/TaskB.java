package bg.svilen.codeforces;

import java.io.PrintWriter;
import net.egork.utils.io.InputReader;

// Codeforces: April Fools Day Contest 2019 - Task B
// Accepted
// 2019-04-15
// Tags: guessing
// Niceness: 2/10
// Competition: https://codeforces.com/contest/1145
// Analysis: https://codeforces.com/blog/entry/66327
//
// Analysis:
// YES or NO answer implies that you need to figure out whether the given number is a kanban number; but first you need to figure out what kind of a number is kanban? OEIS does not have this sequence, but it has several similar ones, such as urban numbers and turban numbers. These sequences are defined as numbers which "ban" certain letters, i.e., don't use them in their English spelling. Thus, kanban numbers "ban" letters "k", "a" and "n"; in practice the numbers from 1 to 99 never contain letters "k" or "a" so you only need to watch for "n".
public class TaskB {

  boolean hasNInItsEnglishName(int n) {
    if (n < 10) {
      return n != 1 && n != 7 && n != 9;
    } else if (n < 20) {
      return n == 12;
    } else {
      if (n / 10 == 2 || n / 10 == 7 || n / 10 == 9) {
        return false;
      }
      if (n % 10 == 1 || n % 10 == 7 || n % 10 == 9) {
        return false;
      }
      return true;
    }
  }

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    int n = in.readInt();
    out.println(hasNInItsEnglishName(n) ? "YES" : "NO");
  }
}
