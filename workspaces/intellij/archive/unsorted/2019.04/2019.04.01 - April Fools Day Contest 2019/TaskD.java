package bg.svilen.codeforces;

import java.io.PrintWriter;
import net.egork.io.IOUtils;
import net.egork.misc.ArrayUtils;
import net.egork.utils.io.InputReader;

// Codeforces: April Fools Day Contest 2019 - Task D
// Accepted
// 2019-04-15
// Tags: guessing
// Niceness: 2/10
// Competition: https://codeforces.com/contest/1145
// Analysis: https://codeforces.com/blog/entry/66327
//
// Analysis:
// Pigeon d'Or is an actually existing project (well, an existing project concept). But for the purposes of this problem you don't need this knowledge, I just needed a sufficiently long text to hide the real statement. Но для целей этой задачи это знание излишне, мне просто нужен был достаточно длинный текст, в котором можно было бы спрятать настоящее условие. "We did not proofread this statement at all" hints that the typos are not accidental, and you need to pay attention to them. All typos replace correct letters with incorrect ones; if you collect all incorrect letters they will spell the real statement: "two plus xor of third and min elements".
public class TaskD {
  public void solve(int testNumber, InputReader in, PrintWriter out) {
    int n = in.readInt();
    int[] a = IOUtils.readIntArray(in, n);
    out.println(2 + (a[2] ^ ArrayUtils.minElement(a)));
  }
}
