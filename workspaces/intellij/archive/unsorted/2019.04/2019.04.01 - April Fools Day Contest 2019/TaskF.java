package bg.svilen.codeforces;

import java.io.PrintWriter;
import net.egork.utils.io.InputReader;

// Codeforces: April Fools Day Contest 2019 - Task F
// Accepted
// 2019-04-15
// Tags: guessing
// Niceness: 2/10
// Competition: https://codeforces.com/contest/1145
// Analysis: https://codeforces.com/blog/entry/66327
//
// Analysis:
// Again, YES or NO answer implies that you need to figure out whether the given word is neat. It can be tricky, especially since WORD itself is not neat... Long story short, in this problem you had to group all letters of English alphabet in two types — "linear", consisting only of straight lines, and "curved", including curved elements (you had to take the letters the way they are spelled in the statement, i.e., uppercase). Now, obviously, neat words are words in which all letters belong to the same type, and their opposite (disorderly?) are words which mix letters from both types.
public class TaskF {
  public void solve(int testNumber, InputReader in, PrintWriter out) {
    String s = in.readString();
    // Alphabet: "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    // All letters with some curved element
    String straightLetters = "AEFHIKLMNTVWXYZ";

    int numStraight = 0;
    for (char c : s.toCharArray()) {
      if (straightLetters.contains("" + c)) {
        numStraight++;
      }
    }
    out.println(numStraight == 0 || numStraight == s.length() ? "YES" : "NO");
  }
}
