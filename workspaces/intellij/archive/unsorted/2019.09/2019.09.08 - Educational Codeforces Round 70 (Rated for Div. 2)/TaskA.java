package bg.svilen.codeforces;

import java.io.PrintWriter;
import net.egork.utils.io.InputReader;

// Educational Codeforces Round 70 (Rated for Div. 2), Task A
// https://codeforces.com/contest/1202
// Status: Accepted
// Date: 2019-09-08
// Tags: greedy, math
// Niceness: 5/10
// Tutorial: https://codeforces.com/blog/entry/68972
//
// Analysis (Svilen Marchev):
// Just find the offset between the index k of the last '1' in y and the last '1' in x *before*
// or at index k. This way, we will cancel out the most significant '1' that we can in the
// f(x)+f(y)*2^k expression.

public class TaskA {
  int solve(String x, String y) {
    int i = x.length() - 1;
    int j = y.length() - 1;
    while (y.charAt(j) != '1') {
      j--;
      i--;
    }
    int k = 0;
    while (x.charAt(i) != '1') {
      i--;
      k++;
    }
    return k;
  }

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    int t = in.readInt();
    for (int tt = 0; tt < t; ++tt) {
      String x = in.readString();
      String y = in.readString();
      out.println(solve(x, y));
    }
  }
}
