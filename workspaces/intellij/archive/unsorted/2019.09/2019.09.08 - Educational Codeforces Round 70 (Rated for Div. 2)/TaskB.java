package bg.svilen.codeforces;

import java.io.PrintWriter;
import java.util.Arrays;
import net.egork.utils.io.InputReader;

// Educational Codeforces Round 70 (Rated for Div. 2), Task B
// https://codeforces.com/contest/1202
// Status: Accepted
// Date: 2019-09-08
// Tags: greedy, dp
// Niceness: 6/10
// Tutorial: https://codeforces.com/blog/entry/68972
//
// Analysis (Svilen Marchev):
// For each x and y, compute the min number of steps you need to reach from last digit A to
// another digit B. Assuming this is preprocessed, we could easily find the min number of numbers
// that need to be inserted into the given sequence, so that it's the output of a given x-y-counter.

public class TaskB {

  static final int INF = 1_000_000_000;

  // [x,y,cur sum's last digit, target digit]
  int[][][][] numStepsToTarget = new int[10][10][10][10];

  void preprocess() {
    for (int x = 0; x < 10; ++x) {
      for (int y = 0; y < 10; ++y) {
        for (int sumLastDigit = 0; sumLastDigit < 10; ++sumLastDigit) {
          Arrays.fill(numStepsToTarget[x][y][sumLastDigit], INF);

          for (int numStepsWithX = 0; numStepsWithX < 10; ++numStepsWithX) {
            for (int numStepsWithY = numStepsWithX > 0 ? 0 : 1; numStepsWithY < 10;
                ++numStepsWithY) {
              int targetDigit = (sumLastDigit + x * numStepsWithX + y * numStepsWithY) % 10;
              int totalSteps = numStepsWithX + numStepsWithY;
              numStepsToTarget[x][y][sumLastDigit][targetDigit] = Math
                  .min(numStepsToTarget[x][y][sumLastDigit][targetDigit], totalSteps);
            }
          }
        }
      }
    }
  }

  int solveForCounter(String s, int x, int y) {
    int curSumLastDigit = 0;
    int numSteps = 0;
    for (int i = 1; i < s.length(); ++i) {
      int targetDigit = s.charAt(i) - '0';
      int numStepsToReachTargetDigit = numStepsToTarget[x][y][curSumLastDigit][targetDigit];
      if (numStepsToReachTargetDigit >= INF) {
        return -1;
      }
      numSteps += numStepsToReachTargetDigit;
      curSumLastDigit = targetDigit;
    }
    // Account for all existing digits in the sequence, as well as the leading "0".
    return numSteps - s.length() + 1;
  }

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    preprocess();

    String s = in.readString();

    for (int x = 0; x < 10; ++x) {
      for (int y = 0; y < 10; ++y) {
        out.print(solveForCounter(s, x, y));
        out.print(" ");
      }
      out.println();
    }
  }
}
