package bg.svilen.codeforces;

import java.io.PrintWriter;
import net.egork.utils.io.InputReader;

// Educational Codeforces Round 70 (Rated for Div. 2), Task C
// https://codeforces.com/contest/1202
// Status: Accepted
// Date: 2019-09-08
// Tags: reconstruction, constraints fulfilment, simulation
// Niceness: 6/10
// Tutorial: https://codeforces.com/blog/entry/68972
//
// Analysis (Svilen Marchev):
// Assume that we start at (0,0) and compute the bounding box of the drawing procedure without
// modifying it. It's clear that the best we could do by inserting one more instruction is to
// cut down either the first or last row, or the first or last column. So what we do is: we
// compute all 4 "reduced" boundary boxes, and for each of them we run the drawing, and in the
// moment we go out of it, we immediately add one more "contra instruction" to offset the last
// instruction that has taken us out of the constraining bounding box. Of course, we could add
// such a contra instruction at most once. And we still track the boundary box that would fit
// the drawing, as it might well happen that the corrected sequence goes out of the restricted
// boundary box.

public class TaskC {

  static class Point {
    final int r, c;

    Point(int r, int c) {
      this.r = r;
      this.c = c;
    }

    @Override
    public String toString() {
      return String.format("(%d,%d)", r, c);
    }
  }

  static class Box {
    final Point tl, br;

    Box(Point tl, Point br) {
      this.tl = tl;
      this.br = br;
    }

    long area() {
      return ((long) br.r - tl.r + 1) * (br.c - tl.c + 1);
    }

    boolean contains(Point point) {
      return tl.r <= point.r && point.r <= br.r &&
          tl.c <= point.c && point.c <= br.c;
    }

    Box copyUpdate(Point point) {
      return new Box(
          new Point(Math.min(tl.r, point.r), Math.min(tl.c, point.c)),
          new Point(Math.max(br.r, point.r), Math.max(br.c, point.c)));
    }

    @Override
    public String toString() {
      return String.format("%s-%s", tl, br);
    }
  }

  static final int[][] OFFSETS = new int[][]{
      // Offsets for row, column. (0,0) is the top left corner.
      new int[]{-1, 0}, // W
      new int[]{+1, 0}, // S
      new int[]{0, -1}, // A
      new int[]{0, +1}, // D
  };

  int[] offsetForMove(char c) {
    switch (c) {
      case 'W':
        return OFFSETS[0];
      case 'S':
        return OFFSETS[1];
      case 'A':
        return OFFSETS[2];
      case 'D':
        return OFFSETS[3];
      default:
        throw new AssertionError();
    }
  }

  char reversePathMove(char c) {
    switch (c) {
      case 'W':
        return 'S';
      case 'S':
        return 'W';
      case 'A':
        return 'D';
      case 'D':
        return 'A';
      default:
        throw new AssertionError();
    }
  }

  long adaptToReduceArea(Box reducedBox, Point startingPoint, String path) {
    if (!reducedBox.contains(startingPoint)) {
      return Long.MAX_VALUE;
    }
    Point point = startingPoint;
    Box box = new Box(point, point);

    boolean hasCorrected = false;
    for (int i = 0; i < path.length(); ++i) {
      int[] off = offsetForMove(path.charAt(i));
      Point newPoint = new Point(point.r + off[0], point.c + off[1]);
      if (!hasCorrected && !reducedBox.contains(newPoint)) {
        hasCorrected = true;
        // Push an immediate contra-move of this move, so that we stay in the reduced box.
        // We do this simply by ignoring this move, but we need to update the box to
        // account for the extra move.
        int[] contraOff = offsetForMove(reversePathMove(path.charAt(i)));
        Point contraPoint = new Point(point.r + contraOff[0], point.c + contraOff[1]);
        box = box.copyUpdate(contraPoint);
      } else {
        point = newPoint;
        box = box.copyUpdate(point);
      }
    }

    return box.area();
  }

  long calcMinReducedArea(String path) {
    Point startingPoint = new Point(0, 0);

    Point point = startingPoint;
    Box box = new Box(point, point);
    for (int i = 0; i < path.length(); ++i) {
      int[] off = offsetForMove(path.charAt(i));
      point = new Point(point.r + off[0], point.c + off[1]);
      box = box.copyUpdate(point);
    }
//    System.out.println(box);
//    System.out.println(box.area());

    long minArea = box.area();

    Box withoutFirstCol = new Box(new Point(box.tl.r, box.tl.c + 1), box.br);
    minArea = Math.min(minArea, adaptToReduceArea(withoutFirstCol, startingPoint, path));

    Box withoutFirstRow = new Box(new Point(box.tl.r + 1, box.tl.c), box.br);
    minArea = Math.min(minArea, adaptToReduceArea(withoutFirstRow, startingPoint, path));

    Box withoutLastRow = new Box(box.tl, new Point(box.br.r - 1, box.br.c));
    minArea = Math.min(minArea, adaptToReduceArea(withoutLastRow, startingPoint, path));

    Box withoutLastCol = new Box(box.tl, new Point(box.br.r, box.br.c - 1));
    minArea = Math.min(minArea, adaptToReduceArea(withoutLastCol, startingPoint, path));

    return minArea;
  }

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    int tests = in.readInt();
    for (int test = 0; test < tests; ++test) {
      String path = in.readString();
      out.println(calcMinReducedArea(path));
    }
  }
}
