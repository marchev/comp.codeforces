package bg.svilen.codeforces;

import java.io.PrintWriter;
import net.egork.generated.collections.list.IntArrayList;
import net.egork.generated.collections.list.IntList;
import net.egork.utils.io.InputReader;

// Educational Codeforces Round 70 (Rated for Div. 2), Task D
// https://codeforces.com/contest/1202
// Status: Accepted
// Date: 2019-09-08
// Tags: reconstruction, math, recurrence, sequence
// Niceness: 8/10
// Tutorial: https://codeforces.com/blog/entry/68972
// Submission: https://codeforces.com/contest/1202/submission/60271072
//
// Analysis (Svilen Marchev):
// -----------------------
// Assume the sequence has the form:
// 1 333... 777... 333... 777...
// i.e. a single 1, followed by multiple groups of 3s and 7s.
// Now we can use a recurrent function to compute the number of 3s and 7s in each group,
// so that there are exactly n subsequences of "1337".
//
// Official analysis:
// -----------------------
// Let's consider the following string 1333…3337. If digit 3 occurs x times in it, then string
// have x(x−1)/2 subsequences 1337.
//
// Let's increase the number of digits 3 in this string while condition x(x−1)/22≤n holds (x is the
// number of digits 3 in this string). The length of this string will not exceed 45000 because
// 45000(45000−1)/2>10^9. The value rem=n−x(x−1)/2 will not exceed 45000 as well.
//
// All we have to do is increase the number of subsequences 1337 in the current string by rem.
// So if we add rem digits 7 after the first two digits 3 we increase the number of subsequences
// 1337 by rem. The string s will look like this: 13377…7733…337, where sequence 77…77 consists
// of exactly rem digits 7 and sequence 33…33 consists of exactly x−2 digits 3.

public class TaskD {
  int solve(int n, IntList threes, IntList sevens) {
    if (n == 0) {
      return 0;
    }
    int totalThrees = ((int) Math.sqrt(4L * n + 1) + 1) / 2;
    int numSequences = totalThrees * (totalThrees - 1);
    int numSevensInCurGroup = 0;
    while (n >= numSequences) {
      n -= numSequences;
      numSevensInCurGroup++;
    }
    int numThreesBefore = solve(n, threes, sevens);
    int numThreesInCurGroup = totalThrees - numThreesBefore;
//    System.out.println("! " + totalThrees + " times=" + numSevensInCurGroup);
    threes.add(numThreesInCurGroup);
    sevens.add(numSevensInCurGroup);
    return totalThrees;
  }

  void printAnswer(PrintWriter out, IntList threes, IntList sevens) {
    out.print("1");
    for (int i = 0; i < threes.size(); ++i) {
      for (int k = 0; k < threes.get(i); ++k) {
        out.print("3");
      }
      for (int k = 0; k < sevens.get(i); ++k) {
        out.print("7");
      }
    }
    out.println();
  }

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    int t = in.readInt();

    for (int tt = 0; tt < t; ++tt) {
      int n = in.readInt();

      IntList threes = new IntArrayList();
      IntList sevens = new IntArrayList();

      // Multiple by 2, for convenience in the computations later.
      solve(2 * n, threes, sevens);

      printAnswer(out, threes, sevens);
    }
  }
}
