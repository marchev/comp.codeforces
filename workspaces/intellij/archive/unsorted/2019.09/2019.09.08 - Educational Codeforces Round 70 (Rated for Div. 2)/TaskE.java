package bg.svilen.codeforces;

import bg.svilen.comp.lib.string.SvTrie;
import bg.svilen.comp.lib.string.SvTrie.TrieNode;
import java.io.PrintWriter;
import java.util.ArrayList;
import net.egork.string.StringUtils;
import net.egork.utils.io.InputReader;

// Educational Codeforces Round 70 (Rated for Div. 2), Task E
// https://codeforces.com/contest/1202
// Status: Accepted
// Date: 2020-01-20
// Tags: strings, kmp, trie, prefix function, sqrt optimization, aho-corasick
// Niceness: 8/10
// Tutorial: https://codeforces.com/blog/entry/68972
// Submission: https://codeforces.com/contest/1202/submission/69203869
//
// Official analysis from tutorial:
// -----------------------
// Let's look at any occurrence of arbitrary pair si+sj. There is exactly one special split
// position, where the si ends and sj starts. So, instead of counting occurrences for each pair,
// we can iterate over the position of split and count the number of pairs. This transformation
// is convenient, since any si, which ends in split position can be paired with any sj
// which starts here.
//
// So, all we need is to calculate for each suffix the number of strings si, which starts here,
// and for each prefix — the number of strings si, which ends here. But calculating the prefixes
// can be transformed to calculating suffixes by reversing both t and all si.
//
// Now we need, for each position pos, calculate the number of strings si which occur from pos.
// It can be done by Aho-Corasick, Suffix Array, Suffix Automaton, Suffix Tree, but do we
// really need them since constrains are pretty low? The answer is NO. We can use sqrt-heuristic!
//
// Let's divide all si in two groups: short and long. The si is short if |si|≤MAG.
// There are no more than ∑|si|MAG long strings and, for each such string, we can find all
// its occurrences with z-function (or prefix-function). It will cost as O(∑|si|MAG⋅|t|+∑|si|).
//
// What to do with short strings? Let's add them to trie! The trie will have O(∑|si|) vertices,
// but only MAG depth. So we can, for each pos, move down through the trie, while counting
// the occurrences, using only s[pos..(pos+MAG)] substring. It will cost us O(|t|⋅MAG).
//
// So, if we choose MAG=∑|si|−−−−−√ we can acquire O(∑|si|+|t|∑|si|−−−−−√) complexity,
// using only basic string structures.

public class TaskE {
  String t;
  ArrayList<String> s = new ArrayList<>();

  int m;
  int[] numStartingAt;
  int[] numEndingAt;

  void matchWithKmp(String s) {
    int[] prefix = StringUtils.prefixFunction(s);
    int q = 0;
    for (int i = 0; i < t.length(); ++i) {
      while (q > 0 && t.charAt(i) != s.charAt(q)) {
        q = prefix[q - 1];
      }
      if (t.charAt(i) == s.charAt(q)) {
        ++q;
      }
      if (q == s.length()) {
        int startIx = i - s.length() + 1;
//        System.out.println("KMP: Match of " + s + " starting at " + startIx + " ending at " + i);
        q = prefix[q - 1];

        numStartingAt[startIx]++;
        numEndingAt[i]++;
      }
    }
  }

  public void solve(int testNumber, InputReader in, PrintWriter out) {
    t = in.readString();
    int n = in.readInt();
    int totalLen = 0;
    for (int i = 0; i < n; ++i) {
      String tmp = in.readString();
      s.add(tmp);
      totalLen += tmp.length();
    }

    numEndingAt = new int[t.length()];
    numStartingAt = new int[t.length()];
    m = (int) Math.sqrt(totalLen) + 1;

    // Part 1: for long si strings, s.t. |si|>=m
    //
    // Use KMP and the prefix function.
    for (int i = 0; i < n; ++i) {
      if (s.get(i).length() >= m) {
        matchWithKmp(s.get(i));
      }
    }

    // Part 2: for short si stings, s.t. |si|<m
    //
    // Use a trie to add all small strings, and then for each position of t, walk down the trie
    // and update numStartingAt and numEndingAt whenever some of the trie nodes are end of some
    // strings from si.
    SvTrie trie = new SvTrie();
    for (int i = 0; i < n; ++i) {
      if (s.get(i).length() < m) {
        trie.add(s.get(i));
      }
    }
    for (int i = 0; i < t.length(); ++i) {
      TrieNode cur = trie.getRoot();
      for (int q = 0; q < m - 1 && i + q < t.length(); ++q) {
        cur = cur.getLink(t.charAt(i + q));
        if (cur == null) {
          break;
        }

//        System.out.println("Trie: Match starting at " + i + " ending at " + (i + q));
        numStartingAt[i] += cur.getTerminatingStringsCount();
        numEndingAt[i + q] += cur.getTerminatingStringsCount();
      }
    }

    long ans = 0;
    for (int i = 0; i < t.length() - 1; ++i) {
      ans += (long) numEndingAt[i] * numStartingAt[i + 1];
    }
    out.println(ans);
  }
}
