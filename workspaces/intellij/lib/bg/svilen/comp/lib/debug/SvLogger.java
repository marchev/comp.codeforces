package bg.svilen.comp.lib.debug;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Logger with additional performance info that can easily be enabled or disabled.
 */
public class SvLogger {

  private static final String MESSAGE_PREFIX = "> ";

  private boolean enabled = true;
  private boolean printTimeEllapsed = false;
  private long startedTime = System.currentTimeMillis();

  public SvLogger setEnabled(boolean enabled) {
    this.enabled = enabled;
    return this;
  }

  public SvLogger setPrintTimeEllapsed(boolean printTimeEllapsed) {
    this.printTimeEllapsed = printTimeEllapsed;
    return this;
  }

  public void p(Object... args) {
    if (!enabled) {
      return;
    }
    printMessage(Arrays.asList(args).stream()
        .map(Object::toString)
        .collect(Collectors.joining(" ")));
  }

  public void p(String format, Object... args) {
    if (!enabled) {
      return;
    }
    printMessage(String.format(format, args));
  }

  private void printMessage(String msg) {
    String formattedTime =
        printTimeEllapsed ? " (" + (System.currentTimeMillis() - startedTime) + " ms)" : "";
    System.out.println(MESSAGE_PREFIX + msg + formattedTime);
  }
}
