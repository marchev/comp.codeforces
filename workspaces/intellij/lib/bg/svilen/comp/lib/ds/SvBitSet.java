package bg.svilen.comp.lib.ds;

/**
 * Svilen's BitSet implementation.
 */
public class SvBitSet {

  int size;
  long[] words;

  public SvBitSet(int size) {
    this.size = size;
    this.words = new long[(size + 63) / 64];
  }

  public void set(int i, boolean value) {
    if (value) {
      set(i);
    } else {
      unset(i);
    }
  }

  public void set(int i) {
    words[i >>> 6] |= 1L << (i & 63);
  }

  public void unset(int i) {
    words[i >>> 6] &= ~(1L << (i & 63));
  }

  public boolean get(int i) {
    return (words[i >>> 6] & (1L << (i & 63))) != 0L;
  }

  public boolean isAnySet() {
    for (int i = 0; i < words.length; ++i) {
      if (words[i] != 0) {
        return true;
      }
    }
    return false;
  }

  public void shiftRight() {
    long bit0 = words[0] & 1L;
    words[0] >>>= 1;
    for (int i = 1; i < words.length; ++i) {
      long wordFirstBit = words[i] & 1L;
      words[i] >>>= 1;
      words[i - 1] |= wordFirstBit << 63;
    }
    if (bit0 != 0L) {
      set(size - 1);
    }
  }

  public SvBitSet applyAnd(SvBitSet other) {
    for (int i = 0; i < words.length; ++i) {
      words[i] &= other.words[i];
    }
    return this;
  }

  public SvBitSet applyOr(SvBitSet other) {
    for (int i = 0; i < words.length; ++i) {
      words[i] |= other.words[i];
    }
    return this;
  }

  public SvBitSet copy() {
    SvBitSet newBS = new SvBitSet(0);
    newBS.words = words.clone();
    newBS.size = size;
    return newBS;
  }

//  public SvBitSet copyTo(SvBitSet target) {
//    for (int i = 0; i < words.length; ++i) {
//      target.words[i] = words[i];
//    }
//    target.size = size;
//    return target;
//  }

  public SvBitSet copyFrom(SvBitSet source) {
    for (int i = 0; i < words.length; ++i) {
      words[i] = source.words[i];
    }
    size = source.size;
    return this;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("");
    for (int i = 0; i < size; ++i) {
      sb.append(get(i) ? '1' : '0');
    }
    return sb.toString();
  }
}
