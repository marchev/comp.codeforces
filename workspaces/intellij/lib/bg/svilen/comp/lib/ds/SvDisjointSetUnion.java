package bg.svilen.comp.lib.ds;

import java.util.Arrays;

/**
 * Disjoint set union, aka union-find trees or independent set system. Recursive implementation.
 */
public class SvDisjointSetUnion {

  private int[] parent;
  private int[] size;
  private int numSets;

  public SvDisjointSetUnion(int numSets) {
    this.numSets = numSets;
    this.parent = new int[numSets];
    this.size = new int[numSets];
    Arrays.fill(parent, -1);
    Arrays.fill(size, 1);
  }

  /**
   * Returns the root of a set. O(log N) time complexity.
   *
   * @param i any element from the set
   */
  public int getRoot(int i) {
    int root = i;
    while (parent[root] != -1) {
      root = parent[root];
    }
    // Use the opportunity to flatten the tree.
    while (i != root) {
      int oldParent = parent[i];
      parent[i] = root;
      i = oldParent;
    }
    return root;
  }

  /**
   * Merges two sets. O(log N) time complexity.
   *
   * @param i any element from the first set
   * @param j any element from the second set
   * @return true if the two elements were from different sets and the sets were joined now
   */
  public boolean join(int i, int j) {
    int rooti = getRoot(i);
    int rootj = getRoot(j);
    if (rooti == rootj) {
      return false;
    }
    if (size[rooti] < size[rootj]) {
      int tmp = rooti;
      rooti = rootj;
      rootj = tmp;
    }
    parent[rootj] = rooti;
    size[rooti] += size[rootj];
    numSets -= 1;
    return true;
  }

  /**
   * Returns the size of a given set. Time complexity:
   * <li>O(log N) if the element is a random element from the set;</li>
   * <li>O(1) if it is the root of the set.</li>
   *
   * @param i any element from the set
   */
  public int getSetSize(int i) {
    int root = getRoot(i);
    return size[root];
  }

  /** Returns the number of disjoint sets in the union. O(1) time complexity */
  public int getNumSets() {
    return numSets;
  }
}
