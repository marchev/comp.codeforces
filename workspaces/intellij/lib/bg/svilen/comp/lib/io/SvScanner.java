package bg.svilen.comp.lib.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * Svilen's faster Scanner.
 */
public class SvScanner {

  private BufferedReader reader;
  private StringTokenizer tokenizer = new StringTokenizer("");

  public SvScanner(InputStreamReader in) {
    reader = new BufferedReader(in);
  }

  public String next() {
    try {
      while (!tokenizer.hasMoreTokens()) {
        String nextLine = reader.readLine();
        if (nextLine == null) {
          throw new IllegalStateException("next line is null");
        }
        tokenizer = new StringTokenizer(nextLine);
      }
      return tokenizer.nextToken();
    } catch (IOException e) {
      throw new IllegalStateException(e);
    }
  }

  public int nextInt() {
    return Integer.parseInt(next());
  }

  public long nextLong() {
    return Long.parseLong(next());
  }
}
