package bg.svilen.comp.lib.math;

import java.math.BigInteger;

/**
 * Math utils.
 */
// TODO: use directly from SMathLib.
public class SvCommonMathUtils {

  public static BigInteger sqrt(BigInteger n) {
    if (n.signum() < 0) {
      throw new ArithmeticException("root of negative num");
    }
    BigInteger root = BigInteger.ZERO;
    for (int i = n.bitLength() - 1; i >= 0; --i) {
      BigInteger add = BigInteger.ONE.shiftLeft(i);
      BigInteger newRoot = root.add(add);
      if (newRoot.pow(2).compareTo(n) <= 0) {
        root = newRoot;
      }
    }
    return root;
  }

  public static boolean isSquare(BigInteger n) {
    BigInteger sq = sqrt(n);
    return sq.multiply(sq).equals(n);
  }

  public static boolean isSquare(long n) {
    long sq = (long) Math.sqrt(n);
    return sq * sq == n;
  }

  public static int pow(int n, long deg, int mod) {
    if (n < 0 || deg < 0 || mod <= 0) {
      throw new IllegalArgumentException();
    }
    if (deg == 0) {
      return 1;
    } else if (deg % 2 == 0) {
      long tmp = pow(n, deg / 2, mod);
      return (int) ((tmp * tmp) % mod);
    } else {
      return (int) (((n % mod) * (long) pow(n, deg - 1, mod)) % mod);
    }
  }

  public static long pow(int n, int deg) {
    if (deg < 0) {
      throw new IllegalArgumentException();
    }
    if (deg == 0) {
      return 1;
    } else if (deg % 2 == 0) {
      long tmp = pow(n, deg / 2);
      return tmp * tmp;
    } else {
      return n * pow(n, deg - 1);
    }
  }

  // TODO: extend to work with negative nums
  public static long gcd(long a, long b) {
    if (a <= 0 || b <= 0) {
      throw new IllegalArgumentException();
    }
    while (b != 0) {
      long c = a % b;
      a = b;
      b = c;
    }
    return a;
  }

  // TODO: extend to work with negative nums
  public static int gcd(int a, int b) {
    if (a <= 0 || b <= 0) {
      throw new IllegalArgumentException();
    }
    while (b != 0) {
      int c = a % b;
      a = b;
      b = c;
    }
    return a;
  }
}
