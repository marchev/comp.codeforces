package bg.svilen.comp.lib.math;

import java.util.ArrayList;
import java.util.List;

/** Number theory utils. */
// TODO: Consolidate with the smathlib.
public class SvNumberTheoryUtils {

  public static List<Integer>[] genListsOfDivisorsForIntsUpTo(int n) {
    @SuppressWarnings("unchecked")
    ArrayList<Integer>[] ret = new ArrayList[n + 1];
    for (int i = 1; i <= n; ++i) {
      ret[i] = new ArrayList<Integer>();
    }
    for (int i = 1; i <= n; ++i) {
      for (int j = i; j <= n; j += i) {
        ret[j].add(i);
      }
    }
    return ret;
  }

  public static int[] genEulerPhiForIntsUpTo(int n) {
    if (n < 0) {
      throw new IllegalArgumentException();
    }
    int[] phi = new int[n + 1];
    for (int i = 0; i <= n; ++i) {
      phi[i] = i;
    }
    for (int i = 2; i <= n; ++i) {
      if (phi[i] == i) { // if i is prime
        for (int j = i; j <= n && j >= 0; j += i) {
          phi[j] = (phi[j] / i) * (i - 1);
        }
      }
    }
    return phi;
  }

  public static long calcEulerPhi(long n, int certainty) {
    long p = n;
    for (long i = 1; i * i <= n; ++i) {
      if (n % i == 0) {
        long j = n / i;
        if (SvPrimalityUtils.isPrime(i, certainty)) {
          p = (p / i) * (i - 1);
        }
        if (i != j && SvPrimalityUtils.isPrime(j, certainty)) {
          p = (p / j) * (j - 1);
        }
      }
    }
    return p;
  }

  /**
   * Calculates sigma_x(n) - the sum of positive divisors function. For a real or complex number x
   * it is defined as the sum of the x-th powers of the positive divisors of n.
   * <p>
   * sigma_x(n) = sum {d^x : for all d|n}
   * <p>
   * sigma_0(n) gives the number of divisors of n.
   * <p>
   * sigma_1(n) gives the sum of divisors of n.
   *
   * @param n
   * @param x
   * @return sigma_x(n)
   */
  public static long calcSigma(int n, int x) {
    if (x < 0) {
      throw new IllegalArgumentException();
    }
    if (n == 0) {
      return 0;
    }
    n = Math.abs(n);

    SvPrimalityUtils.IntPrimeFactor[] f = SvPrimalityUtils.calcPrimeFactorizationOf(n);

    // Uses the formula:
    // sigma_x(n) = prod{ 1 + p_i^x + p_i^(2x) + p_i^(3x) + ... + p_i^(a_i * x) :
    // for all prime divisor p_i of n, where a_i is the factor of p_i in n }
    long ret = 1;
    for (int i = 0; i < f.length; ++i) {
      long sum = 1;
      long cur = 1;
      long px = SvCommonMathUtils.pow(f[i].prime, x);
      for (int j = 1; j <= f[i].degree; ++j) {
        cur *= px;
        sum += cur;
      }
      ret *= sum;
    }

    return ret;
  }

  /**
   * Compute the Moebius function m(k) for all k=1,2,...,n. It is defined for all positive integers
   * n and has its values in {-1, 0, 1} depending on the factorization of n into prime factors. It
   * is defined as follows:
   * <p>
   * <li>m(n) = 1 if n is a square-free positive integer with an even number of prime factors.
   * <p>
   * <li>m(n) = -1 if n is a square-free positive integer with an odd number of prime factors.
   * <p>
   * <li>m(n) = 0 if n is not square-free.
   */
  public static byte[] genMoebiusFuncForIntsUpTo(int n) {
    byte[] m = new byte[n + 1];
    for (int i = 1; i <= n; ++i) {
      m[i] = 1;
    }
    boolean[] isPr = SvPrimalityUtils.genBoolArrayForPrimalityTestUpTo(n);
    for (int p = 2; p <= n; ++p) {
      if (isPr[p]) {
        long p2 = (long) p * p;
        for (long j = p2; j <= n; j += p2) {
          m[(int) j] = 0;
        }
        for (int j = p; j <= n && j > 0; j += p) {
          m[j] = (byte) -m[j];
        }
      }
    }
    return m;
  }

  /**
   * Generates the Moebius function for all values up to n, provided the lowest prime
   * factor of each number up to n. See {@link #genMoebiusFuncOfIntsUpTo(int, int[])}.
   */
  public static byte[] genMoebiusFuncOfIntsUpTo(int n, int[] lowestPrimeFactorOf) {
    byte[] moebius = new byte[n + 1];
    moebius[1] = 1;
    for (int i = 2; i <= n; i++) {
      int j = i / lowestPrimeFactorOf[i];
      if (lowestPrimeFactorOf[j] == lowestPrimeFactorOf[i]) {
        moebius[i] = 0;
      } else {
        moebius[i] = (byte) -moebius[j];
      }
    }
    return moebius;
  }

  /**
   * Computes the multiplicative inverse of a given number under a given modulo, i.e. a number t,
   * s.t. t a === 1 (mod m).
   * <p>
   * The algorithm is explained here: https://en.wikipedia.org/wiki/Extended_Euclidean_algorithm
   *
   * @param a   the number whose inverse we are looking for
   * @param mod the modulo
   * @return the multiplicative inverse t, 0 <= t < mod.
   * @throws IllegalArgumentException if multiplicative inverse does not exist
   */
  public static int multiplicativeInverse(int a, int mod) {
    int t = 0, newt = 1;
    int r = mod, newr = a;
    while (newr != 0) {
      int quotient = r / newr;

      int oldt = t;
      t = newt;
      newt = oldt - quotient * newt;

      int oldr = r;
      r = newr;
      newr = oldr - quotient * newr;
    }
    if (r > 1) {
      throw new IllegalArgumentException("a is not invertible");
    }
    if (t < 0) {
      t += mod;
    }
    return t;
  }
}
