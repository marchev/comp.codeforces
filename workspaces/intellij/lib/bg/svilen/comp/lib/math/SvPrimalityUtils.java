package bg.svilen.comp.lib.math;

import net.egork.generated.collections.list.IntArrayList;
import net.egork.generated.collections.list.IntList;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/** Utils around primes. */
// TODO: Consolidate with the smathlib.
public class SvPrimalityUtils {

  public static boolean[] genBoolArrayForPrimalityTestUpTo(int n) {
    if (n < 0) {
      throw new IllegalArgumentException();
    }
    boolean[] isPrime = new boolean[n + 1];
    Arrays.fill(isPrime, true);
    if (n >= 0) {
      isPrime[0] = false;
    }
    if (n >= 1) {
      isPrime[1] = false;
    }
    for (int i = 2; i <= n; ++i) {
      if (isPrime[i]) {
        for (long j = ((long) i) * i; j <= n; j += i) {
          isPrime[(int) j] = false;
        }
      }
    }
    return isPrime;
  }

  public static int[] genArrayOfPrimesUpTo(int n) {
    if (n <= 1) {
      return new int[0];
    }
    boolean[] isPr = genBoolArrayForPrimalityTestUpTo(n);
    int primesCnt = 0;
    for (int i = 2; i <= n; ++i) {
      if (isPr[i]) {
        primesCnt += 1;
      }
    }
    int[] primes = new int[primesCnt];
    primesCnt = 0;
    for (int i = 2; i <= n; ++i) {
      if (isPr[i]) {
        primes[primesCnt++] = i;
      }
    }
    return primes;
  }

  public static IntList[] genIntListsOfPrimeDivisorsForIntsUpTo(int n) {
    IntList[] ret = new IntList[n + 1];
    for (int i = 0; i <= n; ++i) {
      ret[i] = new IntArrayList();
    }
    for (int i = 2; i <= n; ++i) {
      if (ret[i].isEmpty()) {
        for (int j = i; j <= n; j += i) {
          ret[j].add(i);
        }
      }
    }
    return ret;
  }

  /** Use {@link #genIntListsOfPrimeDivisorsForIntsUpTo(int)} instead. */
  @Deprecated
  public static List<Integer>[] genListsOfPrimeDivisorsForIntsUpTo(int n) {
    @SuppressWarnings("unchecked")
    ArrayList<Integer>[] ret = new ArrayList[n + 1];
    for (int i = 0; i <= n; ++i) {
      ret[i] = new ArrayList<Integer>();
    }
    for (int i = 2; i <= n; ++i) {
      if (ret[i].isEmpty()) {
        for (int j = i; j <= n; j += i) {
          ret[j].add(i);
        }
      }
    }
    return ret;
  }

  public static boolean isPrime(long n, int certainty) {
    if (n <= 10) {
      return (n == 2 || n == 3 || n == 5 || n == 7);
    } else if (n % 2 == 0 || n % 3 == 0 || n % 5 == 0 || n % 7 == 0) {
      return false;
    } else {
      return BigInteger.valueOf(n).isProbablePrime(certainty);
    }
  }

  /**
   * Calculates the prime factorization of a positive integer.
   *
   * @param n should be > 0
   * @return an array of {@link IntPrimeFactor}
   */
  public static IntPrimeFactor[] calcPrimeFactorizationOf(int n) {
    if (n < 0) {
      throw new IllegalArgumentException();
    }
    if (n == 0) {
      return new IntPrimeFactor[0];
    }
    ArrayList<IntPrimeFactor> factors = new ArrayList<>();

    if (n % 2 == 0) {
      n /= 2;
      int degree = 1;
      while (n % 2 == 0) {
        n /= 2;
        degree++;
      }
      factors.add(new IntPrimeFactor(2, degree));
    }

    int maxFactor = (int) Math.sqrt(n);
    for (int factor = 3; factor <= maxFactor && n > 1; factor += 2) {
      if (n % factor == 0) {
        n /= factor;
        int degree = 1;
        while (n % factor == 0) {
          n /= factor;
          degree++;
        }
        factors.add(new IntPrimeFactor(factor, degree));
        maxFactor = (int) Math.sqrt(n);
      }
    }

    if (n > 1) {
      factors.add(new IntPrimeFactor(n, 1));
    }

    return factors.toArray(new IntPrimeFactor[0]);
  }

  public static class IntPrimeFactor {
    /** Prime factor. */
    public int prime;
    public int degree;

    public IntPrimeFactor(int prime, int degree) {
      this.prime = prime;
      this.degree = degree;
    }
  }

  /**
   * Calculates the prime factorization of a positive integer.
   *
   * @param n should be > 0
   * @return an array of {@link LongPrimeFactor}-s
   */
  public static LongPrimeFactor[] calcPrimeFactorizationOf(long n) {
    if (n < 0) {
      throw new IllegalArgumentException();
    }
    if (n == 0) {
      return new LongPrimeFactor[0];
    }

    ArrayList<LongPrimeFactor> factors = new ArrayList<>();

    if (n % 2 == 0) {
      n /= 2;
      int degree = 1;
      while (n % 2 == 0) {
        n /= 2;
        degree++;
      }
      factors.add(new LongPrimeFactor(2, degree));
    }

    long maxFactor = (long) Math.sqrt(n);
    for (long factor = 3; factor <= maxFactor && n > 1; factor += 2) {
      if (n % factor == 0) {
        n /= factor;
        int degree = 1;
        while (n % factor == 0) {
          n /= factor;
          degree++;
        }
        factors.add(new LongPrimeFactor(factor, degree));
        maxFactor = (long) Math.sqrt(n);
      }
    }

    if (n > 1) {
      factors.add(new LongPrimeFactor(n, 1));
    }

    return factors.toArray(new LongPrimeFactor[0]);
  }

  public static class LongPrimeFactor {
    /** Prime factor. */
    public long prime;
    public int degree;

    public LongPrimeFactor(long prime, int degree) {
      this.prime = prime;
      this.degree = degree;
    }
  }

  public static long calcLargestPrimeDivOf(long n) {
    if (n <= 0) {
      throw new IllegalArgumentException();
    }
    long lastF = 1;
    if (n % 2 == 0) {
      lastF = 2;
      n /= 2;
      while (n % 2 == 0) {
        n /= 2;
      }
    }
    long maxF = (long) Math.sqrt(n);
    for (long f = 3; f <= maxF && n > 1; f += 2) {
      if (n % f == 0) {
        lastF = f;
        n /= f;
        while (n % f == 0) {
          n /= f;
        }
        maxF = (long) Math.sqrt(n);
      }
    }
    if (n > 1) {
      lastF = n;
    }
    return lastF;
  }

  /** Computes the lowest prime factor of each integer between 2 and n. */
  public static int[] genLowestPrimeFactorOfIntsUpTo(int n) {
    // Allocate enough memory for the primes up to n.
    int u = n + 32;
    double lu = Math.log(u);
    int[] primes = new int[(int) (u / lu + u / lu / lu * 1.5)];
    int numPrimes = 0;

    int[] lowestPrimeFactorOf = new int[n + 1];
    for (int i = 2; i <= n; i++) {
      lowestPrimeFactorOf[i] = i;
    }

    for (int p = 2; p <= n; ++p) {
      if (lowestPrimeFactorOf[p] == p) {
        primes[numPrimes++] = p;
      }
      for (int i = 0; i < numPrimes && primes[i] <= lowestPrimeFactorOf[p]; ++i) {
        int tmp = primes[i] * p;
        if (tmp > n) break;
        lowestPrimeFactorOf[tmp] = primes[i];
      }
    }
    return lowestPrimeFactorOf;
  }
}
