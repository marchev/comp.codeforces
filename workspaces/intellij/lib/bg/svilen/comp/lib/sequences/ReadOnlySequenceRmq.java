package bg.svilen.comp.lib.sequences;

/**
 * RMQ implementation based on arrays. Assumes the sequence does not change.
 */
public class ReadOnlySequenceRmq implements Rmq {

  private final int origArraySz;
  private final int[] auxArray;
  private final int[][] rmq;

  public ReadOnlySequenceRmq(int[] seq) {
    origArraySz = seq.length;
    int deg = getFirstPowerOf2GreaterThan(seq.length - 1);

    int auxArraySz = 1 << deg;
    assert auxArraySz >= seq.length;
    auxArray = new int[auxArraySz];
    // TODO(marchev): use System.arraycopy
    for (int i = 0; i < seq.length; ++i) {
      auxArray[i] = seq[i];
    }
    for (int i = seq.length; i < auxArraySz; ++i) {
      auxArray[i] = Integer.MAX_VALUE;
    }

    rmq = new int[deg + 1][];
    for (int d = 0; d <= deg; ++d) {
      rmq[d] = new int[auxArraySz - (1 << d) + 1];
    }
    for (int i = 0; i < auxArraySz; ++i) {
      rmq[0][i] = i;
    }
    for (int d = 1; d <= deg; ++d) {
      for (int p = 0; p <= auxArraySz - (1 << d); ++p) {
        int x = rmq[d - 1][p];
        int y = rmq[d - 1][p + (1 << (d - 1))];
        rmq[d][p] = (auxArray[x] <= auxArray[y]) ? x : y;
      }
    }
  }

  public int queryMinIndex(int left, int right) {
    if (left < 0 || right >= origArraySz) {
      throw new ArrayIndexOutOfBoundsException(String.format("[l,r]=[%d,%d]", left, right));
    }
    if (left > right) {
      throw new IllegalArgumentException(String.format("[l,r]=[%d,%d]", left, right));
    }
    // TODO(marchev): optimize this
    int d = getFirstPowerOf2GreaterThan(right - left + 1) - 1;
    int x = rmq[d][left];
    int y = rmq[d][right - (1 << d) + 1];
    return (auxArray[x] <= auxArray[y]) ? x : y;
  }

  int getFirstPowerOf2GreaterThan(int n) {
    int d = 0;
    for (int i = 1; i <= n; i <<= 1) {
      d++;
    }
    return d;
  }
}
