package bg.svilen.comp.lib.sequences;

/**
 * Interface for all RMQ (Range Minimum Query) implementations.
 */
public interface Rmq {
  int queryMinIndex(int left, int right);
}
