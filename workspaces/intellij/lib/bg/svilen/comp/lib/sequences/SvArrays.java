package bg.svilen.comp.lib.sequences;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Array utilities.
 */
public class SvArrays {

  public static void sort(int[] array) {
    // Convert and sort as a list of boxed types, since the standard Arrays.sort(int[]) has a O(N^2) worst case.
    List<Integer> list = toList(array);
    Collections.sort(list);
    int arrayIx = 0;
    for (Integer e : list) {
      array[arrayIx++] = e;
    }
  }

  public static void sort(long[] array) {
    // Convert and sort as a list of boxed types, since the standard Arrays.sort(long[]) has a O(N^2) worst case.
    List<Long> list = toList(array);
    Collections.sort(list);
    int arrayIx = 0;
    for (Long e : list) {
      array[arrayIx++] = e;
    }
  }

  public static List<Integer> toList(int[] array) {
    List<Integer> list = new ArrayList<>(array.length);
    for (int e : array) {
      list.add(e);
    }
    return list;
  }

  public static List<Long> toList(long[] array) {
    List<Long> list = new ArrayList<>(array.length);
    for (long e : array) {
      list.add(e);
    }
    return list;
  }

  public static int[] fromIntegerList(List<Integer> list) {
    int[] array = new int[list.size()];
    int arrayIx = 0;
    for (Integer e : list) {
      array[arrayIx++] = e;
    }
    return array;
  }

  public static long[] fromLongList(List<Long> list) {
    long[] array = new long[list.size()];
    int arrayIx = 0;
    for (Long e : list) {
      array[arrayIx++] = e;
    }
    return array;
  }

  /** From uwi. http://codeforces.com/contest/915/submission/34141965 */
  public static long[] radixSort(long[] f) {
    return radixSort(f, f.length);
  }

  public static long[] radixSort(long[] f, int n) {
    long[] to = new long[n];
    {
      int[] b = new int[65537];
      for (int i = 0; i < n; i++) b[1 + (int) (f[i] & 0xffff)]++;
      for (int i = 1; i <= 65536; i++) b[i] += b[i - 1];
      for (int i = 0; i < n; i++) to[b[(int) (f[i] & 0xffff)]++] = f[i];
      long[] d = f;
      f = to;
      to = d;
    }
    {
      int[] b = new int[65537];
      for (int i = 0; i < n; i++) b[1 + (int) (f[i] >>> 16 & 0xffff)]++;
      for (int i = 1; i <= 65536; i++) b[i] += b[i - 1];
      for (int i = 0; i < n; i++) to[b[(int) (f[i] >>> 16 & 0xffff)]++] = f[i];
      long[] d = f;
      f = to;
      to = d;
    }
    {
      int[] b = new int[65537];
      for (int i = 0; i < n; i++) b[1 + (int) (f[i] >>> 32 & 0xffff)]++;
      for (int i = 1; i <= 65536; i++) b[i] += b[i - 1];
      for (int i = 0; i < n; i++) to[b[(int) (f[i] >>> 32 & 0xffff)]++] = f[i];
      long[] d = f;
      f = to;
      to = d;
    }
    {
      int[] b = new int[65537];
      for (int i = 0; i < n; i++) b[1 + (int) (f[i] >>> 48 & 0xffff)]++;
      for (int i = 1; i <= 65536; i++) b[i] += b[i - 1];
      for (int i = 0; i < n; i++) to[b[(int) (f[i] >>> 48 & 0xffff)]++] = f[i];
      long[] d = f;
      f = to;
      to = d;
    }
    return f;
  }
}
