package bg.svilen.comp.lib.sequences;

/**
 * Binary-indexed tree (aka Fenwick tree).
 * <p>
 * Docs:
 * <li>https://en.wikipedia.org/wiki/Fenwick_tree</li>
 * <li>https://www.topcoder.com/community/data-science/data-science-tutorials/binary-indexed-trees/</li>
 */
public class SvBinaryIndexedTree {

  public long[] t; // 1-based

  public SvBinaryIndexedTree(int size) {
    t = new long[size + 1];
  }

  /** Sums all elements in [0, ix], ix < size. */
  public long query(int ix) {
    ix++;
    long sum = 0;
    for (int i = ix; i > 0; i -= i & (-i)) {
      sum += t[i];
    }
    return sum;
  }

  /** Adds the given value to the element at index ix, 0 <= ix < size. */
  public void update(int ix, int value) {
    ix++;
    for (int i = ix; i < t.length; i += i & (-i)) {
      t[i] += value;
    }
  }

  /** Sums all elements in [from, to], 0 <= from,to < size. */
  public long sumOfRange(int from, int to) {
    return query(to) - query(from - 1);
  }
}
