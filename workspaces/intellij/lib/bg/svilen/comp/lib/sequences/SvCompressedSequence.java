package bg.svilen.comp.lib.sequences;

import java.util.Arrays;

/**
 * Compresses a given sequence.
 */
public class SvCompressedSequence {
  private long[] compressed;
  private int compressedSize;

  /** Modifies the original array. */
  public SvCompressedSequence(long[] original) {
    Arrays.sort(original);
    compressed = new long[original.length];
    compressedSize = 0;
    for (int i = 0, j; i < original.length; i = j) {
      compressed[compressedSize++] = original[i];
      for (j = i + 1; j < original.length && original[j] == original[i]; ++j) ;
    }
  }

  public int size() {
    return compressedSize;
  }

  public int map(long originalValue) {
    return Arrays.binarySearch(compressed, 0, compressedSize, originalValue);
  }

  public long mapToOriginal(int compressedValue) {
    return compressed[compressedValue];
  }

  /**
   * By uwi, from http://codeforces.com/contest/915/submission/34141965
   *
   * Overwrites the original coordinates and returns an array with the new coordinates.
   int p = 0;
   for(int i = 0;i < Q;i++){
   xs[p++] = as[i];
   xs[p++] = bs[i]+1;
   }
   int[] imap = shrinkX(xs);
   p = 0;
   for(int i = 0;i < Q;i++){
   as[i] = xs[p++];
   bs[i] = xs[p++];
   }
   */
//  public static int[] shrinkX(int[] a) {
//    int n = a.length;
//    long[] b = new long[n];
//    for (int i = 0; i < n; i++)
//      b[i] = (long) a[i] << 32 | i;
////    b = radixSort(b);
//    Arrays.sort(b);
//    int[] ret = new int[n];
//    int p = 0;
//    ret[0] = (int) (b[0] >> 32);
//    for (int i = 0; i < n; i++) {
//      if (i > 0 && (b[i] ^ b[i - 1]) >> 32 != 0) {
//        p++;
//        ret[p] = (int) (b[i] >> 32);
//      }
//      a[(int) b[i]] = p;
//    }
//    return Arrays.copyOf(ret, p + 1);
//  }

}
