package bg.svilen.comp.lib.string;

import java.util.Arrays;

/**
 * Suffix automaton online algorithm. O(N) in time and memory.
 * <p>
 * Resources:
 * <li>Suffix structure lecture, Moscow International Workshop ACM ICPC, 2015:
 * https://drive.google.com/file/d/0B0BBPCmtPbIcbVFsSG9qeTI1TjA/view</li>
 * <li>Short tutorial: http://codeforces.com/blog/entry/20861</li>
 * <li>Egor's implementation: https://github.com/EgorKulikov/yaal/tree/master/lib/main/net/egork/graph</li>
 * <li>Bugman's implementation: http://codeforces.com/contest/873/submission/31266854</li>
 * <li>Detailed tutorial (in Russian): http://e-maxx.ru/algo/suffix_automata</li>
 *
 * @author Svilen Marchev
 */
/*
From "Suffix structure lecture": https://drive.google.com/file/d/0B0BBPCmtPbIcbVFsSG9qeTI1TjA/view
----------------------------------------------------------------

Construction of suffix automaton

Finally, consider the algorithm for suffix automaton construction. To do this,
let’s define the term called suffix link. Let the length of the shortest line, which
is accepted by the state q is equal to . Then suffix link link(q) leads from this
state to a state that takes the same string without its first character. Referring
again to the suffix tree we can understand that in this structure suffix link will
lead to the ancestor of q. Thus, the suffix links form a tree, which corresponds
to the suffix tree of the reversed string. Also let’s denote the length of the
longest line, which is accepted by the state q as len(q). Obviously, the length
of the shortest line of q in this case will be equal to len(link(q)) + 1. Just by
the definition of suffix link.

Now let’s add characters to the end of s one by one keeping correct suffix
automaton and its suffix links. Suppose we have automaton of string s and s is
accepted by state last. We want to update it so it will be automaton for string
sc. So we need for each suffix of new string to have the final state which accepts
it.

Let’s add a state that accepts the whole string sc and call it new. Right
context of string sc, obviously is just an empty string, it means that new will
include only those suffixes that have exactly one occurrence in the string. All
of these strings can be obtained by adding the character c to suffixes of s that
are accepted by state, from which there are no transition by character c. Thus,
to make that new suffixes accepted, we will need to "jump" by the suffix links
and add transitions by the character c leading to the state new, until we come
the root or find the state which already have some transition by character c.

In case we came to the root, every non-empty suffix of string sc is accepted
by state new hence we can make link(new) = q0 and finish our work on this
step.

Otherwise we found such state q', which already has transition by character
c. It means that all suffixes of length ≤ len(q') + 1 are already accepted by
some state in automaton hence we don’t need to add transitions to state new
anymore. But we also have to calculate suffix link for state new. The largest
string accepted by this state will be suffix of sc of length len(q') + 1. It is
accepted by state t at the moment, in which there is transition by character
c from state q'. But state t can also accept strings of bigger length. So, if
len(t) = len(q') + 1, then t is the suffix link we are looking for. We make
link(new) = t and finish algorithm.

Otherwise t is a state in automaton which accepts its suffixes as well as
some other strings. Because of this we can’t determine whether it is final or
not. To solve the case we have to split off from t some state t', which will accept
every string which is accepted by t and has length ≤ len(q') + 1, hence those
suffixes of sc which made trouble. To do this let’s copy in t' all transitions
and suffix link of t but set len(t') to len(q') + 1. After this we can say that
link(new) = link(t) = t'. Finally to redirect paths corresponding to such strings
from t to t' we will jump through the suffix links of q' until transition from
that states by character c leads to t and redirect those transitions to t'. After
considering all these cases suffix automaton will be finally obtained.
*/
public class SvSuffixAutomaton {

  /**
   * The length of the longest string that this state accepts.
   * <p>
   * Note: Each state accepts a few strings with lengths in [length[link[state]]+1, length[state]].
   * Each string (except the longest) of each such class of equivalence is a suffix
   * of a longer string in the same class.
   * <p>
   * Example: A state can accept these strings: {"abbc", "bbc", "bc"},
   * but not {"abbc", "bc"} or {"abbc", "dbc"}.
   */
  public final int[] length;
  /**
   * The suffix link of a state.
   * <p>
   * Note: By following all suffix links, starting from the state that accepts the whole string
   * (i.e. {@code last}), one can find all terminal states, i.e. such that accept the full suffixes of the string.
   * <p>
   * Note: The suffix links build a suffix tree of the reversed string.
   * See: https://drive.google.com/file/d/0B0BBPCmtPbIcbVFsSG9qeTI1TjA/view
   */
  public final int[] link;

  // Representation of the graph.
  public final int[] firstEdgeOfState;
  public final int[] nextEdge;
  public final int[] destOfEdge;
  public final int[] labelOfEdge;

  /** State that accepts the whole string added so far. */
  public int lastState;
  /** Number of states currently in the automaton. */
  public int numStates;
  /** Number of edges currently in the automaton. */
  public int numEdges;

  public SvSuffixAutomaton(int stringLength) {
    length = new int[getMaxStates(stringLength)];
    link = new int[getMaxStates(stringLength)];
    firstEdgeOfState = new int[getMaxStates(stringLength)];
    nextEdge = new int[getMaxEdges(stringLength)];
    labelOfEdge = new int[getMaxEdges(stringLength)];
    destOfEdge = new int[getMaxEdges(stringLength)];

    Arrays.fill(firstEdgeOfState, -1);
    link[0] = -1;
    numStates = 1;
    numEdges = 0;
    lastState = 0;
  }

  /**
   * Builds the suffix automaton for the string Sc, assuming it is already built for S.
   *
   * @return the state that accepts the string Sc
   */
  public int add(int c) {
    int state = lastState; // State that accepts the string S
    lastState = numStates++;  // Create new state for string Sc
    // Note that length[state] is set only once and it does not change after the state is created.
    // This means that `last` will always be the state that accepts the whole string Sc, so we can
    // return it reliably to the caller.
    length[lastState] = length[state] + 1;
    for (; state != -1 && findEdge(state, c) == -1; state = link[state]) {
      // Add a transition to the new state, that accepts Sc, from each state, that accepts a suffix of S.
      addEdge(state, c, lastState);
    }
    if (state == -1) {
      // This is the first occurrence of c if we are here.
      link[lastState] = 0;
    } else {
      int q = destOfEdge[findEdge(state, c)];
      if (length[q] == length[state] + 1) {
        link[lastState] = q;
      } else { // length[q] > length[state] + 1
        // Split the q state, so that the `length` of the new state is length[state]+1.
        int clone = numStates++;
        copyEdges(q, clone);
        link[clone] = link[q];
        length[clone] = length[state] + 1;
        link[q] = clone;
        link[lastState] = clone;
        // Redirect existing transitions to the clone where needed.
        for (; state != -1; state = link[state]) {
          int edge = findEdge(state, c);
          assert edge != -1 : "If Xc is already in the automaton, Yc should also be there, for each suffix Y of X";
          if (destOfEdge[edge] != q) {
            break;
          }
          destOfEdge[edge] = clone;
        }
      }
    }
    return lastState;
  }

  public static int getMaxStates(int stringLength) {
    return 2 * stringLength + 1;
  }

  public static int getMaxEdges(int stringLength) {
    return 4 * stringLength;
  }

  private int findEdge(int state, int label) {
    for (int edge = firstEdgeOfState[state]; edge != -1; edge = nextEdge[edge]) {
      if (labelOfEdge[edge] == label) {
        return edge;
      }
    }
    return -1;
  }

  private void addEdge(int fromState, int edgeLabel, int toState) {
    int newEdge = numEdges++;
    destOfEdge[newEdge] = toState;
    labelOfEdge[newEdge] = edgeLabel;
    nextEdge[newEdge] = firstEdgeOfState[fromState];
    firstEdgeOfState[fromState] = newEdge;
  }

  private void copyEdges(int fromState, int toState) {
    for (int edge = firstEdgeOfState[fromState]; edge != -1; edge = nextEdge[edge]) {
      addEdge(toState, labelOfEdge[edge], destOfEdge[edge]);
    }
  }
}
