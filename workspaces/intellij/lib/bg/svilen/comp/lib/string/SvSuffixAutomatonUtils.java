package bg.svilen.comp.lib.string;

import net.egork.generated.collections.list.IntArrayList;
import net.egork.generated.collections.list.IntList;

/**
 * Utils for working with suffix automata.
 *
 * @author Svilen Marchev
 */
public class SvSuffixAutomatonUtils {

  /** Topologically sorts the automaton's states, with respect to the link function. */
  public static IntList getStatesInTopologicalOrder(SvSuffixAutomaton a) {
    // lastState accepts the whole string, so its `length` is the longest possible.
    int maxLength = a.length[a.lastState];
    IntArrayList[] nodesByLength = new IntArrayList[maxLength + 1];
    for (int len = 0; len <= maxLength; ++len) {
      nodesByLength[len] = new IntArrayList();
    }
    for (int state = 0; state < a.numStates; ++state) {
      nodesByLength[a.length[state]].add(state);
    }

    IntArrayList list = new IntArrayList(a.numStates);
    for (int len = maxLength; len >= 0; --len) {
      for (int state : nodesByLength[len]) {
        list.add(state);
      }
    }
    return list;
  }
}
