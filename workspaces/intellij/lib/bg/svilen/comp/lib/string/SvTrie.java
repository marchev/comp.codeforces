package bg.svilen.comp.lib.string;

/** A trie data structure, used to hold a collection of strings. */
public class SvTrie {
  private TrieNode root = new TrieNode();

  public SvTrie() {
  }

  public void add(String s) {
    TrieNode cur = root;
    for (int i = 0; i < s.length(); ++i) {
      if (cur.links[s.charAt(i)] == null) {
        cur.links[s.charAt(i)] = new TrieNode();
      }
      cur = cur.links[s.charAt(i)];
    }
    cur.value++;
  }

  public TrieNode getRoot() {
    return root;
  }

  public static class TrieNode {
    /** Links from the current node. Optimized for small alphabets, e.g. for latin letters. */
    private TrieNode[] links = new TrieNode[128];
    private int value = 0;

    /** Return a given child node, or null if such doesn't exist. */
    public TrieNode getLink(int next) {
      return links[next];
    }

    /** Returns how many strings terminate at this node. */
    public int getTerminatingStringsCount() {
      return value;
    }
  }
}
