package bg.svilen.comp.lib.string;

import static com.google.common.truth.Truth.assertThat;

import bg.svilen.comp.lib.string.SvTrie.TrieNode;
import org.junit.Test;

public class SvTrieTest {

  @Test
  public void works_forOneString() {
    SvTrie trie = new SvTrie();

    trie.add("svilen");

    assertThat(trie.getRoot()).isNotNull();

    assertThat(walkTo(trie, "s").getTerminatingStringsCount()).isEqualTo(0);
    assertThat(walkTo(trie, "svile").getTerminatingStringsCount()).isEqualTo(0);
    assertThat(walkTo(trie, "svilen").getTerminatingStringsCount()).isEqualTo(1);

    assertThat(walkTo(trie, "Z")).isNull();
    assertThat(walkTo(trie, "svilenZ")).isNull();
  }

  @Test
  public void works_forMultipleStrings() {
    SvTrie trie = new SvTrie();

    trie.add("svilen");
    trie.add("pilen");

    assertThat(walkTo(trie, "svilen").getTerminatingStringsCount()).isEqualTo(1);
    assertThat(walkTo(trie, "pilen").getTerminatingStringsCount()).isEqualTo(1);
  }

  @Test
  public void works_forEmptyString() {
    SvTrie trie = new SvTrie();

    trie.add("");

    assertThat(trie.getRoot()).isNotNull();
    assertThat(trie.getRoot().getTerminatingStringsCount()).isEqualTo(1);
  }

  @Test
  public void works_forMultipleOccurrences() {
    SvTrie trie = new SvTrie();

    trie.add("svilen");
    trie.add("svilen");
    trie.add("svilen");

    assertThat(walkTo(trie, "svilen").getTerminatingStringsCount()).isEqualTo(3);
  }

  TrieNode walkTo(SvTrie trie, String s) {
    TrieNode cur = trie.getRoot();
    for (char c : s.toCharArray()) {
      assertThat(cur).isNotNull();
      cur = cur.getLink(c);
    }
    return cur;
  }
}
